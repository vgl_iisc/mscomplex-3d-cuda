#include <queue>
#include <unordered_map>
#include <iterator>

#include <boost/foreach.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>

#include <omp.h>

using namespace std;

namespace br = boost::range;
namespace ba = boost::adaptors;

namespace grid
{

/*===========================================================================*/
  
  bool mscomplex_t::is_valid_canc_edge_prl (int_pair_t e, cell_fn_t thr)
{
  order_pr_by_cp_index(*this,e[0],e[1]);

  if(is_canc_prl(e[0]) || is_canc_prl(e[1]))
    return false;

  if(is_paired(e[0]) || is_paired(e[1]))
    return false;

  if(m_domain_rect.isOnBoundry(cellid(e[0])) !=
     m_domain_rect.isOnBoundry(cellid(e[1])))
    return false;

  ASSERT(m_des_conn[e[0]].count(e[1]) == 1);
  ASSERT(m_asc_conn[e[1]].count(e[0]) == 1);
  ASSERT(m_des_conn[e[0]][e[1]] == m_asc_conn[e[1]][e[0]]);

  if(m_des_conn[e[0]][e[1]] != 1) //If the paired nodes are connected by >1 path
    return false;

  bool   is_epsilon_persistent = (vertid(e[0]) == vertid(e[1]));
  bool   is_pers_lt_t          = std::abs(fn(e[0]) - fn(e[1])) < thr;

  return (is_epsilon_persistent || is_pers_lt_t);
}

/*---------------------------------------------------------------------------*/

  bool mscomplex_t::is_canc_prl(int i) const
  {
    ENSURE(is_in_range(i,0,m_cp_is_canc_prl.size()), "Out of range cancellation");
    return (m_cp_is_canc_prl[i] == 1) ? true : false;
  }  
  
 /*---------------------------------------------------------------------------*/

  void mscomplex_t::compute_nbd(int& verts)
  {
    std::vector<int>::const_iterator it;
    long key;
  
    for(int i = 0; i < verts; ++i)
      {
	BOOST_FOREACH(int j, pers_edges[i])
	  {
	    ENSURES(!is_canc_prl(i) && !is_canc_prl(j));
	      
	    if(index(i) == index(j)+1)
	      {		
		int nbs = 0;
		
		nbs += pers_edges[i].size()-1; //adj(i) except the edge itself
		nbs += pers_edges[j].size()-1; //adj(j)-1
		
		for(it=pers_edges[i].begin(); it!=pers_edges[i].end(); it++)
		  {
		    if(*it != j)
		      nbs += pers_edges[*it].size()-1; //exclude incoming edge
		  }
		
		for(it=pers_edges[j].begin(); it!=pers_edges[j].end(); it++)
		  {
		    if(*it != i)
		      nbs += pers_edges[*it].size()-1;
		  }  
		
		key = return_long(i, j, verts);
		pb_map.insert(std::make_pair(key, nbs));

		i_to_p_pers.push_back(std::make_pair(i,j));
		
		ENSURES(p_to_i_pers.count(key) == 0);
		p_to_i_pers.insert(std::make_pair(key, pb_map.size()-1));
	      }
	  }
      }
    
  }

  
/*---------------------------------------------------------------------------*/

  void mscomplex_t::clear_mis_ds()
  {
    mis_edges = 0;
    
    pers_edges.clear();
    pb_map.clear();
    p_to_i_pers.clear();
    i_to_p_pers.clear();
    p_to_i_ms.clear();
    i_to_p_ms.clear();

    least_nbd.clear();
    least_nbd_2.clear();
    ind_set.clear();
 
  }
  
  /*---------------------------------------------------------------------------*/
  void mscomplex_t::ms_to_id(int_pair_t &p, int &verts)
  {
    long key = return_long(p[0], p[1], verts);
    i_to_p_ms.push_back(std::make_pair(p[0],p[1]));
    
    ENSURES(p_to_i_ms.count(key) == 0);
    p_to_i_ms.insert(std::make_pair(key, i_to_p_ms.size()-1));
  }

  /*---------------------------------------------------------------------------*/
  void mscomplex_t::mis_simplify(double &thresh, int &nmax, int &nmin,
				      std::ofstream &output)
{
  int verts = get_num_critpts();
  
  pers_edges.resize(verts);
  
  //#pragma omp parallel for
  for(int i = 0; i < verts; ++i)
    {
      if(!is_canc_prl(i))
	{
	  BOOST_FOREACH(int_int_t j, m_des_conn[i]) //For each element in the map m_des_conn[i]
	    {
	      int_pair_t pr(i,j.first);
	      ENSURES(index(i) == index(j.first)+1);

	      ms_to_id(pr, verts);
	      
	      if(is_valid_canc_edge_prl(pr, thresh))
		{
		  pers_edges[i].push_back(j.first);
		  pers_edges[j.first].push_back(i);
		}
	    }
	}
    }

  compute_nbd(verts);

  std::cout<<"MS graph: "<<p_to_i_ms.size()<<" : Pers graph: "<<p_to_i_pers.size()<<"\n";
  output<<p_to_i_ms.size()<<","<<p_to_i_pers.size()<<",";
  
  least_nbd.resize(i_to_p_ms.size());
  least_nbd_2.resize(i_to_p_ms.size());
  
  ind_set.resize(p_to_i_pers.size(), 1);
    
  //double s_test = omp_get_wtime();

  #pragma omp parallel for
  for(int i = 0; i < i_to_p_ms.size(); i++)
  {
    least_nbd[i] = pers_adj_nbd(i_to_p_ms[i].first, i_to_p_ms[i].second, verts);
  }

  //Testing
  /*#pragma omp parallel for
  for(int i = 0; i < i_to_p_ms.size(); i++)
  {
    check_least_nbd(i_to_p_ms[i].first, i_to_p_ms[i].second, verts);
    }*/
  
  #pragma omp parallel for
  for(int i = 0; i < i_to_p_ms.size(); i++)
  {
    least_nbd_2[i] = ms_inter_nbd(i_to_p_ms[i].first, i_to_p_ms[i].second, verts, i);
  }
      
  least_nbd = least_nbd_2;

  //Testing
  /*#pragma omp parallel for
  for(int i = 0; i < i_to_p_ms.size(); i++)
  {
    check_least_nbd(i_to_p_ms[i].first, i_to_p_ms[i].second, verts);
    }*/
  
  #pragma omp parallel for
  for(int i = 0; i < i_to_p_pers.size(); i++)
  {
    if(ind_set.at(i) == 1)      
      ms_final_nbd(i_to_p_pers[i].first, i_to_p_pers[i].second, verts);
  }

  //Testing
  /*#pragma omp parallel for
  for(int i = 0; i < i_to_p_ms.size(); i++)
  {
    check_least_nbd(i_to_p_ms[i].first, i_to_p_ms[i].second, verts);
    }*/
  
  //double e_test = omp_get_wtime();
  //std::cout<<"Time: "<<e_test-s_test<<"\n";
  
  //Edges in the MIS
  for(int i = 0; i < p_to_i_pers.size(); i++)
  {
    if(ind_set.at(i) == 1)
      {
	mis_edges++;
	
	//int_pair_t pr(i_to_p_pers[i].first, i_to_p_pers[i].second);
	//output<<"p: "<<pr[0]<<" "<<pr[1]<<" "<<(float)fn(pr[0])<<" "<<(float)fn(pr[1])<<
	//" "<<(float)(std::abs(fn(pr[0]) - fn(pr[1])))<<"\n";
      }
  }
  std::cout<<"MIS edges: "<<mis_edges<<"\n";

  output<<mis_edges<<",";

  //Testing
  /*#pragma omp parallel for
  for(int i = 0; i < i_to_p_pers.size(); i++)
  {
    if(ind_set.at(i) == 1)
      test_mis_ms(i_to_p_pers[i].first, i_to_p_pers[i].second, verts);
      }*/
  
  //Cancellation
  #pragma omp parallel for
  for(int i = 0; i < i_to_p_pers.size(); ++i)
  {
    if(ind_set.at(i) == 1)
      {
	int_pair_t pr(i_to_p_pers[i].first, i_to_p_pers[i].second);
	ENSURES(is_valid_canc_edge_prl(pr,thresh));

	cancel_pair(pr[0],pr[1]);
      }
  }
 
}


}

/*---------------------------------------------------------------------------*/

