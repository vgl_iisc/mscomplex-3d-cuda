#include <thrust/host_vector.h>

#include <unordered_map>
#include <iterator>
#include <utility>

#include <boost/foreach.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>
#include <grid_mscomplex_inl.h>

#include <boost/range/adaptors.hpp>
#include <boost/foreach.hpp>

#include <grid_dataset_cuda.h>

#include <cuda_runtime.h>
#include <omp.h>
#include <fstream>

typedef float func_t;
typedef unsigned char flag_t;

#define static_assert BOOST_STATIC_ASSERT
#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

using namespace std;
namespace br = boost::range;
namespace ba = boost::adaptors;


namespace grid
{

 inline void serial_simpl_geom(const mscomplex_ptr_t msc, const double &thresh);

  inline void cancel_pair (const mscomplex_ptr_t msc, int p, int q)
{
  order_pr_by_cp_index(*msc,p,q);

  ENSURE(msc->m_hversion == msc->m_canc_list.size(),
  "Cannot cancel pair !! Ms complex resolution is not coarsest.");
  ENSURE(msc->index(p) == msc->index(q)+1,
         "indices do not differ by 1");
  ENSURE(msc->m_cp_pair_idx[p] == -1 && msc->m_cp_pair_idx[q] == -1,
         "p/q has already been paired");
  ENSURE(msc->m_des_conn[p].count(q) == 1 && msc->m_asc_conn[q].count(p) == 1,
         "p is not connected to q");
  ENSURE(msc->m_des_conn[p][q] == 1 && msc->m_asc_conn[q][p] == 1,
         "p and q are multiply connected");

  msc->m_cp_pair_idx[p] = q;
  msc->m_cp_pair_idx[q] = p;
  msc->m_canc_list.push_back(int_pair_t(p,q));

  msc->cancel_pair();
}
  
/*---------------------------------------------------------------------------*/

inline bool is_valid_recanc_edge
(const mscomplex_t &msc, int_pair_t e, cell_fn_t thr)
{
  //std::cout<<"Threshold = "<<thr<<"\n";
  order_pr_by_cp_index(msc,e[0],e[1]);

  if(msc.is_canceled(e[0])||msc.is_canceled(e[1]))
    return false;

  if(msc.is_paired(e[0]) || msc.is_paired(e[1]))
    return false;
  
  if(msc.m_domain_rect.isOnBoundry(msc.cellid(e[0])) !=
     msc.m_domain_rect.isOnBoundry(msc.cellid(e[1])))
    return false;
     
  ASSERT(msc.m_des_conn[e[0]].count(e[1]) == 1);
  ASSERT(msc.m_asc_conn[e[1]].count(e[0]) == 1);
  ASSERT(msc.m_des_conn[e[0]][e[1]] == msc.m_asc_conn[e[1]][e[0]]);

  if(msc.m_des_conn[e[0]][e[1]] != 1) //If the paired nodes are connected by >1 path
    return false;
  
  bool   is_epsilon_persistent = (msc.vertid(e[0]) == msc.vertid(e[1]));
  bool   is_pers_lt_t          = std::abs(msc.fn(e[0]) - msc.fn(e[1])) < thr;
  
  return (is_epsilon_persistent || is_pers_lt_t);
}

/*---------------------------------------------------------------------------*/
    
  void mscomplex_t::resimplify_geom()
  {
    std::cout<<"Resimplifying: \n";
    double thresh = 0.05;
    bool is_nrm = true;
    if(is_nrm)
    thresh *= (*br::max_element(m_cp_fn) - *br::min_element(m_cp_fn));
    serial_simpl_geom(shared_from_this(), thresh);
  }

/*---------------------------------------------------------------------------*/
  
inline void serial_simpl_geom(const mscomplex_ptr_t msc, const double &thresh)
{
  //insert all ordered edges into queue
  queue<int_pair_t> pq;

  std::vector<int_pair_t>::iterator iter;
    
  for(int i = 0; i < 6; i++)
    {
      for(iter = msc->prl_m_canc_list_0[i].begin(); iter < msc->prl_m_canc_list_0[i].end(); iter++)
	pq.push(*iter);

      for(iter = msc->prl_m_canc_list_1[i].begin(); iter < msc->prl_m_canc_list_1[i].end(); iter++)
	pq.push(*iter);
    }

  for(iter = msc->serial_m_canc_list_2.begin(); iter < msc->serial_m_canc_list_2.end(); iter++)
    pq.push(*iter);
	  
  while(pq.size()!=0)
  {
    int_pair_t pr = pq.front();
    //std::cout<<pr<<"\n";
    pq.pop();
        
    ENSURE(is_valid_recanc_edge(*msc,pr,thresh),
      "Invalid edge found during resimplification.");

    ASSERT(order_pair<DES>(msc,pr) == pr);

    cancel_pair(msc,pr[0],pr[1]);
  }

  msc->m_merge_dag->update(msc);

  //DLOG << "Exited  :" << SVAR(m_hversion) <<SVAR(ns_max) <<SVAR(ns_min);
  
}

/*---------------------------------------------------------------------------*/
   
  

}
