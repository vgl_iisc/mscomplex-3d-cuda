#include <unordered_map>
#include <iterator>
#include <utility>

#include <boost/foreach.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>
#include <grid_mscomplex_inl.h>

const int null_nbd = -1;

namespace grid
{

  long mscomplex_t::return_long(int &p, int &q, int &verts)
  {
    return long(p)*long(verts) + long(q);
  }

  /*---------------------------------------------------------------------------*/

  bool mscomplex_t::order_edges(const int &n1, const int &n2,
				const int &verts, std::pair<int,int> &tmp_e)
  {
    int e1 = tmp_e.first;
    int e2 = tmp_e.second;

    if((e1 > n1) || ((e1 == n1) && (e2 > n2)))
      {
	tmp_e = std::make_pair(n1, n2);
	return true;
      }   
      
    return false;
  }

/*---------------------------------------------------------------------------*/

  void mscomplex_t::adj_compare(int &p, int &q, int &tmp_nbd,
				std::pair<int,int> &tmp_e, int &verts)
  {  
    int n_nbd = pb_map.at(return_long(p, q, verts));
    
    if((n_nbd < tmp_nbd) || (tmp_nbd == null_nbd))
      {
	tmp_nbd = n_nbd;
	tmp_e = std::make_pair(p,q);
      }
      
    else if(n_nbd == tmp_nbd)
      order_edges(p, q, verts, tmp_e);
  }
  
/*---------------------------------------------------------------------------*/
  //Input: ms graph = 0 and persistent graph = 1
  
  template <int graph>
  void mscomplex_t::inter_compare(int &p, int &q, int &tmp_nbd,
				  std::pair<int,int> &tmp_e, int &verts)
  {    
    int pq_idx = (graph == 0) ? p_to_i_ms.at(return_long(p, q, verts)) :
      p_to_i_pers.at(return_long(p, q, verts));
    
    int n_nbd = least_nbd.at(pq_idx).first;
    int e1 = least_nbd.at(pq_idx).second.first;
    int e2 = least_nbd.at(pq_idx).second.second;

    if(n_nbd == null_nbd) //Remove invalid neighbors
      return;
	   
    if((n_nbd < tmp_nbd) || (tmp_nbd == null_nbd))
      {
	tmp_nbd = n_nbd;
	tmp_e = std::make_pair(e1,e2);
      }
    
    else if(n_nbd == tmp_nbd)
      order_edges(e1, e2, verts, tmp_e);
    
  }

  /*---------------------------------------------------------------------------*/

  template <int graph>
  bool mscomplex_t::is_ind(int &p, int &q, int &tmp_nbd,
			   std::pair<int,int> &tmp_e, int &verts)
  {
    int pq_idx = (graph == 0) ? p_to_i_ms.at(return_long(p, q, verts)) :
      p_to_i_pers.at(return_long(p, q, verts));
   
    int n_nbd = least_nbd.at(pq_idx).first;

    if(n_nbd == null_nbd)
      return true;
    
    if(n_nbd < tmp_nbd)
      return false;

    if(n_nbd == tmp_nbd)
      {
	if(order_edges(least_nbd.at(pq_idx).second.first,
		       least_nbd.at(pq_idx).second.second, verts, tmp_e))
	  return false;
      }
      
  return true;
  
  }

  /*---------------------------------------------------------------------------*/

  inline bool is_filt_edge(int &e1, int &e2, int &verts, const mscomplex_t &msc)
  {
    return msc.pb_map.count(long(e1)*long(verts) + long(e2)) == 1 ? true : false;
  }

  /*---------------------------------------------------------------------------*/

  inline bool is_equal(const int &e1, const int &e2, const std::pair<int,int> &tmp_e)
  {
    return ((e1 == tmp_e.first) && (e2 == tmp_e.second));
  }

  /*---------------------------------------------------------------------------*/
  //Input=persistent or ms graph, computes adjacent least neighborhood on pers graph, P(e) = 1/n where n = neighbors, maximizing P(e) for any e means you select the edge with the least no of neighbors.

  std::pair<int,std::pair<int,int>>
  mscomplex_t::pers_adj_nbd(int &i, int &j, int &verts)
  {
    int p, q;
    long ij_key = return_long(i, j, verts);
    bool am_filt = (pb_map.count(ij_key) == 1) ? true : false;

    int ij_nbd = (am_filt) ? pb_map.at(ij_key) : null_nbd;
    int tmp_nbd = ij_nbd;
    std::pair<int,int> tmp_e = std::make_pair(i,j);
    
    BOOST_FOREACH(int k, pers_edges[i]) //i, adj(i)
      {
	if(k != j)
	  {
	    p = i;
	    q = k;

	    order_pr_by_cp_index(*this, p, q);
	    ENSURES(index(p) == index(q)+1);
    
	    adj_compare(p, q, tmp_nbd, tmp_e, verts);
	  }
      }
    
    BOOST_FOREACH(int k, pers_edges[j]) //j, adj(j)
      {
	if(k != i)
	  {
	    p = j;
	    q = k;

	    order_pr_by_cp_index(*this, p, q);
	    ENSURES(index(p) == index(q)+1);
	     
	    adj_compare(p, q, tmp_nbd, tmp_e, verts);
	  }
      }
    
    if(am_filt && ((tmp_nbd < ij_nbd) || (!is_equal(i, j, tmp_e))))
       ind_set[p_to_i_pers.at(ij_key)] = 0;
    
    return std::make_pair(tmp_nbd, tmp_e);
  
  }
  
  /*---------------------------------------------------------------------------*/
  //Input=persistent graph, computes intermediate (edges 1 hop away) least neighborhood values on pers graph
  //If input = ms graph, your underlying graph on which inter nbd is computed has to be the ms graph
    
  std::pair<int,std::pair<int,int>>
  mscomplex_t::pers_inter_nbd(int &i, int &j, int &verts, const int &idx)
  {
    int ij_nbd = least_nbd.at(idx).first;
    int p, q;
    int tmp_nbd = ij_nbd;
    std::pair<int,int> tmp_e = least_nbd.at(idx).second;
    
    BOOST_FOREACH(int k, pers_edges[i]) //i, adj(i)
      {
	if(k != j)
	  {
	    p = i;
	    q = k;
	    
	    order_pr_by_cp_index(*this, p, q);
	    ENSURES(index(p) == index(q)+1);
	   
	    inter_compare<1>(p, q, tmp_nbd, tmp_e, verts);
	  }
      }
    
    BOOST_FOREACH(int k, pers_edges[j]) //j, adj(j)
      {
	if(k != i)
	  {
	    p = j;
	    q = k;
	    
	    order_pr_by_cp_index(*this, p, q);
	    ENSURES(index(p) == index(q)+1);
	    
	    inter_compare<1>(p, q, tmp_nbd, tmp_e, verts);
	 }
      }
    
    if((ind_set.at(idx) == 1) && ((tmp_nbd < ij_nbd) || (!is_equal(i, j, tmp_e))))
       ind_set[idx] = 0;
     
       return std::make_pair(tmp_nbd, tmp_e);
       
   }
    
 /*---------------------------------------------------------------------------*/
    //Input=ms graph computes intermediate neighbors on the ms graph
    
    std::pair<int,std::pair<int,int>>
      mscomplex_t::ms_inter_nbd(int &i, int &j, int &verts, const int &idx)
  {
    long ij_key = return_long(i, j, verts);
    bool am_filt = (pb_map.count(ij_key) == 1) ? true : false;
    int ij_nbd = least_nbd.at(idx).first;
    int tmp_nbd = ij_nbd;
    std::pair<int,int> tmp_e = least_nbd.at(idx).second;

    BOOST_FOREACH(int_int_t k, m_des_conn[i]) //i, adj(i)
      {
	if(k.first != j)
	    inter_compare<0>(i, k.first, tmp_nbd, tmp_e, verts);
      }

    BOOST_FOREACH(int_int_t k, m_asc_conn[i]) //i, adj(i)
	inter_compare<0>(k.first, i, tmp_nbd, tmp_e, verts);
    
    BOOST_FOREACH(int_int_t k, m_des_conn[j]) //j, adj(j)
	inter_compare<0>(j, k.first, tmp_nbd, tmp_e, verts);

    BOOST_FOREACH(int_int_t k, m_asc_conn[j]) //j, adj(j)
      {
	if(k.first != i)
	    inter_compare<0>(k.first, j, tmp_nbd, tmp_e, verts);
      }
    
    if(am_filt && (ind_set.at(p_to_i_pers.at(ij_key)) == 1))
      {
	if((tmp_nbd < ij_nbd) || (!is_equal(i, j, tmp_e)))
	  ind_set[p_to_i_pers.at(ij_key)] = 0;
      }

      return std::make_pair(tmp_nbd, tmp_e);
  }
  
 /*---------------------------------------------------------------------------*/
  //Input: persistent graph, computes ind set on pers edges
  
   void mscomplex_t::pers_final_nbd(int &i, int &j, int &verts, const int &idx)
 {
   long ij_key = return_long(i, j, verts);
   int ij_nbd = least_nbd.at(idx).first;
   int p, q;
   std::pair<int,int> ij = std::make_pair(i,j);
   
   BOOST_FOREACH(int k, pers_edges[i]) //i, adj(i)
     {
       if(k != j)
	 {
	   p = i;
	   q = k;
	   
	   order_pr_by_cp_index(*this, p, q);
	   ENSURES(index(p) == index(q)+1);
	   
	   if(!is_ind<1>(p, q, ij_nbd, ij, verts))
	     {
	       ind_set[p_to_i_pers.at(ij_key)] = 0;
	       return;
	     }
	 }
     }
   
   BOOST_FOREACH(int k, pers_edges[j]) //j, adj(j)
     {
       if(k != i)
	 {
	   p = j;
	   q = k;
	   
	   order_pr_by_cp_index(*this, p, q);
	   ENSURES(index(p) == index(q)+1);
	   
	   if(!is_ind<1>(p, q, ij_nbd, ij, verts))
	     {
	       ind_set[p_to_i_pers.at(ij_key)] = 0;
	       return;
	     }
	 }
     }
 }

  /*---------------------------------------------------------------------------*/

   void mscomplex_t::ms_final_nbd(int &i, int &j, int &verts)
 {
   long ij_key = return_long(i, j, verts);
   int ij_nbd = least_nbd.at(p_to_i_ms.at(ij_key)).first;
   int p, q;
   std::pair<int,int> ij = std::make_pair(i,j);
   
   BOOST_FOREACH(int_int_t k, m_des_conn[i]) //i, adj(i)
      {
	if((k.first != j) && (!is_ind<0>(i, k.first, ij_nbd, ij, verts)))
	  {
	    ind_set[p_to_i_pers.at(ij_key)] = 0;
	    return;
	  }
      }

   BOOST_FOREACH(int_int_t k, m_asc_conn[i]) //i, adj(i)
     {
       if(!is_ind<0>(k.first, i, ij_nbd, ij, verts))
	 {
	   ind_set[p_to_i_pers.at(ij_key)] = 0;
	    return;
	 }
     }
   
    BOOST_FOREACH(int_int_t k, m_des_conn[j]) //j, adj(j)
      {
	if(!is_ind<0>(j, k.first, ij_nbd, ij, verts))
	 {
	   ind_set[p_to_i_pers.at(ij_key)] = 0;
	   return;
	 }
      }

     BOOST_FOREACH(int_int_t k, m_asc_conn[j]) //j, adj(j)
      {
	if((k.first != i) && (!is_ind<0>(k.first, j, ij_nbd, ij, verts)))
	     {
	       ind_set[p_to_i_pers.at(ij_key)] = 0;
	       return;
	     }
      }
 }


   /*---------------------------------------------------------------------------*/
}
