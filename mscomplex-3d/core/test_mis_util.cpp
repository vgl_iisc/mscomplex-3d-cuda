#include <unordered_map>
#include <utility>

#include <boost/foreach.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>
#include <grid_mscomplex_inl.h>

/*---------------------------------------------------------------------------*/

namespace grid
{
  inline bool is_filt_edge(const int &e1, const int &e2, const int &verts,
			   const mscomplex_t &msc)
  {
    return msc.pb_map.count(long(e1)*long(verts) + long(e2)) == 1 ? true : false;
  }

  /*---------------------------------------------------------------------------*/
  
  void mscomplex_t::check_least_nbd(const int &p, const int &q, const int &verts)
  {
    if(is_filt_edge(p, q, verts, *this))
      {
	int pq_idx = p_to_i_ms.at(long(p)*long(verts) + long(q));

	ENSURES(least_nbd.at(pq_idx).first != -1);

	int e1 = least_nbd.at(pq_idx).second.first;
	int e2 = least_nbd.at(pq_idx).second.second;
	
	ENSURES(is_filt_edge(e1, e2, verts, *this));
	
      }
  }

 /*---------------------------------------------------------------------------*/

  void mscomplex_t::conn_test(const int &p, const int &q, const int &verts)
  {
    long pq_key;
    bool am_filt;

    pq_key = (long)p*(long)verts + (long)q;
    am_filt = (pb_map.count(pq_key) == 1) ? true : false;
    
    if(am_filt && ind_set.at(p_to_i_pers.at(pq_key)) == 1)
      std::cout<<"Debug\n";
  }

  /*---------------------------------------------------------------------------*/
  
  void mscomplex_t::test_mis_ms(const int &i, const int &j, const int &verts)
  {
    BOOST_FOREACH(int_int_t di, m_des_conn[i]) //i, adj(i)
      {
	if(di.first != j)
	  {
	    conn_test(i, di.first, verts);
	    
	    BOOST_FOREACH(int_int_t ddi, m_des_conn[di.first])
	      {
		conn_test(di.first, ddi.first, verts);

		BOOST_FOREACH(int_int_t dddi, m_des_conn[ddi.first])
		  conn_test(ddi.first, dddi.first, verts);

		BOOST_FOREACH(int_int_t addi, m_asc_conn[ddi.first])
		  {
		    if(addi.first !=  di.first)
		      conn_test(addi.first, ddi.first, verts);
		  }
	      }

	    /*---------------*/

	    BOOST_FOREACH(int_int_t adi, m_asc_conn[di.first])
	      {
		if(adi.first != i)
		  {
		    conn_test(adi.first, di.first, verts);

		    BOOST_FOREACH(int_int_t dadi, m_des_conn[adi.first])
		      {
			if(dadi.first !=  di.first)
			  conn_test(adi.first, dadi.first, verts);
		      }

		    BOOST_FOREACH(int_int_t aadi, m_asc_conn[adi.first])
		      conn_test(aadi.first, adi.first, verts);
		    
		  }
	      }

	  }
      }

    /*---------------------------*/
    
    BOOST_FOREACH(int_int_t ai, m_asc_conn[i]) //i, adj(i)
      {
	conn_test(ai.first, i, verts);
	
	BOOST_FOREACH(int_int_t dai, m_des_conn[ai.first])
	  {
	    if(dai.first != i)
	      {
		conn_test(ai.first, dai.first, verts);

		BOOST_FOREACH(int_int_t ddai, m_des_conn[dai.first])
		  conn_test(dai.first, ddai.first, verts);

		BOOST_FOREACH(int_int_t adai, m_asc_conn[dai.first])
		  {
		    if(adai.first !=  ai.first)
		      conn_test(adai.first, dai.first, verts);
		  }
	      }
	  }

	/*---------------*/
	
	BOOST_FOREACH(int_int_t aai, m_asc_conn[ai.first])
	  {
	    conn_test(aai.first, ai.first, verts);

	     BOOST_FOREACH(int_int_t daai, m_des_conn[aai.first])
	       {
		 if(daai.first !=  ai.first)
		   conn_test(aai.first, daai.first, verts);
	       }
	     
	     BOOST_FOREACH(int_int_t aaai, m_asc_conn[aai.first])
	       conn_test(aaai.first, aai.first, verts);
	  }
      }

    /*---------------------------*/
    
    BOOST_FOREACH(int_int_t dj, m_des_conn[j]) //j, adj(j)
      {
	conn_test(j, dj.first, verts);
	
	BOOST_FOREACH(int_int_t ddj, m_des_conn[dj.first])
	  {
	    conn_test(dj.first, ddj.first, verts);

	    BOOST_FOREACH(int_int_t dddj, m_des_conn[ddj.first])
	      conn_test(ddj.first, dddj.first, verts);

	    BOOST_FOREACH(int_int_t addj, m_asc_conn[ddj.first])
	      {
		if(addj.first !=  dj.first)
		  conn_test(addj.first, ddj.first, verts);
	      }
	    
	  }

	/*---------------*/
	
	BOOST_FOREACH(int_int_t adj, m_asc_conn[dj.first])
	  {
	    if(adj.first != j)
	      {
	        conn_test(adj.first, dj.first, verts);

		BOOST_FOREACH(int_int_t dadj, m_des_conn[adj.first])
		  {
		    if(dadj.first !=  dj.first)
		      conn_test(adj.first, dadj.first, verts);
		  }
		
		BOOST_FOREACH(int_int_t aadj, m_asc_conn[adj.first])
		  conn_test(aadj.first, adj.first, verts);
	      }
	  }
      }

    /*---------------------------*/

    BOOST_FOREACH(int_int_t aj, m_asc_conn[j]) //j, adj(j)
      {
	if(aj.first != i)
	  {
	    conn_test(aj.first, j, verts);
	    
	    BOOST_FOREACH(int_int_t daj, m_des_conn[aj.first])
	      {
		if(daj.first != j)
		  {
		    conn_test(aj.first, daj.first, verts);

		    BOOST_FOREACH(int_int_t ddaj, m_des_conn[daj.first])
		      conn_test(daj.first, ddaj.first, verts);
		    
		    BOOST_FOREACH(int_int_t adaj, m_asc_conn[daj.first])
		      {
			if(adaj.first !=  aj.first)
			conn_test(adaj.first, daj.first, verts);
		      }
		  }
	      }

	    /*---------------*/
	    
	    BOOST_FOREACH(int_int_t aaj, m_asc_conn[aj.first])
	      {
		conn_test(aaj.first, aj.first, verts);

		BOOST_FOREACH(int_int_t daaj, m_des_conn[aaj.first])
		  {
		    if(daaj.first !=  aj.first)
		      conn_test(aaj.first, daaj.first, verts);
		  }
		
		BOOST_FOREACH(int_int_t aaaj, m_asc_conn[aaj.first])
		  conn_test(aaaj.first, aaj.first, verts);
		    
	      }
	  }
      }
  }


/*---------------------------------------------------------------------------*/
  
}
