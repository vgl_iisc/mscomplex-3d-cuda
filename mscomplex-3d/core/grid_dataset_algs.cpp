#include <thrust/host_vector.h>

#include <boost/range/adaptors.hpp>
#include <boost/foreach.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>
#include <grid_dataset_cl.h>

#include <grid_dataset_cuda.h>

#include <cuda_runtime.h>
#include <omp.h>
#include <fstream>

typedef float func_t;
typedef unsigned char flag_t;

#define static_assert BOOST_STATIC_ASSERT
#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

using namespace std;
namespace br = boost::range;
namespace ba = boost::adaptors;

namespace grid
{

/*===========================================================================*/

 /// \brief A thread safe class to connect two critical cells in the mscomplex
class mscomplex_connector_t
{
  std::map<cellid_t,int> id_cp_map;
  mscomplex_ptr_t        msc;

public:
  inline void init(dataset_ptr_t ds)
  {
    /*#pragma omp critical(mscomplex_connector_critical_section)
      {
      for(int i = 0 ; i < msc->get_num_critpts(); i++)
      id_cp_map.insert(make_pair(msc->cellid(i),i));
      }*/

    int num_cps = msc->get_num_critpts();
    std::map<cellid_t,int>::iterator it = id_cp_map.end();
    
    #pragma omp critical(mscomplex_connector_critical_section)
    {
      for(int i=0; i<num_cps; i++)
	id_cp_map.insert(it,std::make_pair(msc->cp_map[i],msc->cp_idx[i]));
    }
    
  }

public:
  mscomplex_connector_t(mscomplex_ptr_t msc):msc(msc){}

  inline void connect_cells(cellid_t c1, cellid_t c2, int m)
  {
    int i = id_cp_map.at(c1);
    int j = id_cp_map.at(c2);

    // Exceptions thrown inside critical sections cannot pass outside
    // So this tomfoolery has to be resorted to.
    std::string e_what;

    #pragma omp critical(mscomplex_connector_critical_section)
    {
      //        if (id_cp_map.size() == 0)
      //          init();
      try{msc->connect_cps(i,j,m);}
      catch(const std::exception &e){e_what = e.what();}
    }

    ENSURES(e_what.empty()) << SVAR(c1) <<SVAR(c2);

  }

  template<eGDIR dir>
  inline void connect_cells_prl(const cellid_t &c1, const cellid_t &c2, const int &m)
  { 
    int i = id_cp_map.at(c1);
    int j = id_cp_map.at(c2);

    msc->connect_cps_prl<dir> (i, j, m);
      
  }


  };


/*===========================================================================*/

  
  
/*===========================================================================*/

template<int dim,eGDIR dir,typename range_t>
inline void mark_reachable(const range_t &rng, dataset_ptr_t ds)
{
  //bool* my_visited = new bool [num_cells(ds->get_ext_rect())]; //Extra
  
  cellid_t cets[40];

  cellid_list_t stk(boost::begin(rng),boost::end(rng));
  
  while(stk.size() != 0)
  {
    cellid_t c = stk.back(); stk.pop_back();
    
    ASSERT(ds->getCellDim(c) == dim);

    ds->visitCell(c);
    //my_visited[c_to_i(ds->get_ext_rect(),c)] = 1;
    
    for(cellid_t *b = cets, *e = cets + ds->get_cets<dir>(c,cets); b!=e; ++b)
    {
      if(!ds->isCellCritical(*b))
      {
        cellid_t p = ds->getCellPairId(*b);
        if(ds->getCellDim(p) == dim && !ds->isCellVisited(p))
        {
          stk.push_back(p);
	}
      }
    }
  }

  //Extra (to compute 1-2 junction points)
  /*cellid_t cets2[40];
  cellid_list_t stk_2(boost::begin(rng),boost::end(rng));

   while(stk_2.size() != 0)
  {
    cellid_t c2 = stk_2.back(); stk_2.pop_back();
    for(cellid_t *b2 = cets2, *e2 = cets2 + ds->get_cets<dir>(c2,cets2); b2!=e2; ++b2)
    {
      if(!ds->isCellCritical(*b2))
	{
	  cellid_t p2 = ds->getCellPairId(*b2);
	  if(ds->isCellVisited(p2) && (p2 != c2) && my_visited[c_to_i(ds->get_ext_rect(),c2)]==1)
	    {
	      if(!ds->isCellCritical(c2))
		{
		  test_12_jn_pts[c_to_i(ds->get_ext_rect(),c2)] += 1;
		}

	      if(test_12_jn_pts[c_to_i(ds->get_ext_rect(),p2)] == 0) //Undiscovered pair
		stk_2.push_back(p2);
	    }
	    
	}
    }
    
    my_visited[c_to_i(ds->get_ext_rect(),c2)] = 0;
  }
    //Extra ends
    */
}

/*---------------------------------------------------------------------------*/

template <int dim,eGDIR dir>
inline bool cellid_int_pair_cmp
(dataset_ptr_t ds, cellid_int_pair_t c1,cellid_int_pair_t c2)
{return ds->compare_cells_pp_<dir,dim>(c1.first,c2.first);}

template <int dim,eGDIR dir>
inline void compute_inc_pairs_pq
(cellid_t s, dataset_ptr_t ds, mscomplex_connector_t &msc_connector) 
{
  const int pdim = (dir == DES)?(dim - 1):(dim + 1);
  const bool no_vcheck =
      ((dim==1) && (dir==DES)) || (dim==0) ||
      ((dim==2) && (dir==ASC)) || (dim==3);

  auto cmp_dim = bind(cellid_int_pair_cmp<dim, dir>,ds,_1,_2);
  auto cmp_pdim = bind(cellid_int_pair_cmp<pdim, dir>,ds,_1,_2);
  
  priority_queue<cellid_int_pair_t,cellid_int_pair_list_t,typeof(cmp_dim)>
      pq(cmp_dim);
  priority_queue<cellid_int_pair_t,cellid_int_pair_list_t,typeof(cmp_pdim)>
      inc_pq(cmp_pdim);

  cellid_t f[40];
  
  pq.push(make_pair(s,1));

  while(pq.size() != 0 )
  {
    cellid_t c = pq.top().first; //2-cells

    ASSERT(ds->getCellDim(c) == dim);
    
    int n = 0 ;
  
    do {n += pq.top().second; pq.pop();}

    while (pq.size() != 0 && pq.top().first == c);
      
    for(cellid_t *b = f, *e = f + ds->get_cets<dir>(c,f); b != e; ++b)
    {
      if(ds->isCellCritical(*b))
      {
        ASSERT(ds->getCellDim(*b) == pdim);
        inc_pq.push(make_pair(*b,n));
      }
      else
      {
        cellid_t p = ds->getCellPairId(*b);

        if ((p != c) &&
            (no_vcheck||ds->isCellVisited(*b)) &&
            (ds->getCellDim(p) == dim ))
        {
          pq.push(make_pair(p,n));
        }
      }
    }
  }
    
  while(inc_pq.size() != 0 )
  {
    cellid_t p = inc_pq.top().first;

    ASSERT(ds->getCellDim(p) == pdim);

    int n = 0 ;

    do {n += inc_pq.top().second; inc_pq.pop();}
    while(inc_pq.size() != 0 && inc_pq.top().first == p);

    msc_connector.connect_cells(s,p,n);

    //Extra
    //output<<s<<" "<<p<<" "<<n<<"\n";
    //Ends
  }
}

/*---------------------------------------------------------------------------*/

template <int dim,eGDIR dir,bool no_vcheck,typename range_t >
inline void collect_reachable_saddle
(mfold_t &mfold,const range_t &rng, dataset_ptr_t ds_ptr)
{
  const dataset_t & ds = *ds_ptr;

  const int pdim = (dir == DES)?(dim - 1):(dim + 1);

  auto cmp_dim = bind(cellid_int_pair_cmp<dim, dir>,ds_ptr,_1,_2);

  priority_queue<cellid_int_pair_t,cellid_int_pair_list_t,typeof(cmp_dim) >
      pq(cmp_dim);

  BOOST_FOREACH(cellid_t c, rng){pq.push(cellid_int_pair_t(c,1));}

  cellid_t f[40];

  while(pq.size() != 0 )
  {
    cellid_t c = pq.top().first;

    ASSERT(ds.getCellDim(c) == dim);

    int n = 0 ;

    do {n += pq.top().second; pq.pop();}
    while(pq.size() != 0 && pq.top().first == c);

    mfold.push_back(c);

    for(cellid_t *b = f,*e = f + ds.get_cets<dir>(c,f);b != e; ++b)
    {
      if(!ds.isCellCritical(*b))
      {
        cellid_t p = ds.getCellPairId(*b);

        if (p != c && (no_vcheck||ds.isCellVisited(*b)) && ds.getCellDim(p) == dim )
        {
          pq.push(make_pair(p,n));
        }
      }
    }
  }
}

/*---------------------------------------------------------------------------*/

template <eGDIR dir,typename range_t>
inline void collect_reachable_extrema
(mfold_t &mfold,const range_t &rng, dataset_ptr_t ds)
{
  const int dim = (dir == DES)?(gc_grid_dim):(0);

  cellid_t cets[40];

  cellid_list_t stk(boost::begin(rng),boost::end(rng));

  while(stk.size() != 0 )
  {
    cellid_t c = stk.back(); stk.pop_back();

    ASSERT(ds->getCellDim(c) == dim);

    mfold.push_back(c);

    for(cellid_t * b = cets, *e = cets + ds->get_cets<dir>(c,cets); b!=e;++b)
    {
      if(!ds->isCellCritical(*b))
      {
        cellid_t p = ds->getCellPairId(*b);
        if(ds->getCellDim(p) == dim && p!= c)
        {
          stk.push_back(p);
        }
      }
    }
  }
}

/*===========================================================================*/



/*===========================================================================*/

template<int dim,eGDIR dir>
inline bool is_required_cp(const mscomplex_t& msc,int i)
{
  static const int pdim = (dir == GDIR_DES)? (dim - 1):(dim +1);

  return msc.index(i) == dim && msc.m_rect.contains(msc.cellid(i))
      && (!msc.is_paired(i) || msc.index(msc.pair_idx(i)) == pdim);
}

template <int dim, eGDIR dir>
void computeConnections(mscomplex_ptr_t msc,dataset_ptr_t ds,
                        mscomplex_connector_t &msc_connector)
{
  ENSURES(!(dim == 1 && dir == ASC));
  
  if(dim == 2 && dir == DES)
  {
    cellid_list_t cps_1asc;

    br::copy(msc->cpno_range()|
             ba::filtered(bind(is_required_cp<1,GDIR_ASC>,boost::cref(*msc),_1))|
             ba::transformed(bind(&mscomplex_t::cellid,msc,_1)),
             std::back_inserter(cps_1asc));
    
    mark_reachable<1,GDIR_ASC,typeof(cps_1asc)>(cps_1asc,ds);

    //std::cout<<"Serial Mark Reachable Time: "<<e_serial-s_serial<<"\n";

    /*std::ofstream output;
    output.open("opencl_visited_foot.txt");
    for(int i=0; i<num_cells(ds->m_ext_rect); i++)
      {
	if(ds->isCellVisited(i_to_c(ds->m_ext_rect, i)))
	   output<<i_to_c(ds->m_ext_rect, i)<<"\n";
	   }*/
  }
  
  cellid_list_t cps;

  br::copy(msc->cpno_range()|
           ba::filtered(bind(is_required_cp<dim,dir>,boost::cref(*msc),_1))|
           ba::transformed(bind(&mscomplex_t::cellid,boost::cref(msc),_1)),
           std::back_inserter(cps));

  //Extra
  /*std::ofstream output; 
    output.open("Final_Datasets_MSC_Graphs/Heptane/Testing/opencl_pq_2-1-n_heptane_Jun1.txt");*/
  //Extra ends

#pragma omp parallel for
  for(int i = 0 ; i < cps.size(); ++i)
    {
      compute_inc_pairs_pq<dim,dir>(cps[i],ds,msc_connector);
    }
  
    //std::cout<<"Compute Connections For Loop: "<<endcc-startcc<<"\n";

}

/*---------------------------------------------------------------------------*/

template<eGDIR ex_dir>
void computeExtremaConnections(mscomplex_ptr_t msc,dataset_ptr_t ds,
                               mscomplex_connector_t &mscomplex_connector)
{
  const int   sad_dim = (ex_dir == DES)?(2):(1);
  const int   ex_dim  = (ex_dir == DES)?(3):(0);
  const eGDIR sad_dir = (ex_dir == DES)?(ASC):(DES);

  cellid_list_t sad_cps;
  
  br::copy(msc->cpno_range()|
           ba::filtered(bind(is_required_cp<sad_dim,sad_dir>,boost::cref(*msc),_1))|
           ba::transformed(bind(&mscomplex_t::cellid,boost::cref(msc),_1)),
           std::back_inserter(sad_cps));

  //double startec = omp_get_wtime();
  #pragma omp parallel for
  for(int i = 0; i < sad_cps.size(); ++i)
    {
    cellid_t c = sad_cps[i],e1,e2;

    get_adj_extrema<sad_dim>(c,e1,e2);

    if(ds->m_rect.contains(e1))
    {
      e1 = i_to_c2(ds->get_extrema_rect<ex_dir>(),ds->owner_extrema<ex_dir>()(e1/2));
      mscomplex_connector.connect_cells(c,e1,1);
    }

    if(ds->m_rect.contains(e2))
    {
      e2 = i_to_c2(ds->get_extrema_rect<ex_dir>(),ds->owner_extrema<ex_dir>()(e2/2));
      mscomplex_connector.connect_cells(c,e2,1);
    }
  }
  //double endec = omp_get_wtime();
  //std::cout<<"Extrema Connections For Loop: "<<endec-startec<<"\n"; 
}

/*---------------------------------------------------------------------------*/

template<eGDIR dir>
inline void extract_conns_prl(dataset_ptr_t ds, mscomplex_connector_t &msc_connector,
			      thrust::host_vector<int> &dimptr, std::vector<int> &c1_coords,
			      thrust::host_vector<short> &c2_coords,
			      thrust::host_vector<float> &c1xc2_conns)
{
  #pragma omp parallel for
  for(int dim = 1; dim < dimptr.size(); dim++) //dim = row or col
      {
	cellid_t c1, c2;
	c1 = i_to_c(ds->m_ext_rect, c1_coords[dim-1]);
	
	for(int i = dimptr[dim-1]; i < dimptr[dim]; i++)
	  {
	    c2 = cellid_t(c2_coords[3*i], c2_coords[(3*i)+1], c2_coords[(3*i)+2]);
	    msc_connector.connect_cells_prl<dir>(c1, c2, c1xc2_conns[i]);
	  }
      }
  
}

/*---------------------------------------------------------------------------*/
inline void compute_extrema_conns(dataset_ptr_t ds, mscomplex_ptr_t msc,
				  mscomplex_connector_t &msc_connector)
{
  //Storing s1xmin and s2xmax 
  thrust::host_vector<short> min_csr;
  thrust::host_vector<float> s1xmin_conns_csr;
  thrust::host_vector<int> s1xmin_rowptr;
  
  thrust::host_vector<short> s1_csc;
  thrust::host_vector<float> s1xmin_conns_csc;
  thrust::host_vector<int> s1xmin_colptr;

  thrust::host_vector<short> max_csr;
  thrust::host_vector<float> s2xmax_conns_csr;
  thrust::host_vector<int> s2xmax_rowptr;
  
  thrust::host_vector<short> s2_csc;
  thrust::host_vector<float> s2xmax_conns_csc;
  thrust::host_vector<int> s2xmax_colptr;

  cuda::owner_extrema(ds, msc, min_csr, s1xmin_conns_csr, s1xmin_rowptr,
		      s1_csc, s1xmin_conns_csc, s1xmin_colptr,
		      max_csr, s2xmax_conns_csr, s2xmax_rowptr,
		      s2_csc,  s2xmax_conns_csc, s2xmax_colptr);
	
  //Comment out to exclude extrema population runtimes
  //double s_expop = omp_get_wtime();
  
  //s1xmin
  extract_conns_prl<DES>(ds, msc_connector, s1xmin_rowptr,
			 msc->s1_coords, min_csr, s1xmin_conns_csr);

  //minxs1
  extract_conns_prl<ASC>(ds, msc_connector, s1xmin_colptr,
			 msc->min_coords, s1_csc, s1xmin_conns_csc);
  
  //s2xmax
  extract_conns_prl<ASC>(ds, msc_connector, s2xmax_rowptr,
			 msc->s2_coords, max_csr, s2xmax_conns_csr);

  //maxxs2
  extract_conns_prl<DES>(ds, msc_connector, s2xmax_colptr,
  msc->max_coords, s2_csc, s2xmax_conns_csc);

  //double e_expop = omp_get_wtime();
  //std::cout<<"Extrema Population Time (extract_conns_prl(DES+ASC+ASC+DES)): "<<e_expop-s_expop<<"\n";
  
}

/*---------------------------------------------------------------------------*/
/*Note: When extracting connections from Thrust host vectors, always typecast to
  int, gives faulty results with no trace otherwise.*/
/*---------------------------------------------------------------------------*/

inline void compute_sad_conns(dataset_ptr_t ds, mscomplex_ptr_t msc,
			      mscomplex_connector_t &msc_connector)
  {
    //Storing s1xs2 in CSR and CSC formats for fast population
    thrust::host_vector<short> s2_csr;
    thrust::host_vector<float> s_conns_csr;
    thrust::host_vector<int> s1xs2_rowptr;

    thrust::host_vector<short> s1_csc;    
    thrust::host_vector<float> s_conns_csc;
    thrust::host_vector<int> s1xs2_colptr;
    
    cuda::compute_saddle_pairs(ds, msc, s2_csr, s_conns_csr, s1xs2_rowptr,
			       s1_csc, s_conns_csc, s1xs2_colptr);

    //Comment out to exclude saddle population runtimes
    //double s_pop = omp_get_wtime();
    
    //s1xs2
    extract_conns_prl<ASC>(ds, msc_connector, s1xs2_rowptr,
      msc->s1_coords, s2_csr, s_conns_csr);

    //s2xs1
    extract_conns_prl<DES>(ds, msc_connector, s1xs2_colptr,
    msc->s2_coords, s1_csc, s_conns_csc);

    //double e_pop = omp_get_wtime();
    //std::cout<<"Path Counting Population Time (extract_conns_prl (ASC+DES)): "<<e_pop-s_pop<<"\n";
    
  }
  
/*---------------------------------------------------------------------------*/

void dataset_t::computeMsGraph(mscomplex_ptr_t msc)
{
  //double start = omp_get_wtime();
  //double s_asgr = omp_get_wtime();
  cuda::cuda_assign_gradient(shared_from_this(), msc);

  //double s_cps_pop = omp_get_wtime();
  mscomplex_connector_t msc_connector(msc);
  msc_connector.init(shared_from_this());
  //double e_cps_pop = omp_get_wtime();
  //std::cout<<"Populate Critical Points Time (func: msc_connector.init()): "<<e_cps_pop-s_cps_pop<<"\n";
  
  //double e_asgr = omp_get_wtime();
  
  //double s_sad = omp_get_wtime();
  compute_sad_conns(shared_from_this(), msc, msc_connector);
  //double e_sad = omp_get_wtime();
  
  //double s_ext = omp_get_wtime(); 
  compute_extrema_conns(shared_from_this(), msc, msc_connector);
  //double e_ext = omp_get_wtime();

  //For geometry extraction after simplification

  /*if(msc->prl_m_canc_list_0.size()>0)
  msc->resimplify_geom();*/

  /*std::ofstream output;
  rect_size_t size;
  output.open("Final_Datasets_MSC_Graphs/Tooth/cuda_tooth_m_owner_maxima_data.txt");
  size = get_extrema_rect<GDIR_DES>().span()/2 + 1;
  output<<"Size of m_owner_maxima.data(): "<<size[0]*size[1]*size[2]<<"\n";
  for(int i=0; i<size[0]*size[1]*size[2]; i++)
    output<<m_owner_maxima.data()[i]<<"\n";
  
  output.close();
  
  size = get_extrema_rect<GDIR_ASC>().span()/2 + 1;
  output.open("Final_Datasets_MSC_Graphs/Tooth/cuda_tooth_m_owner_minima_data.txt");
  output<<"Size of m_owner_minima.data(): "<<size[0]*size[1]*size[2]<<"\n";
  for(int i=0; i<size[0]*size[1]*size[2]; i++)
    output<<m_owner_minima.data()[i]<<"\n";
  
    output.close();
    
    //std::ofstream output;
  output.open("Final_Datasets_MSC_Graphs/Tooth/cuda_tooth_desc_extr.txt");
  for(int i = 0; i < msc->get_num_critpts(); ++i)
    {
      output<<i<<" : ";
       BOOST_FOREACH(int_int_t j, msc->m_des_conn[i]) 
	    {
	      output<<"("<<j.first<<","<<j.second<<")";
	    }
       output<<"\n";
       }

  output.close();

  output.open("Final_Datasets_MSC_Graphs/Tooth/cuda_tooth_asc_extr.txt");
  for(int i = 0; i < msc->get_num_critpts(); ++i)
    {
      output<<i<<" : ";
       BOOST_FOREACH(int_int_t j, msc->m_asc_conn[i]) 
	    {
	      output<<"("<<j.first<<","<<j.second<<")";
	    }
       output<<"\n";
       }

       output.close();*/
  
  //double end = omp_get_wtime();

  //std::cout<<"Assign Gradient + Critical Point Counting Time: "<<e_asgr-s_asgr<<"\n";
  //std::cout<<"Parallel BFS Time: "<<e_sad-s_sad<<"\n";
  //std::cout<<"New Saddle-Saddle Time (Mark Reachable + Counting + Population): "<<e_sad-s_sad<<"\n";
  //std::cout<<"Total CUDA Extrema Time (compute_extrema_conns): "<<e_ext-s_ext<<"\n";
  //std::cout<<"Total CUDA Extrema Time Without Population (compute_extrema_conns-extract_conns_prl): "<<e_ext-s_ext<<"\n";
  //std::cout<<"Total MSC Computation Time Including Saddle+Extrema Populations: "<<end-start<<"\n";

  //save_runt(s_ext, e_ext);
}

/*---------------------------------------------------------------------------*/

void dataset_t::getManifold
(mfold_t &mfold,const cellid_list_t &rng,int dim,eGDIR dir)
{
  if(dim == 0 && dir == ASC) collect_reachable_extrema<  ASC>      (mfold,rng,shared_from_this());
  if(dim == 1 && dir == ASC) collect_reachable_saddle <1,ASC,true> (mfold,rng,shared_from_this());
  if(dim == 2 && dir == ASC) collect_reachable_saddle <2,ASC,true> (mfold,rng,shared_from_this());

  if(dim == 1 && dir == DES) collect_reachable_saddle <1,DES,true> (mfold,rng,shared_from_this());
  if(dim == 2 && dir == DES) collect_reachable_saddle <2,DES,true> (mfold,rng,shared_from_this());
  if(dim == 3 && dir == DES) collect_reachable_extrema<  DES>      (mfold,rng,shared_from_this());
}

/*---------------------------------------------------------------------------*/

void dataset_t::compute_owner_grad()
{
  opencl::worker w;
  w.assign_gradient(shared_from_this());
  //cuda::cuda_assign_gradient(shared_from_this(),msc);

  if(opencl::is_gpu_context())
    w.owner_extrema(shared_from_this());
    //cuda::owner_extrema(shared_from_this());
    }

/*===========================================================================*/

}
