//NOTE: Including thrust header files after cuda headers causes a compile time error, unreported on SO as of now. Include them at the top to avoid the error.

#include<thrust/host_vector.h>
#include <thrust/device_malloc.h>
#include<thrust/device_vector.h>
#include <thrust/copy.h>
#include<thrust/scan.h>
#include "cutil.h"
#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>

//#define NUM_THREADS_PER_BLOCK 256
//#define NUM_BLOCKS 32
//#define NUM_THREADS 8192

#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64
#define binary_op(a, b) ((a) +(b))

int* d_cp_array = 0;

int* d_cp_array_asc = 0;
int* d_cp_cells = 0;

typedef float func_t;
typedef unsigned char flag_t;

 //Performance
 float elapsed=0;
 cudaEvent_t start, stop;

 int* d_cp_offset_buf  = 0;
 short* d_cp_cellid_buf = 0;
 short* d_cp_vertid_buf = 0;
 char* d_cp_index_buf = 0;
 int* d_cp_pair_idx_buf = 0;
 func_t* d_cp_func_buf = 0;

__global__ void count_cps(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* cp_array, int* cp_cells)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();
  int n_cp = 0;

  for(int i=tid; i<num_cells; i+=num_thds)
  {
   cell_t c = ext_rect.i_to_c(i);
   flag_t fg;
   surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

   if(is_paired(fg))
   {
   //cp_cells[ext_rect.c_to_i_asc(c)] = 0;
   continue;
   }

   //cp_cells[ext_rect.c_to_i_asc(c)] = 1;
   n_cp++;
  }
 
 cp_array[tid] = n_cp;
}

__global__ void count_cps_asc(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* cp_array_asc)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();
  int n_cp_asc = 0;

  int chunksize = num_cells/num_thds;
  int limit = (tid+1)*chunksize;

  for(int i=tid*chunksize; i<limit; i++)
  {
   
   if(tid == (num_thds-1))
   {
    for(int j=i; j<num_cells; j++)
    {
     cell_t c = ext_rect.i_to_c_asc(j);
     flag_t fg;
     surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
   
     if(is_paired(fg))
     continue;

     n_cp_asc++;
    }
   break;
   }

   cell_t c = ext_rect.i_to_c_asc(i);
   flag_t fg;
   surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
   
   if(is_paired(fg))
   continue;

   n_cp_asc++;
  }
 
 cp_array_asc[tid] = n_cp_asc;
}

__global__ void count_boundary_cps(rect_t bnd_rect, cell_t bnd_dir, cudaSurfaceObject_t flag_surface, int* cp_array)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;
 
  int num_cells = bnd_rect.num_cells();
  
  int n_cp = cp_array[tid]; 
  
  for(int i = tid ; i < num_cells; i += num_thds)
  {
   cell_t c = bnd_rect.i_to_c(i);

   flag_t fg;
   surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

   if(!is_paired(fg))
     continue;

   cell_t p = flag_to_pair(c,fg);
   if(!(p+bnd_dir==c) && (!(c+bnd_dir==p)))
     continue;

   n_cp +=2;

  }

 cp_array[tid] = n_cp;
}


__forceinline__ __device__ void scan_local_threads(int* array)
{ 
 __syncthreads();

 int val = array[threadIdx.x];

 if (NUM_THREADS_PER_BLOCK > 1) { if(threadIdx.x >= 1) { int tmp = array[threadIdx.x - 1]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 2) { if(threadIdx.x >= 2) { int tmp = array[threadIdx.x - 2]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 4) { if(threadIdx.x >= 4) { int tmp = array[threadIdx.x - 4]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 8) { if(threadIdx.x >= 8) { int tmp = array[threadIdx.x - 8]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 16) { if(threadIdx.x >= 16) { int tmp = array[threadIdx.x - 16]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 32) { if(threadIdx.x >= 32) { int tmp = array[threadIdx.x - 32]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 64) { if(threadIdx.x >= 64) { int tmp = array[threadIdx.x - 64]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 128) { if(threadIdx.x >= 128) { int tmp = array[threadIdx.x - 128]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_THREADS_PER_BLOCK > 256) { if(threadIdx.x >= 256) { int tmp = array[threadIdx.x - 256]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}
  
 if (NUM_THREADS_PER_BLOCK > 512) { if(threadIdx.x >= 512) { int tmp = array[threadIdx.x - 512]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}
}

__forceinline__ __device__ void scan_num_groups(int* array)
{
 __syncthreads();

 int val = array[threadIdx.x];

 if (NUM_BLOCKS > 1) { if(threadIdx.x >= 1) { int tmp = array[threadIdx.x - 1]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 2) { if(threadIdx.x >= 2) { int tmp = array[threadIdx.x - 2]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 4) { if(threadIdx.x >= 4) { int tmp = array[threadIdx.x - 4]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 8) { if(threadIdx.x >= 8) { int tmp = array[threadIdx.x - 8]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 16) { if(threadIdx.x >= 16) { int tmp = array[threadIdx.x - 16]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 32) { if(threadIdx.x >= 32) { int tmp = array[threadIdx.x - 32]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 64) { if(threadIdx.x >= 64) { int tmp = array[threadIdx.x - 64]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 128) { if(threadIdx.x >= 128) { int tmp = array[threadIdx.x - 128]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (NUM_BLOCKS > 256) { if(threadIdx.x >= 256) { int tmp = array[threadIdx.x - 256]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}
  
 if (NUM_BLOCKS > 512) { if(threadIdx.x >= 512) { int tmp = array[threadIdx.x - 512]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

}

__global__ void scan_local_sums(int* group_sums, int* cp_array)
{ 
 __shared__ int sarray[NUM_THREADS_PER_BLOCK+1];
 sarray[NUM_THREADS_PER_BLOCK] = 0;
 
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 sarray[threadIdx.x] = cp_array[tid];

 scan_local_threads(sarray);
 
 //Left shift operation shifting 256 to 0, hence 0th element of each block now holds the sum of the entire block
 int ridx = (threadIdx.x - 1 + NUM_THREADS_PER_BLOCK + 1) % (NUM_THREADS_PER_BLOCK+1);

 cp_array[tid] = sarray[ridx];

 if(threadIdx.x == 0)
 group_sums[blockIdx.x] = sarray[NUM_THREADS_PER_BLOCK-1];

}

__global__ void scan_group_sums(int* group_sums)
{
 __shared__ int sarray[NUM_BLOCKS];

 sarray[threadIdx.x] = group_sums[threadIdx.x];

 scan_num_groups(sarray);

 group_sums[threadIdx.x] = sarray[threadIdx.x];

}

__global__ void scan_update_sums(int* group_sums, int* cp_array)
{
 if(blockIdx.x != 0)
    cp_array[blockIdx.x*blockDim.x + threadIdx.x] += group_sums[blockIdx.x-1];
}

__global__ void inclusive_scan(int* group_sums, int* cp_array)
{ 
 __shared__ int sarray[NUM_THREADS_PER_BLOCK+1];
 sarray[NUM_THREADS_PER_BLOCK] = 0;
 
 int tid = blockIdx.x*blockDim.x + threadIdx.x;
 sarray[threadIdx.x] = cp_array[tid];

 scan_local_threads(sarray);

 cp_array[tid] = sarray[threadIdx.x];

 if(threadIdx.x == 0)
 group_sums[blockIdx.x] = sarray[NUM_THREADS_PER_BLOCK-1];

}

__forceinline__ __device__ void scan_grid_groups(int* array, int num_blocks)
{
 __syncthreads();

 int val = array[threadIdx.x];

 if (num_blocks > 1) { if(threadIdx.x >= 1) { int tmp = array[threadIdx.x - 1]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 2) { if(threadIdx.x >= 2) { int tmp = array[threadIdx.x - 2]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 4) { if(threadIdx.x >= 4) { int tmp = array[threadIdx.x - 4]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 8) { if(threadIdx.x >= 8) { int tmp = array[threadIdx.x - 8]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 16) { if(threadIdx.x >= 16) { int tmp = array[threadIdx.x - 16]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 32) { if(threadIdx.x >= 32) { int tmp = array[threadIdx.x - 32]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 64) { if(threadIdx.x >= 64) { int tmp = array[threadIdx.x - 64]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 128) { if(threadIdx.x >= 128) { int tmp = array[threadIdx.x - 128]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}

 if (num_blocks > 256) { if(threadIdx.x >= 256) { int tmp = array[threadIdx.x - 256]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}
  
 if (num_blocks > 512) { if(threadIdx.x >= 512) { int tmp = array[threadIdx.x - 512]; val = binary_op(tmp, val); } __syncthreads(); array[threadIdx.x] = val; __syncthreads();}
}

__global__ void scan_grid_cells(int* group_sums, dim3 grid_scan)
{
 extern __shared__ int sarray[];

 sarray[threadIdx.x] = group_sums[threadIdx.x];

 scan_grid_groups(sarray, grid_scan.x);

 group_sums[threadIdx.x] = sarray[threadIdx.x];

}

__forceinline__ __device__ cell_t get_max_vert (const cell_t c, cudaSurfaceObject_t flag_surface)
{
  cell_t v = c;
  flag_t fg;

  switch(cell_dim(c))
  {
    
    case 3: {
             surf3Dread(&fg, flag_surface, v.x*sizeof(flag_t), v.y, v.z);
             v = flag_to_mxfct(v,fg);
            }
    case 2: {
             surf3Dread(&fg, flag_surface, v.x*sizeof(flag_t), v.y, v.z);
             v = flag_to_mxfct(v,fg);
            }
    case 1: { 
             surf3Dread(&fg, flag_surface, v.x*sizeof(flag_t), v.y, v.z);
             v = flag_to_mxfct(v,fg);
            }
  }
  return v;
}

__global__ void save_cps_asc(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* cp_array_asc, short* cp_map)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();
  
  int pos;

  if(tid!=0)
  pos = cp_array_asc[tid-1];
  else
  pos = 0;

  int chunksize = num_cells/num_thds;
  int limit = (tid+1)*chunksize;

  for(int i=tid*chunksize; i<limit; i++)
  {
   
     if(tid == (num_thds-1))
    {
      for(int j=i; j<num_cells; j++)
      {
       cell_t c = ext_rect.i_to_c_asc(j);
       flag_t fg;
       surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
   
       if(is_paired(fg))
       continue;

       cp_map[3*pos+0] = c.x;
       cp_map[3*pos+1] = c.y;
       cp_map[3*pos+2] = c.z;

       pos++;
      }
    break;
    }

    cell_t c = ext_rect.i_to_c_asc(i);

    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(is_paired(fg))
      continue;

    cp_map[3*pos+0] = c.x;
    cp_map[3*pos+1] = c.y;
    cp_map[3*pos+2] = c.z;

    pos++;
    
  }

}


__global__ void save_cps(const rect_t ext_rect, short* cp_cellid_buf, char* cp_index_buf, int* cp_pair_idx_buf, short* cp_vertid_buf, func_t* cp_func_buf, cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface, int* cp_array, int* cp_cells)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();
  int n_cp = cp_array[tid];
  
  for(int i=tid; i < num_cells; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(i); 

    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(is_paired(fg))
    { 
      cp_cells[ext_rect.c_to_i_asc(c)] = -1;
      continue;
    }

    cp_cells[ext_rect.c_to_i_asc(c)] = n_cp;   
 
    cell_t v = get_max_vert(c, flag_surface);
    func_t func = tex3D<func_t>(func_tex, v.x/2, v.y/2, v.z/2);

    cp_cellid_buf[3*n_cp + 0] = c.x;
    cp_cellid_buf[3*n_cp + 1] = c.y;
    cp_cellid_buf[3*n_cp + 2] = c.z;

    cp_vertid_buf[3*n_cp + 0] = v.x;
    cp_vertid_buf[3*n_cp + 1] = v.y;
    cp_vertid_buf[3*n_cp + 2] = v.z;

    cp_index_buf[n_cp]        = cell_dim(c);
    cp_pair_idx_buf[n_cp]     = -1;
    cp_func_buf[n_cp]         = func;

    n_cp++;
  }
  cp_array[tid] = n_cp;
}

__global__ void save_boundary_cps(const rect_t bnd_rect, cell_t bnd_dir, int* cp_offset_buf, short* cp_cellid_buf, char* cp_index_buf, int* cp_pair_idx_buf, short* cp_vertid_buf, func_t* cp_func_buf, cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface, int* cp_array)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int num_cells = bnd_rect.num_cells();
  
  int n_cp = cp_array[tid];

  for(int i = tid; i < num_cells; i += num_thds)
  {
    cell_t c = bnd_rect.i_to_c(i);

    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(!is_paired(fg))
      continue;

    cell_t p = flag_to_pair(c,fg);

    if(!(p+bnd_dir==c) && !(c+bnd_dir==p))
      continue;

    cell_t v  = get_max_vert(c, flag_surface);
    func_t func = tex3D<func_t>(func_tex, v.x/2, v.y/2, v.z/2);

    cp_cellid_buf[3*n_cp + 0] = c.x;
    cp_cellid_buf[3*n_cp + 1] = c.y;
    cp_cellid_buf[3*n_cp + 2] = c.z;

    cp_vertid_buf[3*n_cp + 0] = v.x;
    cp_vertid_buf[3*n_cp + 1] = v.y;
    cp_vertid_buf[3*n_cp + 2] = v.z;

    cp_index_buf[n_cp]        = cell_dim(c);
    cp_pair_idx_buf[n_cp]     = n_cp+1;
    cp_func_buf[n_cp]         = func;

    n_cp++;

    cp_cellid_buf[3*n_cp + 0] = p.x;
    cp_cellid_buf[3*n_cp + 1] = p.y;
    cp_cellid_buf[3*n_cp + 2] = p.z;

    cp_vertid_buf[3*n_cp + 0] = v.x;
    cp_vertid_buf[3*n_cp + 1] = v.y;
    cp_vertid_buf[3*n_cp + 2] = v.z;

    cp_index_buf[n_cp]        = cell_dim(p);
    cp_pair_idx_buf[n_cp]     = n_cp-1;
    cp_func_buf[n_cp]         = func;

    n_cp++;
  }

  cp_array[tid] = n_cp;
}

extern "C" void count_cps(short* erptr, cudaSurfaceObject_t &flag_surface)
{
  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);

  int num_threads = NUM_THREADS_PER_BLOCK*NUM_BLOCKS;
  
  cutilSafeCall(cudaMalloc((void**)&d_cp_array, num_threads*sizeof(int)));
   
  cutilSafeCall(cudaMalloc((void**)&d_cp_cells, ext_rect.num_cells()*sizeof(int)));

  cutilSafeCall(cudaMalloc((void**)&d_cp_array_asc, num_threads*sizeof(int)));

  dim3 block(512,1);
  dim3 grid(64,1);

  count_cps<<<grid,block>>>(ext_rect, flag_surface, d_cp_array, d_cp_cells);
  count_cps_asc<<<grid,block>>>(ext_rect, flag_surface, d_cp_array_asc);
  cudaDeviceSynchronize();

  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) 
  {
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
  }

}

extern "C" void count_boundarycps_cuda(short* bnd, short* bnd_dir, cudaSurfaceObject_t &flag_surface)
{
  dim3 block(512,1);
  dim3 grid(64,1);
  rect_t bnd_rect(bnd[0],bnd[1],bnd[2],bnd[3],bnd[4],bnd[5]);
  cell_t bnddir = {bnd_dir[0], bnd_dir[1], bnd_dir[2]};
  count_boundary_cps<<<grid,block>>>(bnd_rect, bnddir, flag_surface, d_cp_array);
}

extern "C" void scan_cp_cuda(short* erptr, int* h_group_sums)
{
 dim3 block(512,1);
 dim3 grid(64,1);
 rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);

 int* d_group_sums = 0;
 cutilSafeCall(cudaMalloc((void**)&d_group_sums, NUM_BLOCKS*sizeof(int)));

 scan_local_sums<<<grid,block>>>(d_group_sums, d_cp_array);
 //scan_group_sums<<<grid,grid>>>(d_group_sums);

 //Inclusive scan
 //thrust::device_ptr<int> thrust_dgroupsums = thrust::device_pointer_cast(d_group_sums);
 //thrust::inclusive_scan(thrust_dgroupsums, thrust_dgroupsums+NUM_BLOCKS, thrust_dgroupsums);
 //OR
 //thrust::inclusive_scan(thrust::device, d_cp_array, d_cp_array+(NUM_THREADS_PER_BLOCK*NUM_BLOCKS), d_cp_array);
 thrust::inclusive_scan(thrust::device, d_group_sums, d_group_sums+NUM_BLOCKS, d_group_sums);

 scan_update_sums<<<grid,block>>>(d_group_sums, d_cp_array);
 cudaDeviceSynchronize();

 thrust::inclusive_scan(thrust::device, d_cp_array_asc, d_cp_array_asc+(NUM_THREADS_PER_BLOCK*NUM_BLOCKS), d_cp_array_asc);
  
 //thrust::inclusive_scan(thrust::device, d_cp_cells, d_cp_cells+num_cells, d_cp_cells);

 cutilSafeCall(cudaMemcpy(h_group_sums, d_group_sums, sizeof(int)*NUM_BLOCKS, cudaMemcpyDeviceToHost));
 
 cudaFree(d_group_sums);

  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) 
  {
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
  } 

}

extern "C" void save_cps_cuda(short* erptr, int num_cps, short* h_cp_cellid_buf, short* h_cp_vertid_buf, char* h_cp_index_buf, int* h_cp_pair_idx_buf, float* h_cp_func_buf, cudaTextureObject_t &func_tex, cudaSurfaceObject_t &flag_surface, short* h_cp_map, int* h_cp_idx)
{
 dim3 block(512,1);
 dim3 grid(64,1);
 
 rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);

 cutilSafeCall(cudaMalloc((void**)&d_cp_cellid_buf, 3*num_cps*sizeof(short)));
 cutilSafeCall(cudaMalloc((void**)&d_cp_index_buf, num_cps*sizeof(char)));
 cutilSafeCall(cudaMalloc((void**)&d_cp_pair_idx_buf, num_cps*sizeof(int)));
 cutilSafeCall(cudaMalloc((void**)&d_cp_vertid_buf, 3*num_cps*sizeof(short)));
 cutilSafeCall(cudaMalloc((void**)&d_cp_func_buf, num_cps*sizeof(func_t)));
 
 short* d_cp_map = 0;
 cutilSafeCall(cudaMalloc((void**)&d_cp_map, 3*num_cps*sizeof(short)));

 save_cps<<<grid,block>>>(ext_rect, d_cp_cellid_buf, d_cp_index_buf, d_cp_pair_idx_buf, d_cp_vertid_buf, d_cp_func_buf, func_tex, flag_surface, d_cp_array, d_cp_cells);

 save_cps_asc<<<grid,block>>>(ext_rect, flag_surface, d_cp_array_asc, d_cp_map);

 cudaDeviceSynchronize();

 const int num_cells = ext_rect.num_cells();

 thrust::device_vector<int> d_copyif_result(num_cps);
 thrust::copy_if(d_cp_cells, d_cp_cells+num_cells, d_copyif_result.begin(), is_not_negative());
 thrust::copy(&d_copyif_result[0], &d_copyif_result[0]+num_cps, h_cp_idx);

 cutilSafeCall(cudaMemcpy(h_cp_cellid_buf, d_cp_cellid_buf, 3*num_cps*sizeof(short), cudaMemcpyDeviceToHost));
 cutilSafeCall(cudaMemcpy(h_cp_index_buf, d_cp_index_buf, num_cps*sizeof(char), cudaMemcpyDeviceToHost));
 cutilSafeCall(cudaMemcpy(h_cp_pair_idx_buf, d_cp_pair_idx_buf, num_cps*sizeof(int), cudaMemcpyDeviceToHost));
 cutilSafeCall(cudaMemcpy(h_cp_vertid_buf, d_cp_vertid_buf, 3*num_cps*sizeof(short), cudaMemcpyDeviceToHost)); 
 cutilSafeCall(cudaMemcpy(h_cp_func_buf, d_cp_func_buf, num_cps*sizeof(func_t), cudaMemcpyDeviceToHost));

 cutilSafeCall(cudaMemcpy(h_cp_map, d_cp_map, 3*num_cps*sizeof(short), cudaMemcpyDeviceToHost));
 //cutilSafeCall(cudaMemcpy(h_cp_idx, d_cp_idx, num_cps*sizeof(int), cudaMemcpyDeviceToHost));

  //cudaFree(d_cp_idx); 
  cudaFree(d_cp_map);
 
 cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) 
  {
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
  } 
}

extern "C" void save_boundarycps_cuda(short* bnd, short* bnd_dir, cudaTextureObject_t &func_tex, cudaSurfaceObject_t &flag_surface)
{
  //dim3 block(256,1);
  //dim3 grid(32,1);
  dim3 block(512,1);
  dim3 grid(64,1);
  rect_t bnd_rect(bnd[0],bnd[1],bnd[2],bnd[3],bnd[4],bnd[5]);
  cell_t bnddir = {bnd_dir[0], bnd_dir[1], bnd_dir[2]};
  save_boundary_cps<<<grid,block>>>(bnd_rect, bnddir, d_cp_offset_buf, d_cp_cellid_buf, d_cp_index_buf, d_cp_pair_idx_buf, d_cp_vertid_buf, d_cp_func_buf, func_tex, flag_surface, d_cp_array);

 cudaDeviceSynchronize();

 cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) 
  {
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
  } 
}

extern "C" void free_cp_arrays()
{
  cudaFree(d_cp_cellid_buf);
  cudaFree(d_cp_index_buf);
  cudaFree(d_cp_pair_idx_buf);
  cudaFree(d_cp_vertid_buf);
  cudaFree(d_cp_func_buf);

  cudaFree(d_cp_cells);
  cudaFree(d_cp_array);
  cudaFree(d_cp_array_asc);
}



