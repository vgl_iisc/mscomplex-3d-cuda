#include<utl.h>
#include<grid.h>

#include<grid_dataset.h>
#include<grid_mscomplex.h>
#include <cuda_runtime.h>

namespace grid
{
namespace cuda
{
  void cuda_assign_gradient(dataset_ptr_t ds, mscomplex_ptr_t msc=mscomplex_ptr_t());

  void count_criticalpts(short* erptr, rect_t rct, rect_t ext, cudaSurfaceObject_t &flag_surface);

  void save_criticalpts(short* extrectptr, mscomplex_ptr_t msc, dataset_ptr_t ds,
			int num_cps, rect_t rct, rect_t ext,
			cudaTextureObject_t &func_tex, cudaSurfaceObject_t &flag_surface);

  void owner_extrema(dataset_ptr_t ds, mscomplex_ptr_t msc,
		     thrust::host_vector<short> &min_csr, thrust::host_vector<float> &s1xmin_conns_csr,
		     thrust::host_vector<int> &s1xmin_rowptr, thrust::host_vector<short> &s1_csc,
		     thrust::host_vector<float> &s1xmin_conns_csc, thrust::host_vector<int> &s1xmin_colptr,
		     thrust::host_vector<short> &max_csr, thrust::host_vector<float> &s2xmax_conns_csr,
		     thrust::host_vector<int> &s2xmax_rowptr, thrust::host_vector<short> &s2_csc,
		     thrust::host_vector<float> &s2xmax_conns_csc, thrust::host_vector<int> &s2xmax_colptr);

  void compute_saddle_pairs(dataset_ptr_t ds, mscomplex_ptr_t msc, thrust::host_vector<short> &s2_csr,
			    thrust::host_vector<float> &s_conns_csr, thrust::host_vector<int> &s1xs2_rowptr,
			    thrust::host_vector<short> &s1_csc, thrust::host_vector<float> &s_conns_csc,
			    thrust::host_vector<int> &s1xs2_colptr);
}
}
