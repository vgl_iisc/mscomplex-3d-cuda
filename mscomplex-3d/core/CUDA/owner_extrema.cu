#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include<thrust/sort.h>
#include<thrust/execution_policy.h>

#include <cusparse.h>

#include "cutil.h"
#include "matrix_init.cu.h"

#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>

#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

enum eDIM
{
  EXT_MIN = 0,
  EXT_MAX = 3
};

const eDIM MIN = EXT_MIN;
const eDIM MAX = EXT_MAX;

int* d_min_scan = 0;
int* d_max_scan = 0;

int* d_min_coords = 0;
int* d_max_coords = 0;

typedef float func_t;
typedef unsigned char flag_t;

int* num_max = new int[1];
int* num_min = new int[1];

extern "C" void owner_extrema_cuda(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &flag_surface, int* h_ex_own, const int &num_ext,
				int* d_ext_coords, int* d_ext_scan, thrust::device_vector<int>& sxe, const eDIM& dim, 
				thrust::host_vector<short> &csr_coords, thrust::host_vector<float> &csr_conns, 
				thrust::host_vector<int> &csr_rowptr, thrust::host_vector<short> &csc_coords, 
				thrust::host_vector<float> &csc_conns, thrust::host_vector<int> &csc_colptr);

extern "C" void get_sad_max(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &d_ext, thrust::device_vector<int>& s2xmax, 
				const int& num_max, int* d_max_coords, int* d_max_scan, const eDIM& dim,
				thrust::host_vector<short> &h_max_csr, thrust::host_vector<float> &h_conns_csr, 
				thrust::host_vector<int> &h_rowptr, thrust::host_vector<short> &h_s2_csc, 
				thrust::host_vector<float> &h_conns_csc, thrust::host_vector<int> &h_colptr);

extern "C" void get_sad_min(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &d_ext, thrust::device_vector<int>& s1xmin, 
				const int& num_min, int* d_min_coords, int* d_min_scan, const eDIM& dim, 
				thrust::host_vector<short> &h_min_csr, thrust::host_vector<float> &h_conns_csr, 
				thrust::host_vector<int> &h_rowptr, thrust::host_vector<short> &h_s1_csc, 
				thrust::host_vector<float> &h_conns_csc, thrust::host_vector<int> &h_colptr);

__global__ void init_propagate(rect_t ex_rect, cudaSurfaceObject_t flag_surface, cudaSurfaceObject_t ex_own_buf, eDIM dim)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int num_cells = ex_rect.num_cells2();

  for(int i = tid; i < num_cells; i += num_thds)
  {
    cell_t c = ex_rect.i_to_c2(i);

    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    cell_t o = c;

    if(!is_critical(fg))
    {
      cell_t p = flag_to_pair(c,fg);
      o  = p + p-c;
    }

    //ex_own_buf[ex_rect.c_to_i2(c)] = ex_rect.c_to_i2(o);

    int val = ex_rect.c_to_i2(o);

    if(dim==MAX)
    surf3Dwrite(val, ex_own_buf, ((c.x-1)/2)*sizeof(int), ((c.y-1)/2), ((c.z-1)/2));
    else
    surf3Dwrite(val, ex_own_buf, (c.x/2)*sizeof(int), (c.y/2), (c.z/2));
  }
}

__global__ void propagate(rect_t ex_rect, cudaSurfaceObject_t ex_own_buf1, cudaSurfaceObject_t ex_own_buf2, int* is_updated, eDIM dim)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;
  
  int num_cells = ex_rect.num_cells2();

  int updated = 0;

  for(int i = tid; i < num_cells; i += num_thds)
  {
    //int  o = ex_own_buf1[i];
    //int oo = ex_own_buf1[o];
    //ex_own_buf2[i] = oo;

    int o, oo;
    cell_t c  = ex_rect.i_to_c2(i);
    
    if(dim==MAX)
    {
    surf3Dread(&o, ex_own_buf1, ((c.x-1)/2)*sizeof(int), ((c.y-1)/2), ((c.z-1)/2));

    cell_t o1 = ex_rect.i_to_c2(o);
    surf3Dread(&oo, ex_own_buf1, ((o1.x-1)/2)*sizeof(int), ((o1.y-1)/2), ((o1.z-1)/2));
    surf3Dwrite(oo, ex_own_buf2, ((c.x-1)/2)*sizeof(int), ((c.y-1)/2), ((c.z-1)/2));
    }

    else
    {
    surf3Dread(&o, ex_own_buf1, (c.x/2)*sizeof(int), (c.y/2), (c.z/2));

    cell_t o1 = ex_rect.i_to_c2(o);
    surf3Dread(&oo, ex_own_buf1, (o1.x/2)*sizeof(int), (o1.y/2), (o1.z/2));
    surf3Dwrite(oo, ex_own_buf2, (c.x/2)*sizeof(int), (c.y/2), (c.z/2));
    }

    if(o != oo)
      updated = 1;
  }

  if(updated == 1)
    *is_updated = 1;
}

__global__ void update_cp_cell_to_cp_no(rect_t rct, rect_t ex_rect, short* cp_cellid_buf, int num_cps, int* ex_own_buf)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int ex_dim = cell_dim(ex_rect.lc);

  for( int i = tid; i < num_cps; i += num_thds)
  {
    cell_t c;

    c.x = cp_cellid_buf[3*i+0];
    c.y = cp_cellid_buf[3*i+1];
    c.z = cp_cellid_buf[3*i+2];

    if(cell_dim(c) != ex_dim)
      continue;

    if(!rct.contains(c))
      continue;

    ex_own_buf[ex_rect.c_to_i2(c)] = i;
  }
}

__global__ void init_update_to_surv_cp_no(rect_t rct, rect_t ex_rect, short* cp_cellid_buf, int* surv_cp_no_buf, int num_cps, int* ex_own_buf)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int ex_dim = cell_dim(ex_rect.lc);

  for(int i = tid; i < num_cps; i += num_thds)
  {
    cell_t c;

    c.x = cp_cellid_buf[3*i+0];
    c.y = cp_cellid_buf[3*i+1];
    c.z = cp_cellid_buf[3*i+2];

    if(cell_dim(c) != ex_dim)
      continue;

    if(!rct.contains(c))
      continue;

    ex_own_buf[ex_rect.c_to_i2(c)] = -surv_cp_no_buf[i];
  }
}

__global__ void update_to_surv_cp_no(rect_t ex_rect, int* ex_own_buf1, int* ex_own_buf2)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int num_cells = ex_rect.num_cells2();

  for(int i = tid; i < num_cells; i += num_thds)
  {
    cell_t c  = ex_rect.i_to_c2(i);

    int o  = ex_own_buf1[i];
    int oo = (o>=0)?(-ex_own_buf1[o]):(-o);

    ex_own_buf2[i] = oo;
  }

}


extern "C" void extrema_wrapper(short* max_rptr, short* min_rptr, short* erptr, cudaSurfaceObject_t &flag_surface, 
				int* h_own_max, int* h_own_min, const int &num_1s, 
				const int &num_2s, const int &num_max, const int &num_min,
				thrust::host_vector<short> &h_min_csr, thrust::host_vector<float> &h_s1xmin_conns_csr, 
				thrust::host_vector<int> &h_s1xmin_rowptr, thrust::host_vector<short> &h_s1_csc,
				thrust::host_vector<float> &h_s1xmin_conns_csc, thrust::host_vector<int> &h_s1xmin_colptr,
				thrust::host_vector<short> &h_max_csr, thrust::host_vector<float> &h_s2xmax_conns_csr,
		     		thrust::host_vector<int> &h_s2xmax_rowptr, thrust::host_vector<short> &h_s2_csc,
		     		thrust::host_vector<float> &h_s2xmax_conns_csc, thrust::host_vector<int> &h_s2xmax_colptr) 
{
   rect_t max_rect(max_rptr[0],max_rptr[1],max_rptr[2],max_rptr[3],max_rptr[4],max_rptr[5]);
   rect_t min_rect(min_rptr[0],min_rptr[1],min_rptr[2],min_rptr[3],min_rptr[4],min_rptr[5]);
   rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);

   thrust::device_vector<int> d_sad1xmin(num_1s*2, -1);
   thrust::device_vector<int> d_sad2xmax(num_2s*2, -1);

   //Maxima - 2-saddle connections
   owner_extrema_cuda(max_rect, ext_rect, flag_surface, h_own_max, num_max, d_max_coords, d_max_scan, d_sad2xmax, EXT_MAX, 
			h_max_csr, h_s2xmax_conns_csr, h_s2xmax_rowptr, h_s2_csc, h_s2xmax_conns_csc, h_s2xmax_colptr);

   //Minima - 1-saddle connections
   owner_extrema_cuda(min_rect, ext_rect, flag_surface, h_own_min, num_min, d_min_coords, d_min_scan, d_sad1xmin, EXT_MIN, 
			h_min_csr, h_s1xmin_conns_csr, h_s1xmin_rowptr, h_s1_csc, h_s1xmin_conns_csc, h_s1xmin_colptr);

}


extern "C" void owner_extrema_cuda(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &flag_surface, int* h_ex_own, const int &num_ext,
				int* d_ext_coords, int* d_ext_scan, thrust::device_vector<int>& sxe, const eDIM& dim, 
				thrust::host_vector<short> &csr_coords, thrust::host_vector<float> &csr_conns, 
				thrust::host_vector<int> &csr_rowptr, thrust::host_vector<short> &csc_coords, 
				thrust::host_vector<float> &csc_conns, thrust::host_vector<int> &csc_colptr)
{
  int num_ex = extrema_rect.num_cells2();
  int* d_is_updated_buf;

  cudaChannelFormatDesc ownext_channel_desc = cudaCreateChannelDesc<int>();
  cell_t ex_span = (extrema_rect.uc - extrema_rect.lc)/2 + 1;
  cudaExtent ex_vol_extent =  make_cudaExtent(ex_span.x, ex_span.y, ex_span.z);

  cudaArray *d_ownbuf1 = 0;
  cudaArray *d_ownbuf2 = 0;
  cudaSurfaceObject_t ownbuf1 = 0;
  cudaSurfaceObject_t ownbuf2 = 0;

  cutilSafeCall(cudaMalloc3DArray(&d_ownbuf1, &ownext_channel_desc, ex_vol_extent, cudaArraySurfaceLoadStore));
  cutilSafeCall(cudaMalloc3DArray(&d_ownbuf2, &ownext_channel_desc, ex_vol_extent, cudaArraySurfaceLoadStore));

  cutilSafeCall(cudaMalloc((void**)&d_is_updated_buf, sizeof(int)));
  
  cudaResourceDesc ownbuf1_surfResDes;
  memset(&ownbuf1_surfResDes, 0, sizeof(cudaResourceDesc));
  ownbuf1_surfResDes.resType            = cudaResourceTypeArray;
  ownbuf1_surfResDes.res.array.array    = d_ownbuf1;
  cutilSafeCall(cudaCreateSurfaceObject(&ownbuf1, &ownbuf1_surfResDes));

  cudaResourceDesc ownbuf2_surfResDes;
  memset(&ownbuf2_surfResDes, 0, sizeof(cudaResourceDesc));
  ownbuf2_surfResDes.resType            = cudaResourceTypeArray;
  ownbuf2_surfResDes.res.array.array    = d_ownbuf2;
  cutilSafeCall(cudaCreateSurfaceObject(&ownbuf2, &ownbuf2_surfResDes));
  
  dim3 block(512,1);
  dim3 grid(64,1);
  init_propagate<<<grid,block>>>(extrema_rect, flag_surface, ownbuf1, dim);
  cudaDeviceSynchronize();

  int h_is_updated;

  do
  {
  h_is_updated = 0;

  cutilSafeCall(cudaMemcpy(d_is_updated_buf, &h_is_updated, sizeof(int), cudaMemcpyHostToDevice));
  propagate<<<grid,block>>>(extrema_rect, ownbuf1, ownbuf2, d_is_updated_buf, dim);
  cudaDeviceSynchronize();

  cutilSafeCall(cudaMemcpy(&h_is_updated, d_is_updated_buf, sizeof(int), cudaMemcpyDeviceToHost));  
  std::swap(d_ownbuf1, d_ownbuf2);
  cudaSurfaceObject_t tempsurf = ownbuf1;
  ownbuf1 = ownbuf2;
  ownbuf2 = tempsurf;
  }
  while(h_is_updated == 1);

  //Comment out from here to exclude extrema population runtimes
  if(dim == MAX)
  get_sad_max(extrema_rect, ext_rect, ownbuf1, sxe, num_ext, d_ext_coords, d_ext_scan, dim, 
		csr_coords, csr_conns, csr_rowptr, csc_coords, csc_conns, csc_colptr);
  
  else if(dim == MIN)
  get_sad_min(extrema_rect, ext_rect, ownbuf1, sxe, num_ext, d_ext_coords, d_ext_scan, dim,
		csr_coords, csr_conns, csr_rowptr, csc_coords, csc_conns, csc_colptr);

  //Comment out till here to exclude extrema population runtimes

  cudaPitchedPtr ownbuf_pitched_ptr;
  ownbuf_pitched_ptr.ptr      = h_ex_own;
  ownbuf_pitched_ptr.pitch    = ex_vol_extent.width*sizeof(int);
  ownbuf_pitched_ptr.xsize    = ex_vol_extent.width;
  ownbuf_pitched_ptr.ysize    = ex_vol_extent.height;
  
  //Copy data to 3D array
  cudaMemcpy3DParms exbuf_copy_params = {0};
  exbuf_copy_params.srcArray       = d_ownbuf1;
  exbuf_copy_params.dstPtr       = ownbuf_pitched_ptr;
  exbuf_copy_params.extent       = ex_vol_extent;
  exbuf_copy_params.kind         = cudaMemcpyDeviceToHost;
  cutilSafeCall(cudaMemcpy3D(&exbuf_copy_params));

  cudaFree(d_is_updated_buf);
  cudaFreeArray(d_ownbuf1);
  cudaFreeArray(d_ownbuf2);
  cudaDestroySurfaceObject(ownbuf1);
  cudaDestroySurfaceObject(ownbuf2);

  cudaError_t error = cudaGetLastError();
  if (error != cudaSuccess) 
  {
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
  } 

}

extern "C" void get_ext_coords(short* erptr, cudaSurfaceObject_t &flag_surface,
				int &n_max, int &n_min)
{
  dim3 block(512,1);
  dim3 grid(64,1);

  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  const int num_cells = ext_rect.num_cells();

  cutilSafeCall(cudaMalloc((void**)&d_max_scan, num_cells*sizeof(int)));
  cutilSafeCall(cudaMalloc((void**)&d_min_scan, num_cells*sizeof(int)));

  get_critical_pts<3><<<grid,block>>>(ext_rect, flag_surface, d_max_scan);
  get_critical_pts<0><<<grid,block>>>(ext_rect, flag_surface, d_min_scan);
  cudaDeviceSynchronize();

  thrust::inclusive_scan(thrust::device, d_max_scan, d_max_scan+num_cells, d_max_scan); 
  thrust::inclusive_scan(thrust::device, d_min_scan, d_min_scan+num_cells, d_min_scan);
  
  cutilSafeCall(cudaMemcpy(num_max, &d_max_scan[num_cells-1], sizeof(int), cudaMemcpyDeviceToHost));
  cutilSafeCall(cudaMemcpy(num_min, &d_min_scan[num_cells-1], sizeof(int), cudaMemcpyDeviceToHost));

  //printf("Number of minima and maxima: %d, %d\n", *num_min, *num_max);

  n_min = *num_min;
  n_max = *num_max;

  cutilSafeCall(cudaMalloc((void**)&d_max_coords, *num_max*sizeof(int)));
  cutilSafeCall(cudaMalloc((void**)&d_min_coords, *num_min*sizeof(int)));

  stream_compact<<<grid,block>>>(flag_surface, d_max_scan, d_max_coords, num_cells);
  stream_compact<<<grid,block>>>(flag_surface, d_min_scan, d_min_coords, num_cells);

}

extern "C" void copy_ext(int* h_max_coords, int* h_min_coords)
{
  cutilSafeCall(cudaMemcpy(h_max_coords, d_max_coords, *num_max*sizeof(int), cudaMemcpyDeviceToHost));
  cutilSafeCall(cudaMemcpy(h_min_coords, d_min_coords, *num_min*sizeof(int), cudaMemcpyDeviceToHost));
}






