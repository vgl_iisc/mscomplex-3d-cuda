#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include "cutil.h"
#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>
#include "bfs_util.h"

#define NUM_THREADS_PER_BLOCK 256
#define NUM_BLOCKS 32

typedef unsigned char flag_t;
typedef short3 cell_t;

//Testing only

inline __device__ bool is_valid_cofacet(const rect_t ext_rect, cell_t cfct,
				      cell_t c, cudaSurfaceObject_t flag_surface,
				      int* visited)
{
  if(ext_rect.contains(cfct))
  {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(!is_critical(fg_cfct))
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair

   if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
     return true;

   //else if(cell_dim(flag_to_pair(cfct, fg_cfct))==3)
     //return true;
   }

   else if(is_critical(fg_cfct) && cell_dim(cfct)==2)
     return true;
   
  }

  return false;
}


__global__ void check_visited(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int visited_size, int* visited_coords, int* dead_ends, int* visited)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;
  
  for(int i=tid; i < visited_size; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(visited_coords[i]);

    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
   
    dead_ends[i] = 0;

   if(!is_critical(fg)) //Eliminates 1-saddles
    {

     cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;

     x = (c.x+1)&1;
     y = (c.y+1)&1;
     z = (c.z+1)&1;

     if (!z) //x && y true
     dirs = 0;
     else if (!x) //y && z true
     dirs = 1;
     else if (!y) //x && z true
     dirs = 2;
  
   int count = 0;
       
   switch(dirs)
    {
    case 0: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
     
     if(is_valid_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count==0)
     dead_ends[i] = 1;

     break;
    }

    case 1: 
    {
     cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(is_valid_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count==0)
     dead_ends[i] = 1;
 
     break;
    }

    case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(is_valid_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_valid_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count==0)
     dead_ends[i] = 1;

     break;
    }
   }

  }

 }

}


extern "C" void compute_12_dead_ends(short* erptr, cudaSurfaceObject_t flag_surface, int* d_visited, int* d_visited_coords, int h_visited_size) //Pending analysis on surrounding gradient lines of dead ends
{
  dim3 block(256,1);
  dim3 grid(32,1);

  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  const int num_cells = ext_rect.num_cells();

  int* d_dead_ends = 0;
  cutilSafeCall(cudaMalloc((void**)&d_dead_ends, h_visited_size*sizeof(int)));
  check_visited<<<grid,block>>>(ext_rect, flag_surface, h_visited_size, d_visited_coords, d_dead_ends, d_visited);
 
  int* h_dead_ends = new int[h_visited_size];
  cutilSafeCall(cudaMemcpy(h_dead_ends, d_dead_ends, h_visited_size*sizeof(int), cudaMemcpyDeviceToHost));
  
  //int* h_visited_coords = new int[h_visited_size];
  //cutilSafeCall(cudaMemcpy(h_visited_coords, d_visited_coords, h_visited_size*sizeof(int), cudaMemcpyDeviceToHost));
  
  /*std::ofstream output;
  output.open("cuda_visited_test22Jan.txt");

  for(int i=0; i<h_visited_size; i++)
    {   
        if(h_dead_ends[i] == 1)
        {
        cell_t c = ext_rect.i_to_c(h_visited_coords[i]);
	output<<"("<<c.x<<","<<c.y<<","<<c.z<<")\n";
        }
    }*/

}


//Additional Code
//Testing
  /*thrust::host_vector<float> h_s1xs2_vals = d_s1xs2_vals;
  thrust::host_vector<int> h_s1xs2_rowinds = d_s1xs2_rowinds;
  thrust::host_vector<int> h_s1xs2_colinds = d_s1xs2_colinds;
  
  int* h_1sad_coords = new int[*num_1saddles];
  int* h_2sad_coords = new int[*num_2saddles];
  cutilSafeCall(cudaMemcpy(h_1sad_coords, d_1sad_coords, (*num_1saddles)*sizeof(int), cudaMemcpyDeviceToHost)); 
  cutilSafeCall(cudaMemcpy(h_2sad_coords, d_2sad_coords, (*num_2saddles)*sizeof(int), cudaMemcpyDeviceToHost));

  std::ofstream output;
  output.open("cuda_2-1-n_hydrogen.txt");
  for(int i=0; i<h_s_conns.size(); i++)
  {
	/*cell_t sad1, sad2;
 	int row, col;
        col = h_s1xs2_colinds[i];
        row = h_s1xs2_rowinds[i];
 	//col = i/(*num_1saddles);
	//row = i%(*num_1saddles);
	sad1 = ext_rect.i_to_c(h_1sad_coords[row]);
 	sad2 = ext_rect.i_to_c(h_2sad_coords[col]);
	output<<"("<<sad2.x<<","<<sad2.y<<","<<sad2.z<<") ("<<sad1.x<<","<<sad1.y<<","<<sad1.z<<") "<<h_s1xs2_vals[i]<<"\n";

	//output<<"("<<h_s2[3*i]<<","<<h_s2[(3*i)+1]<<","<<h_s2[(3*i)+2]<<") ("<<h_s1[3*i]<<","<<h_s1[(3*i)+1]<<","<<h_s1[(3*i)+2]<<") "<<h_s_conns[i]<<"\n";

  }*/
  //Testing

 //Testing
  /*
  thrust::device_vector<float> d_jxj(h_jn_pts*h_jn_pts, 0);
  thrust::device_vector<float> d_jxsad2(h_jn_pts*(*num_2saddles), 0);

  traverse_paths_dns(ext_rect, flag_surface, d_jn_pts_coords, d_jn_pts_scan, d_jn_pts_scan, d_2saddles_scan, d_junction_pts, d_visited, d_visited_scan, thrust::raw_pointer_cast(&d_jxj[0]), thrust::raw_pointer_cast(&d_jxsad2[0]), h_jn_pts, h_jn_pts, *num_2saddles);  

  thrust::device_vector<float> d_jxj_vals;
  thrust::device_vector<int> d_jxj_rowptr;
  thrust::device_vector<int> d_jxj_colinds;
  int jxj_nnz = 0; 

  dense_to_csr(thrust::raw_pointer_cast(&d_jxj[0]), h_jn_pts, h_jn_pts, jxj_nnz, d_jxj_vals, d_jxj_rowptr, d_jxj_colinds);

  d_jxj.clear();
  d_jxj.shrink_to_fit();
  //jxj CSR ends
  */
  //End testing

