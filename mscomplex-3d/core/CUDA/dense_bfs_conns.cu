#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include<thrust/sort.h>
#include<thrust/execution_policy.h>

#include<cutil.h>
#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>
#include<dense_bfs_util.h>

#include <cusparse.h>

#define NUM_THREADS_PER_BLOCK 256
#define NUM_BLOCKS 32

#define CuSparse_Check(x) {cusparseStatus_t _c=x; if (_c != CUSPARSE_STATUS_SUCCESS) {printf("CuSparse Failed: %d, line: %d\n", (int)_c, __LINE__); exit(-1);}}

typedef unsigned char flag_t;
typedef short3 cell_t;

//To store matrix multiplication results (vals in A) at the end of each iteration
//Dense matrix is row-order
__global__ void store_dense_matrix(float* vals, int* rowinds, int* colinds, float* dense_mat, int rows, int cols, int nnz)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < nnz; i += num_thds)
  { 
   //int index = (rowinds[i]*cols) + colinds[i]; //row-order
   int index = (colinds[i]*rows) + rowinds[i];  //col-order
   dense_mat[index] += vals[i];
  }
}

__global__ void add_matrices(float* A, float* B, int size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < size; i += num_thds)
  {
   B[i] = B[i] + A[i];
  }

}

__global__ void get_12_sources_dns(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* dest, float* pxj, float* px2sad, int p_srcs, int j_dests, int sad2_dests)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;
  
  for(int i=tid; i < p_srcs; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(srcs_coords[i]);
    
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;   

    x = (c.x+1)&1;
    y = (c.y+1)&1;
    z = (c.z+1)&1;

    if (!z) //x && y true
    dirs = 0;
    else if (!x) //y && z true
    dirs = 1;
    else if (!y) //x && z true
    dirs = 2;

    switch(dirs)
    {
      case 0: 
     {
      cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
       
      get_path_source_dns<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

      get_path_source_dns<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

      get_path_source_dns<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);
 		
      get_path_source_dns<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);  
    
       break;
     }

      case 1: 
    {
     cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

     get_path_source_dns<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source_dns<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source_dns<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);
 
     get_path_source_dns<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);  

      break;
     }

     case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     get_path_source_dns<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source_dns<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source_dns<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests); 

     get_path_source_dns<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests); 
     
     break;
    }

  }
 
 }

}


__global__ void get_12_destination_mult(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int frontier_size, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* dest, float* pxj, float* px2sad, int j_dests, int sad2_dests)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;
  
  for(int i=tid; i < frontier_size; i += num_thds)
  {

   for(int pos=0; pos<4; pos++)
   {
   int index = (4*i) + pos;

   if(dest[index] != -1)
   {
    cell_t c = ext_rect.i_to_c(dest[index]);
    
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;      

    x = (c.x+1)&1;
    y = (c.y+1)&1;
    z = (c.z+1)&1;

    if (!z) //x && y true
    dirs = 0;
    else if (!x) //y && z true
    dirs = 1;
    else if (!y) //x && z true
    dirs = 2;
    
    switch(dirs)
    {
      case 0: 
      {
       cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
       
      if(trace_path_dns(index, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, 
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

      if(trace_path_dns(index, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, 
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

      if(trace_path_dns(index, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;
	
      trace_path_dns(index, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests); 
			break;
    
      }

      case 1: 
     {
      cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

      if(trace_path_dns(index, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

      if(trace_path_dns(index, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

      if(trace_path_dns(index, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

      trace_path_dns(index, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests); 
			break;

     }

     case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(trace_path_dns(index, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

     if(trace_path_dns(index, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

     if(trace_path_dns(index, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
			break;

     trace_path_dns(index, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
			frontier_size, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests); 
			break;
    
    }

  }

  }
 
 }

 }

}


extern "C" void traverse_paths_dns(rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, float* pxj, float* px2sad, int p_srcs, int j_dests, int sad2_dests)
{
  printf("Dense matrix traversal of MS Graph!\n");

  dim3 block(256,1);
  dim3 grid(32,1);

  thrust::device_vector<int> d_dest(4*p_srcs, -1);
  thrust::device_vector<int> d_valid_set(4*p_srcs, -1);
  size_t valid_size;

  get_12_sources_dns<<<grid,block>>>(ext_rect, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, junction_pts, visited, visited_scan, thrust::raw_pointer_cast(&d_dest[0]), pxj, px2sad, p_srcs, j_dests, sad2_dests); //1-2 paths for junction points or 1-saddles

  cudaDeviceSynchronize();

  while(true)
  {
   get_12_destination_mult<<<grid,block>>>(ext_rect, flag_surface, p_srcs, srcs_coords, srcs_scan, j_scan, sad2_scan, junction_pts, visited, visited_scan, thrust::raw_pointer_cast(&d_dest[0]), pxj, px2sad, j_dests, sad2_dests);

   cudaDeviceSynchronize();

   d_valid_set.clear();
   d_valid_set.resize(4*p_srcs, -1);
 
   valid_size = thrust::copy_if(d_dest.begin(), d_dest.end(), d_valid_set.begin(), is_not_negative()) - d_valid_set.begin();
   
   if(valid_size == 0) break;
   
  }

}


extern "C" void dense_to_csr(float* pxq, int p_rows, int q_cols, int &pxq_nnz, thrust::device_vector<float>& pxq_vals, thrust::device_vector<int>& pxq_rowptr, thrust::device_vector<int>& pxq_colinds)
{
  printf("Converting dense to CSR!\n");

  /*thrust::device_ptr<float> d_pxq_vals = thrust::device_pointer_cast(pxq_vals);
  thrust::device_ptr<int> d_pxq_rowptr = thrust::device_pointer_cast(pxq_rowptr);
  thrust::device_ptr<int> d_pxq_colinds = thrust::device_pointer_cast(pxq_colinds);*/

  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseCreate(&handle);

  cusparseMatDescr_t descr_dns;
  cusparseCreateMatDescr(&descr_dns);
  cusparseSetMatType(descr_dns, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatIndexBase(descr_dns, CUSPARSE_INDEX_BASE_ZERO);

  thrust::device_vector<int> pxq_nnz_row(p_rows, 0);
  status = cusparseSnnz(handle, CUSPARSE_DIRECTION_ROW, p_rows, q_cols, descr_dns, pxq, p_rows, thrust::raw_pointer_cast(&pxq_nnz_row[0]), &pxq_nnz); 
  CuSparse_Check(status);  

  pxq_vals.resize(pxq_nnz, 0);
  pxq_rowptr.resize(p_rows+1, 0);
  pxq_colinds.resize(pxq_nnz, 0);

  status = cusparseSdense2csr(handle, p_rows, q_cols, descr_dns, pxq, p_rows, thrust::raw_pointer_cast(&pxq_nnz_row[0]), thrust::raw_pointer_cast(&pxq_vals[0]), thrust::raw_pointer_cast(&pxq_rowptr[0]), thrust::raw_pointer_cast(&pxq_colinds[0]));

  //CuSparse_Check(status);

}

