#include <thrust/host_vector.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdexcept>

#include<grid_dataset_cuda.h>

#include <grid_mscomplex.h>
#include <cuda_runtime.h>
#include "cutil.h"
#include<omp.h>

#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

typedef float func_t;
typedef unsigned char flag_t;

cudaSurfaceObject_t flag_surface = 0;

namespace grid
{
  extern "C" cudaTextureObject_t assign_pairs_cuda(short* erptr, func_t *func, flag_t *flag,
						   cudaSurfaceObject_t* flag_surface);
  
  extern "C" void mark_boundarycps_cuda(short* erptr, short* bnd, short* bnd_dir);

  extern "C" void destroy_surface_obj();
  extern "C" void destroy_texture_obj();
  extern "C" void free_cp_arrays();
    
  extern "C" void count_cps(short* erptr, cudaSurfaceObject_t &flag_surface);

  extern "C" void count_boundarycps_cuda(short* erptr, short* bnd, short* bnd_dir,
					 cudaSurfaceObject_t &flag_surface);

  extern "C" void scan_cp_cuda(short* erptr, int* group_sums);

  extern "C" void save_cps_cuda(short* erptr, int num_cps, short* h_cp_cellid_buf,
				short* h_cp_vertid_buf, char* h_cp_index_buf, int* h_cp_pair_idx_buf,
				float* h_cp_func_buf, cudaTextureObject_t &func_tex,
				cudaSurfaceObject_t &flag_surface, short* h_cp_map, int* h_cp_idx);
  
  extern "C" void save_boundarycps_cuda(short* bnd, short* bnd_dir, cudaTextureObject_t &func_tex,
					cudaSurfaceObject_t &flag_surface);

  extern "C" void get_ext_coords(short* erptr, cudaSurfaceObject_t &flag_surface,
				   int &num_max, int &num_min);

  extern "C" void copy_ext(int* h_max_coords, int* h_min_coords);
  
  extern "C" void extrema_wrapper(short* max_rptr, short* min_rptr, short* erptr, cudaSurfaceObject_t &flag_surface, 
				int* h_own_max, int* h_own_min, const int &num_1s, 
				const int &num_2s, const int &num_max, const int &num_min,
				thrust::host_vector<short> &h_min_csr, thrust::host_vector<float> &h_s1xmin_conns_csr, 
				thrust::host_vector<int> &h_s1xmin_rowptr, thrust::host_vector<short> &h_s1_csc,
				thrust::host_vector<float> &h_s1xmin_conns_csc, thrust::host_vector<int> &h_s1xmin_colptr,
				thrust::host_vector<short> &h_max_csr, thrust::host_vector<float> &h_s2xmax_conns_csr,
		     		thrust::host_vector<int> &h_s2xmax_rowptr, thrust::host_vector<short> &h_s2_csc,
		     		thrust::host_vector<float> &h_s2xmax_conns_csc, thrust::host_vector<int> &h_s2xmax_colptr);
  
  extern "C" void mark_reachable_cuda(short* erptr, cudaSurfaceObject_t flag_surface,
				      int &num_s1, int &num_s2);

  extern "C" void traverse_ms_graph(short* erptr, cudaSurfaceObject_t flag_surface,
				    int* s1_coords, thrust::host_vector<short> &s2_csr,
				    thrust::host_vector<float> &s_conns_csr, thrust::host_vector<int> &s1xs2_rowptr,
				    int* s2_coords, thrust::host_vector<short> &s1_csc,
				    thrust::host_vector<float> &s_conns_csc, thrust::host_vector<int> &s1xs2_colptr);
  
namespace cuda
{
  void count_criticalpts(short* erptr, rect_t rct, rect_t ext, cudaSurfaceObject_t &flag_surface);
  
  void save_criticalpts(short* extrectptr, mscomplex_ptr_t msc, int num_cps, rect_t rct,
			rect_t ext, cudaTextureObject_t &func_tex, cudaSurfaceObject_t &flag_surface);
  
  void cuda_assign_gradient(dataset_ptr_t ds, mscomplex_ptr_t msc)
{
  //double s_asgr = omp_get_wtime();
  short extrectptr[6] = {ds->m_ext_rect.data()[0][0], ds->m_ext_rect.data()[0][1],
			 ds->m_ext_rect.data()[1][0], ds->m_ext_rect.data()[1][1],
			 ds->m_ext_rect.data()[2][0], ds->m_ext_rect.data()[2][1]};

  cudaTextureObject_t func_tex = assign_pairs_cuda(extrectptr, ds->m_vert_fns.data(),
						   ds->m_cell_flags.data(), &flag_surface);
  //double e_asgr = omp_get_wtime();
  //std::cout<<"Assign Gradient Time (func: assign_pairs_cuda): "<<e_asgr-s_asgr<<"\n";

  rect_list_t bnds;
  rect_t rct = ds->m_rect;
  rect_t ext = ds->m_ext_rect;
  
  get_boundry_rects(rct,ext,bnds);
  for(uint i=0; i<bnds.size(); ++i)
   {
     short bnd[6] = {bnds[i].lc()[0], bnds[i].uc()[0],
		     bnds[i].lc()[1], bnds[i].uc()[1],
		     bnds[i].lc()[2], bnds[i].uc()[2]};
     
     short bnd_dir[3] = {bnds[i].get_normal()[0],
			 bnds[i].get_normal()[1],
			 bnds[i].get_normal()[2]};

     mark_boundarycps_cuda(extrectptr, bnd, bnd_dir);
   }
  
if(msc)
  {

    //double s_cps = omp_get_wtime();  

  count_criticalpts(extrectptr, rct, ext, flag_surface);
  int* group_sums = new int[NUM_BLOCKS];
  scan_cp_cuda(extrectptr, group_sums);
  int num_cps = group_sums[NUM_BLOCKS-1];
  //std::cout<<"Number of critical points: "<<num_cps<<"\n";
  msc->resize(num_cps); //inits all msc ds in mscomplex_base

  msc->cp_map.resize(num_cps);
  msc->cp_idx.resize(num_cps);
  
  save_criticalpts(extrectptr, msc, ds, num_cps, rct, ext, func_tex, flag_surface);

  //double e_cps = omp_get_wtime();

  //std::cout<<"Count+Scan+Save Critical Points Time (funcs: count+scan+save_cps_cuda): "<<e_cps-s_cps<<"\n";
  }
}

  void count_criticalpts(short* extrectptr, rect_t rct, rect_t ext, cudaSurfaceObject_t &flag_surface)
{
  count_cps(extrectptr, flag_surface);
  rect_list_t bnds;
  get_boundry_rects(rct,ext,bnds);

  for(uint i=0; i<bnds.size(); ++i)
  {
   short bnd[6] = {bnds[i].lc()[0], bnds[i].uc()[0], bnds[i].lc()[1], bnds[i].uc()[1],
		   bnds[i].lc()[2], bnds[i].uc()[2]};
   short bnd_dir[3] = {bnds[i].get_normal()[0], bnds[i].get_normal()[1], bnds[i].get_normal()[2]};
   count_boundarycps_cuda(extrectptr,bnd,bnd_dir,flag_surface);
  }
}

  void save_criticalpts(short* extrectptr, mscomplex_ptr_t msc, dataset_ptr_t ds,
			int num_cps, rect_t rct, rect_t ext, cudaTextureObject_t &func_tex,
			cudaSurfaceObject_t &flag_surface)
  {
    save_cps_cuda(extrectptr, num_cps, (short*)msc->m_cp_cellid.data(), (short*)msc->m_cp_vertid.data(),
		    (char*)msc->m_cp_index.data(), (int*)msc->m_cp_pair_idx.data(),
		  msc->m_cp_fn.data(), func_tex, flag_surface, (short*)msc->cp_map.data(), msc->cp_idx.data());
      
    rect_list_t bnds;
    get_boundry_rects(rct,ext,bnds);
    for(uint i=0; i<bnds.size(); ++i)
   {
     short bnd[6] = {bnds[i].lc()[0], bnds[i].uc()[0], bnds[i].lc()[1],
		     bnds[i].uc()[1], bnds[i].lc()[2], bnds[i].uc()[2]};
     short bnd_dir[3] = {bnds[i].get_normal()[0], bnds[i].get_normal()[1], bnds[i].get_normal()[2]};

     save_boundarycps_cuda(bnd, bnd_dir, func_tex, flag_surface);
   }

    free_cp_arrays();
    destroy_texture_obj();
  }


  void owner_extrema(dataset_ptr_t ds, mscomplex_ptr_t msc,
		     thrust::host_vector<short> &min_csr, thrust::host_vector<float> &s1xmin_conns_csr,
		     thrust::host_vector<int> &s1xmin_rowptr, thrust::host_vector<short> &s1_csc,
		     thrust::host_vector<float> &s1xmin_conns_csc, thrust::host_vector<int> &s1xmin_colptr,
		     thrust::host_vector<short> &max_csr, thrust::host_vector<float> &s2xmax_conns_csr,
		     thrust::host_vector<int> &s2xmax_rowptr, thrust::host_vector<short> &s2_csc,
		     thrust::host_vector<float> &s2xmax_conns_csc, thrust::host_vector<int> &s2xmax_colptr)
  { 
    short max_rectptr[6] = {ds->get_extrema_rect<GDIR_DES>()[0][0], ds->get_extrema_rect<GDIR_DES>()[0][1],
			    ds->get_extrema_rect<GDIR_DES>()[1][0], ds->get_extrema_rect<GDIR_DES>()[1][1],
			    ds->get_extrema_rect<GDIR_DES>()[2][0], ds->get_extrema_rect<GDIR_DES>()[2][1]};

    short min_rectptr[6] = {ds->get_extrema_rect<GDIR_ASC>()[0][0], ds->get_extrema_rect<GDIR_ASC>()[0][1],
			    ds->get_extrema_rect<GDIR_ASC>()[1][0], ds->get_extrema_rect<GDIR_ASC>()[1][1],
			    ds->get_extrema_rect<GDIR_ASC>()[2][0], ds->get_extrema_rect<GDIR_ASC>()[2][1]};

    short extrectptr[6] = {ds->m_ext_rect.data()[0][0], ds->m_ext_rect.data()[0][1],
			   ds->m_ext_rect.data()[1][0], ds->m_ext_rect.data()[1][1],
			   ds->m_ext_rect.data()[2][0], ds->m_ext_rect.data()[2][1]};

    //Comment from here to exclude extrema population runtimes
    //Init extrema coords
    get_ext_coords(extrectptr, flag_surface, msc->num_max, msc->num_min);

    msc->max_coords.resize(msc->num_max, 0);
    msc->min_coords.resize(msc->num_min, 0);

    copy_ext(msc->max_coords.data(), msc->min_coords.data());
    //Comment upto here to exclude extrema population runtimes
    
    extrema_wrapper(max_rectptr, min_rectptr, extrectptr, flag_surface,
		    ds->m_owner_maxima.data(), ds->m_owner_minima.data(),
		    msc->num_s1, msc->num_s2, msc->num_max, msc->num_min, 
		    min_csr, s1xmin_conns_csr, s1xmin_rowptr,
		    s1_csc, s1xmin_conns_csc, s1xmin_colptr,
		    max_csr, s2xmax_conns_csr, s2xmax_rowptr,
		    s2_csc, s2xmax_conns_csc, s2xmax_colptr);
      
    destroy_surface_obj();
  }


  void compute_saddle_pairs(dataset_ptr_t ds, mscomplex_ptr_t msc,
			    thrust::host_vector<short> &s2_csr, thrust::host_vector<float> &s_conns_csr,
			    thrust::host_vector<int> &s1xs2_rowptr, thrust::host_vector<short> &s1_csc,
			    thrust::host_vector<float> &s_conns_csc, thrust::host_vector<int> &s1xs2_colptr)
  {
    short extrectptr[6] = {ds->m_ext_rect.data()[0][0], ds->m_ext_rect.data()[0][1],
			   ds->m_ext_rect.data()[1][0], ds->m_ext_rect.data()[1][1],
			   ds->m_ext_rect.data()[2][0], ds->m_ext_rect.data()[2][1]};

    //double s_mrk_r = omp_get_wtime();  
    mark_reachable_cuda(extrectptr, flag_surface, msc->num_s1, msc->num_s2);
    //double e_mrk_r = omp_get_wtime();  

    //std::cout<<"Mark Reachable (Parallel BFS) Time (mark_reachable_cuda): "<<e_mrk_r-s_mrk_r<<"\n";
    
    msc->s1_coords.resize(msc->num_s1, 0);
    msc->s2_coords.resize(msc->num_s2, 0);

    //double s_trav = omp_get_wtime();
    traverse_ms_graph(extrectptr, flag_surface, msc->s1_coords.data(),
		      s2_csr, s_conns_csr, s1xs2_rowptr, msc->s2_coords.data(),
		      s1_csc, s_conns_csc, s1xs2_colptr);
    //double e_trav = omp_get_wtime();

    //std::cout<<"Path Counting Time (traverse_ms_graph without CSC storage): "<<e_trav-s_trav<<"\n";
    
  }

}
}
