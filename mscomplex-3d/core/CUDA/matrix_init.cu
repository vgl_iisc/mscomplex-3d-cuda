#include "cutil.h"
#include "matrix_init.cu.h"

#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>

typedef unsigned char flag_t;
typedef short3 cell_t;

__global__ void stream_compact(cudaSurfaceObject_t flag_surface, int* scanned, int* compacted, int scan_size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < scan_size; i += num_thds)
  {

    if(i!=0) 
    {
     if(scanned[i]-scanned[i-1] == 1)
     {
      compacted[scanned[i]-1] = i;
     }
    }  

   else if(scanned[i] == 1)
       { 
        compacted[0] = i;
       }
  }

}



