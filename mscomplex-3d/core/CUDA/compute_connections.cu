#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include<thrust/sort.h>
#include<thrust/execution_policy.h>

#include "cutil.h"
#include "matrix_init.cu.h"

#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>
#include "bfs_util.h"

#include <cusparse.h>

//#define NUM_THREADS_PER_BLOCK 256
//#define NUM_BLOCKS 32
#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

#define CuSparse_Check(x) {cusparseStatus_t _c=x; if (_c != CUSPARSE_STATUS_SUCCESS) {printf("CuSparse Failed: %d, line: %d\n", (int)_c, __LINE__); exit(-1);}}

enum eDIM
{
  EXT_MIN = 0,
  EXT_MAX = 3
};

//const eDIM MIN = EXT_MIN;
const eDIM MAX = EXT_MAX;

extern "C" void traverse_paths(rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* pxj, int* px2sad, int p_srcs, int j_dests, int sad2_dests);

extern "C" void cusparse_matrix_mult(thrust::device_vector<float> &sad1xj_vals, thrust::device_vector<int> &sad1xj_rowptr, thrust::device_vector<int> &sad1xj_colinds, int nnz_sad1xj, thrust::device_vector<float> &jxj_vals, thrust::device_vector<int> &jxj_rowptr, thrust::device_vector<int> &jxj_colinds, int nnz_jxj, thrust::device_vector<float> &jxsad2_vals, thrust::device_vector<int> &jxsad2_rowptr, thrust::device_vector<int> &jxsad2_colinds, int nnz_jxsad2, thrust::device_vector<float> &s1xs2_vals, thrust::device_vector<int> &s1xs2_rowptr, thrust::device_vector<int> &s1xs2_colinds, int &nnz_s1xs2, int rowsA, int colsA, int colsB, int colsB2);

//Additional
extern "C" void traverse_paths_dns(rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, float* pxj, float* px2sad, int p_srcs, int j_dests, int sad2_dests);

extern "C" void dense_to_csr(float* pxq, int p_rows, int q_cols, int &pxq_nnz, thrust::device_vector<float>& pxq_vals, thrust::device_vector<int>& pxq_rowptr, thrust::device_vector<int>& pxq_colinds);

extern "C" void free_ms_arrays();
extern "C" void destroy_surface_obj();

typedef unsigned char flag_t;
typedef short3 cell_t;

int* d_visited = 0;
int* d_visited_scan = 0;

int* d_1saddles_scan = 0;
int* d_1sad_coords = 0; 
int* num_1saddles = new int[1];

int* d_2saddles_scan = 0;
int* d_2sad_coords = 0;
int* num_2saddles = new int[1];

int* d_junction_pts = 0;
int* d_jn_pts_coords = 0;
int* d_jn_pts_scan = 0;

__global__ void get_frontier(const rect_t ext_rect, int* saddles, int* current, int* visited)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();

  for(int i=tid; i < num_cells; i += num_thds)
  {
    visited[i] = 0;

    if(i!=0) 
    {
     if(saddles[i]-saddles[i-1] == 1)
     {
      current[saddles[i]-1] = i;
      visited[i] = 1;
     }
   }  

   else if(saddles[i] == 1)
       { 
        current[0] = i;
        visited[i] = 1;
       }

  }

}


__global__ void get_cofacets(const rect_t ext_rect, int curr_size, int* current, int* outfrontier, cudaSurfaceObject_t flag_surface, int* visited)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;
  bool x, y, z;
  int dirs;

  for(int i=tid; i < curr_size; i += num_thds)
  {
   cell_t c = ext_rect.i_to_c(current[i]);
   flag_t fg;
   surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

   cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;

   x = (c.x+1)&1;
   y = (c.y+1)&1;
   z = (c.z+1)&1;

   if(!z) //x && y true
   dirs = 0;
   else if (!x) //y && z true
   dirs = 1;
   else if (!y) //x && z true
   dirs = 2;

   switch(dirs)
   {
    case 0: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
     get_cofacet_pair<0>(i, c, fg, ext_rect, cfct1, outfrontier, flag_surface, visited); 
     
     get_cofacet_pair<1>(i, c, fg, ext_rect, cfct2, outfrontier, flag_surface, visited);

     get_cofacet_pair<2>(i, c, fg, ext_rect, cfct3, outfrontier, flag_surface, visited);

     get_cofacet_pair<3>(i, c, fg, ext_rect, cfct4, outfrontier, flag_surface, visited); 

     break;
    }

    case 1: 
    {
     cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;
     get_cofacet_pair<0>(i, c, fg, ext_rect, cfct1, outfrontier, flag_surface, visited);
    
     get_cofacet_pair<1>(i, c, fg, ext_rect, cfct2, outfrontier, flag_surface, visited);

     get_cofacet_pair<2>(i, c, fg, ext_rect, cfct3, outfrontier, flag_surface, visited);

     get_cofacet_pair<3>(i, c, fg, ext_rect, cfct4, outfrontier, flag_surface, visited);
 
     break;
    }

    case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;
     get_cofacet_pair<0>(i, c, fg, ext_rect, cfct1, outfrontier, flag_surface, visited);

     get_cofacet_pair<1>(i, c, fg, ext_rect, cfct2, outfrontier, flag_surface, visited);

     get_cofacet_pair<2>(i, c, fg, ext_rect, cfct3, outfrontier, flag_surface, visited);

     get_cofacet_pair<3>(i, c, fg, ext_rect, cfct4, outfrontier, flag_surface, visited);

     break;
    }

   }

 }

}


__global__ void stream_compact_jnpts(cudaSurfaceObject_t flag_surface, int* scanned, int* compacted, int* visited_coords, int scan_size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < scan_size; i += num_thds)
  {

    if(i!=0) 
    {
     if(scanned[i]-scanned[i-1] == 1)
     {
      compacted[scanned[i]-1] = visited_coords[i];
     }
    }  

   else if(scanned[i] == 1)
       { 
        compacted[0] = visited_coords[i];
       }
  }

}

//Outdegree junction points from 1 saddles to 2 saddles
__global__ void get_12_outd_junction_pts(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int visited_size, int* visited_coords, int* junction_pts, int* visited)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;
  
  for(int i=tid; i < visited_size; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(visited_coords[i]);
    
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    junction_pts[i] = 0;

    if(!is_critical(fg)) //Eliminates 1-saddles as potential junction pts
    {
     cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;

     x = (c.x+1)&1;
     y = (c.y+1)&1;
     z = (c.z+1)&1;

     if (!z) //x && y true
     dirs = 0;
     else if (!x) //y && z true
     dirs = 1;
     else if (!y) //x && z true
     dirs = 2;
  
   int count = 0;
       
   switch(dirs)
    {
    case 0: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
     
     if(is_outd_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count>1)
     junction_pts[i] = 1;

     break;
    }

    case 1: 
    {
     cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(is_outd_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count>1)
     junction_pts[i] = 1;
 
     break;
    }

    case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(is_outd_cofacet(ext_rect, cfct1, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct2, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct3, c, flag_surface, visited))
  	count++;
     if(is_outd_cofacet(ext_rect, cfct4, c, flag_surface, visited))
  	count++;
 
     if(count>1)
     junction_pts[i] = 1;

     break;
    }
   }

  }

  //else if(is_critical(fg) && cell_dim(c) == 1) //Including 1-saddles as junction points
   //junction_pts[i] = 1;    

 }

}

__global__ void get_12_sources(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* dest, int* pxj, int* px2sad, int p_srcs, int j_dests, int sad2_dests)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;

  for(int i=tid; i < p_srcs; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(srcs_coords[i]);
    
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;   

    x = (c.x+1)&1;
    y = (c.y+1)&1;
    z = (c.z+1)&1;

    if (!z) //x && y true
    dirs = 0;
    else if (!x) //y && z true
    dirs = 1;
    else if (!y) //x && z true
    dirs = 2;

    switch(dirs)
    {
      case 0: 
     {
      cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;
       
      get_path_source<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

      get_path_source<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

      get_path_source<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);
 		
      get_path_source<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);  
    
       break;
     }

      case 1: 
    {
     cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

     get_path_source<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);
 
     get_path_source<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);  

      break;
     }

     case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     get_path_source<0>(i, c, fg, ext_rect, cfct1, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source<1>(i, c, fg, ext_rect, cfct2, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests);

     get_path_source<2>(i, c, fg, ext_rect, cfct3, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests); 

     get_path_source<3>(i, c, fg, ext_rect, cfct4, flag_surface, srcs_scan, j_scan, sad2_scan,
				visited, visited_scan, junction_pts, dest, pxj, px2sad, p_srcs, j_dests, sad2_dests); 
     
     break;
    }

  }
 
 }

}


__global__ void get_12_destination(const rect_t ext_rect, cudaSurfaceObject_t flag_surface, int frontier_size, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* dest, int* pxj, int* px2sad, int j_dests, int sad2_dests)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  bool x, y, z;
  int dirs;
  
  for(int i=tid; i < frontier_size; i += num_thds)
  {

   if(dest[i] != -1)
   {

    cell_t c = ext_rect.i_to_c(dest[i]);
    
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    cell_t cfct1 = c, cfct2 = c, cfct3 = c, cfct4 = c;      

    x = (c.x+1)&1;
    y = (c.y+1)&1;
    z = (c.z+1)&1;

    if (!z) //x && y true
    dirs = 0;
    else if (!x) //y && z true
    dirs = 1;
    else if (!y) //x && z true
    dirs = 2;
    
    switch(dirs)
    {
      case 0: 
      {
      cfct1.x += 1, cfct2.x -= 1, cfct3.y += 1, cfct4.y -= 1;

      if(trace_path(i, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, 
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      if(trace_path(i, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, 
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      if(trace_path(i, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      trace_path(i, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests);	
      		break;
    
      }

      case 1: 
     {
      cfct1.y += 1, cfct2.y -= 1, cfct3.z += 1, cfct4.z -= 1;

      if(trace_path(i, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      if(trace_path(i, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      if(trace_path(i, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break;

      trace_path(i, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests); 			
   		break;

     }

     case 2: 
    {
     cfct1.x += 1, cfct2.x -= 1, cfct3.z += 1, cfct4.z -= 1;

     if(trace_path(i, c, fg, ext_rect, cfct1, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests)) 
		break;

     if(trace_path(i, c, fg, ext_rect, cfct2, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break; 

     if(trace_path(i, c, fg, ext_rect, cfct3, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests))
		break; 

     trace_path(i, c, fg, ext_rect, cfct4, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan,
		frontier_size/4, visited, visited_scan, junction_pts, dest, pxj, px2sad, j_dests, sad2_dests); 
		break;
    
    }

  }

  }
 
 }

}

__global__ void sort_arrs(int* A, int* B, const int size, const int block_size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < size; i += num_thds)
  { 
 
  int start_ind = (block_size*i);
  int end_ind = (block_size*i) + block_size;

  thrust::sort(thrust::seq, &A[start_ind], &A[end_ind]);
  thrust::sort(thrust::seq, &B[start_ind], &B[end_ind]);

  }

}

__global__ void sort_array(int* A, const int size, const int block_size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < size; i += num_thds)
  { 
 
  int start_ind = (block_size*i);
  int end_ind = (block_size*i) + block_size;

  thrust::sort(thrust::seq, &A[start_ind], &A[end_ind]);

  }

}

//Counts unique instances of each entry in the adj list (row) and populates CSR arrays
__global__ void get_csr(int* A, float* vals, int* colinds, int* rowptr, int rows, const int block_size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < rows; i += num_thds)
  {
   int row_nnz = 0;   

   for(int pos=0; pos<block_size; pos++) //Operations on each i-block where i=block_size
    {
    int index = (block_size*i) + pos;

      if(A[index] != -1)    
        {
	   if(vals[index] == -1) //visiting for the first time, -1=unvisited, 0=visited
	  {
	    int val = A[index];
            int count = 1;
	    colinds[index] = val;

      	    for(int j=(index+1); j<((block_size*i)+block_size); j++)
	      {
	        if((vals[j] == -1) && (A[j] == val))
	         {
		  count++;
		  vals[j] = 0; //visited
	         }
	      }
	  vals[index] = count;

	  }
        }

     else
	vals[index] = 0;

    
   if(vals[index] != 0)
       row_nnz++;

    }

  rowptr[i+1] = row_nnz; 

  }

}

__global__ void copy_coords(const rect_t ext_rect, short* p, int* p_coords, int* s1xp_inds, int sad1xp_nnz)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int dim;
  cell_t coord;

  //int row, col;
  //cell_t sad1, sad2;

  for(int i=tid; i < sad1xp_nnz; i += num_thds)
  {
     //col = s1xs2_colinds[i];
     //row = s1xs2_rowinds[i];
     //sad1 = ext_rect.i_to_c(s1_coords[row]);
     //sad2 = ext_rect.i_to_c(s2_coords[col]);
     
     //s1[3*i] = sad1.x; 
     //s1[(3*i)+1] = sad1.y; 
     //s1[(3*i)+2] = sad1.z; 

     //s2[3*i] = sad2.x; 
     //s2[(3*i)+1] = sad2.y; 
     //s2[(3*i)+2] = sad2.z;

     dim = s1xp_inds[i]; //dim = row/col for s1/s2 or s/ext
     coord = ext_rect.i_to_c(p_coords[dim]);

     p[3*i] = coord.x; 
     p[(3*i)+1] = coord.y; 
     p[(3*i)+2] = coord.z; 
  
  }

}


extern "C" void mark_reachable_cuda(short* erptr, cudaSurfaceObject_t flag_surface, int &n_s1, int &n_s2) 
							//Traversal from 1 to 2-saddles, 
							//marks reachable 1-cells
{ 
  //printf("Inside CUDA Mark Reachable!\n");

  dim3 block(512,1);
  dim3 grid(64,1);

  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  const int num_cells = ext_rect.num_cells();

  //int* d_1saddles_scan = 0; //Made global
  cutilSafeCall(cudaMalloc((void**)&d_1saddles_scan, num_cells*sizeof(int)));
  get_critical_pts<1><<<grid,block>>>(ext_rect, flag_surface, d_1saddles_scan);
  cudaDeviceSynchronize();
  thrust::inclusive_scan(thrust::device, d_1saddles_scan, d_1saddles_scan+num_cells, d_1saddles_scan); 

  //int* num_1saddles = new int[1]; //Made global
  cutilSafeCall(cudaMemcpy(num_1saddles, &d_1saddles_scan[num_cells-1], sizeof(int), cudaMemcpyDeviceToHost));  
  n_s1 = *num_1saddles;

  //printf("Number of 1-saddles: %d\n", *num_1saddles);
 
  //int* d_2saddles_scan = 0; //Made global
  cutilSafeCall(cudaMalloc((void**)&d_2saddles_scan, num_cells*sizeof(int)));
  get_critical_pts<2><<<grid,block>>>(ext_rect, flag_surface, d_2saddles_scan);
  cudaDeviceSynchronize();
  thrust::inclusive_scan(thrust::device, d_2saddles_scan, d_2saddles_scan+num_cells, d_2saddles_scan);
  //int* num_2saddles = new int[1]; //Made global
  cutilSafeCall(cudaMemcpy(num_2saddles, &d_2saddles_scan[num_cells-1], sizeof(int), cudaMemcpyDeviceToHost));
  n_s2 = *num_2saddles;
  //printf("Number of 2-saddles: %d\n", *num_2saddles);

  cutilSafeCall(cudaMalloc((void**)&d_visited, num_cells*sizeof(int)));

  size_t curr_size = *num_1saddles;
  size_t front_size = 4*curr_size;

  thrust::device_vector<int> d_current(curr_size);
  thrust::device_vector<int> d_outfront(front_size, -1); 

  get_frontier<<<grid,block>>>(ext_rect, d_1saddles_scan, thrust::raw_pointer_cast(&d_current[0]), d_visited);

  while(true)
  {

  get_cofacets<<<grid,block>>>(ext_rect, curr_size, thrust::raw_pointer_cast(&d_current[0]), 
				thrust::raw_pointer_cast(&d_outfront[0]), flag_surface, d_visited);
  cudaDeviceSynchronize();

  d_current.clear();
  d_current.resize(front_size, -1);
  //printf("Upper bound size of current: %d\n", d_current.size());

  curr_size = thrust::copy_if(d_outfront.begin(), d_outfront.end(), d_current.begin(), is_not_negative()) - d_current.begin();
  
  //printf("Actual curr_size: %d\n", curr_size);
  if(curr_size == 0) break;
 
  d_current.resize(curr_size);

  front_size = 4*curr_size;
  d_outfront.clear();
  d_outfront.resize(front_size, -1);  

  }

}

extern "C" void compute_junction_pts(rect_t ext_rect, cudaSurfaceObject_t flag_surface, const int num_cells, int& h_jn_pts)
{
  dim3 block(512,1);
  dim3 grid(64,1);

  //int* d_visited_scan = 0; //Made global
  cutilSafeCall(cudaMalloc((void**)&d_visited_scan, num_cells*sizeof(int)));
  thrust::inclusive_scan(thrust::device, d_visited, d_visited+num_cells, d_visited_scan);

  int h_visited_size;
  cutilSafeCall(cudaMemcpy(&h_visited_size, &d_visited_scan[num_cells-1], sizeof(int), cudaMemcpyDeviceToHost));  
  //printf("Visited array size: %d\n", h_visited_size);

  int* d_visited_coords = 0;
  cutilSafeCall(cudaMalloc((void**)&d_visited_coords, h_visited_size*sizeof(int)));
  stream_compact<<<grid,block>>>(flag_surface, d_visited_scan, d_visited_coords, num_cells); //Populates d_visited_coords

  //int* d_junction_pts = 0; //Made global
  cutilSafeCall(cudaMalloc((void**)&d_junction_pts, h_visited_size*sizeof(int)));
 
  get_12_outd_junction_pts<<<grid,block>>>(ext_rect, flag_surface, h_visited_size, d_visited_coords, d_junction_pts, d_visited);
  cudaDeviceSynchronize();

  cutilSafeCall(cudaMalloc((void**)&d_jn_pts_scan, h_visited_size*sizeof(int)));

  thrust::inclusive_scan(thrust::device, d_junction_pts, d_junction_pts+h_visited_size, d_jn_pts_scan);
  cutilSafeCall(cudaMemcpy(&h_jn_pts, &d_jn_pts_scan[h_visited_size-1], sizeof(int), cudaMemcpyDeviceToHost));  
  //printf("Junction points: %d\n", h_jn_pts);

  //int* d_jn_pts_coords = 0; //Made global
  cutilSafeCall(cudaMalloc((void**)&d_jn_pts_coords, h_jn_pts*sizeof(int)));

  stream_compact_jnpts<<<grid,block>>>(flag_surface, d_jn_pts_scan, d_jn_pts_coords, d_visited_coords, h_visited_size);

  cudaFree(d_visited_coords);
  }

 extern "C" void traverse_ms_graph(short* erptr, cudaSurfaceObject_t flag_surface, int* h_1sad_coords, 
				thrust::host_vector<short> &h_s2_csr, thrust::host_vector<float> &h_s_conns_csr, 
				thrust::host_vector<int> &h_s1xs2_rowptr, int* h_2sad_coords, 
				thrust::host_vector<short> &h_s1_csc, thrust::host_vector<float> &h_s_conns_csc, 
				thrust::host_vector<int> &h_s1xs2_colptr)
{
  dim3 block(512,1);
  dim3 grid(64,1);

  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  const int num_cells = ext_rect.num_cells();

  int h_jn_pts; //No of junction points
  compute_junction_pts(ext_rect, flag_surface, num_cells, h_jn_pts);

  //1-saddle + 2-saddle collection and traversal
  cutilSafeCall(cudaMalloc((void**)&d_1sad_coords, *num_1saddles*sizeof(int)));
  stream_compact<<<grid,block>>>(flag_surface, d_1saddles_scan, d_1sad_coords, num_cells);
  cutilSafeCall(cudaMalloc((void**)&d_2sad_coords, *num_2saddles*sizeof(int)));
  stream_compact<<<grid,block>>>(flag_surface, d_2saddles_scan, d_2sad_coords, num_cells);

  thrust::device_vector<int> d_jxj(h_jn_pts*4, -1); //Adjacency lists of size 4
  thrust::device_vector<int> d_jxsad2(h_jn_pts*4, -1);

  //Junction points traversal jxj and jx2s
  traverse_paths(ext_rect, flag_surface, d_jn_pts_coords, d_jn_pts_scan, d_jn_pts_scan, d_2saddles_scan, d_junction_pts, d_visited, d_visited_scan, thrust::raw_pointer_cast(&d_jxj[0]), thrust::raw_pointer_cast(&d_jxsad2[0]), h_jn_pts, h_jn_pts, *num_2saddles);

  //Sorting adjacency lists
  sort_arrs<<<grid,block>>>(thrust::raw_pointer_cast(&d_jxj[0]), thrust::raw_pointer_cast(&d_jxsad2[0]), h_jn_pts, 4);

  //jxj CSR begins
  thrust::device_vector<float> d_jxj_vals(4*h_jn_pts, -1); //Stream compacted to nnz later
  thrust::device_vector<int> d_jxj_rowptr(h_jn_pts+1, 0); //size = num_rows+1 
  thrust::device_vector<int> d_jxj_colinds(4*h_jn_pts, -1); //Stream compacted to nnz later

  get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&d_jxj[0]), thrust::raw_pointer_cast(&d_jxj_vals[0]), 
			thrust::raw_pointer_cast(&d_jxj_colinds[0]), thrust::raw_pointer_cast(&d_jxj_rowptr[0]), h_jn_pts, 4);

  int jxj_nnz = 0;
  jxj_nnz = thrust::copy_if(d_jxj_vals.begin(), d_jxj_vals.end(), d_jxj_vals.begin(), is_not_zero_or_negative()) - d_jxj_vals.begin();
  d_jxj_vals.resize(jxj_nnz);

  thrust::inclusive_scan(d_jxj_rowptr.begin(), d_jxj_rowptr.end(), d_jxj_rowptr.begin());

  thrust::copy_if(d_jxj_colinds.begin(), d_jxj_colinds.end(), d_jxj_colinds.begin(), is_not_negative()); //colinds can be zero
  d_jxj_colinds.resize(jxj_nnz);

  //printf("jxj nnz: %d\n", jxj_nnz);

  d_jxj.clear();
  d_jxj.shrink_to_fit();
  //jxj CSR ends

  
  //jxsad2 CSR begins
  thrust::device_vector<float> d_jxsad2_vals(4*h_jn_pts, -1); //Stream compacted to nnz later
  thrust::device_vector<int> d_jxsad2_rowptr(h_jn_pts+1, 0); //size = num_rows+1 
  thrust::device_vector<int> d_jxsad2_colinds(4*h_jn_pts, -1);

  get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&d_jxsad2[0]), thrust::raw_pointer_cast(&d_jxsad2_vals[0]), 
			thrust::raw_pointer_cast(&d_jxsad2_colinds[0]), thrust::raw_pointer_cast(&d_jxsad2_rowptr[0]), h_jn_pts, 4);

  int jxsad2_nnz = 0;
  jxsad2_nnz = thrust::copy_if(d_jxsad2_vals.begin(), d_jxsad2_vals.end(), d_jxsad2_vals.begin(), is_not_zero_or_negative()) - d_jxsad2_vals.begin();
  d_jxsad2_vals.resize(jxsad2_nnz);

  thrust::inclusive_scan(d_jxsad2_rowptr.begin(), d_jxsad2_rowptr.end(), d_jxsad2_rowptr.begin());

  thrust::copy_if(d_jxsad2_colinds.begin(), d_jxsad2_colinds.end(), d_jxsad2_colinds.begin(), is_not_negative());
  d_jxsad2_colinds.resize(jxsad2_nnz);

  //printf("jxsad2 nnz: %d\n", jxsad2_nnz);

  d_jxsad2.clear();
  d_jxsad2.shrink_to_fit();
  //jxsad2 CSR ends

  thrust::device_vector<int> d_sad1xj((*num_1saddles)*4, -1);
  thrust::device_vector<int> d_sad1xsad2((*num_1saddles)*4, -1);

  //sad1xj and sad1xsad2 traversal
  traverse_paths(ext_rect, flag_surface, d_1sad_coords, d_1saddles_scan, d_jn_pts_scan, d_2saddles_scan, d_junction_pts, d_visited, d_visited_scan, thrust::raw_pointer_cast(&d_sad1xj[0]), thrust::raw_pointer_cast(&d_sad1xsad2[0]), *num_1saddles, h_jn_pts, *num_2saddles);

  sort_arrs<<<grid,block>>>(thrust::raw_pointer_cast(&d_sad1xj[0]), thrust::raw_pointer_cast(&d_sad1xsad2[0]), *num_1saddles, 4);

  //sad1xj CSR begins
  thrust::device_vector<float> d_sad1xj_vals(4*(*num_1saddles), -1); 
  thrust::device_vector<int> d_sad1xj_rowptr((*num_1saddles)+1, 0); //size = num_rows+1 
  thrust::device_vector<int> d_sad1xj_colinds(4*(*num_1saddles), -1);

  get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&d_sad1xj[0]), thrust::raw_pointer_cast(&d_sad1xj_vals[0]), thrust::raw_pointer_cast(&d_sad1xj_colinds[0]), thrust::raw_pointer_cast(&d_sad1xj_rowptr[0]), *num_1saddles, 4);

  int sad1xj_nnz = 0;
  sad1xj_nnz = thrust::copy_if(d_sad1xj_vals.begin(), d_sad1xj_vals.end(), d_sad1xj_vals.begin(), is_not_zero_or_negative()) - d_sad1xj_vals.begin();
  d_sad1xj_vals.resize(sad1xj_nnz);

  thrust::inclusive_scan(d_sad1xj_rowptr.begin(), d_sad1xj_rowptr.end(), d_sad1xj_rowptr.begin());

  thrust::copy_if(d_sad1xj_colinds.begin(), d_sad1xj_colinds.end(), d_sad1xj_colinds.begin(), is_not_negative());
  d_sad1xj_colinds.resize(sad1xj_nnz);

  //printf("sad1xj nnz: %d\n", sad1xj_nnz);
  
  d_sad1xj.clear();
  d_sad1xj.shrink_to_fit();
  //sad1xj CSR ends

  //1sadx2sad CSR begins
  thrust::device_vector<float> d_s1xs2_vals(4*(*num_1saddles), -1); 
  thrust::device_vector<int> d_s1xs2_rowptr((*num_1saddles)+1, 0); //size = num_rows+1 
  thrust::device_vector<int> d_s1xs2_colinds(4*(*num_1saddles), -1);

  get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&d_sad1xsad2[0]), thrust::raw_pointer_cast(&d_s1xs2_vals[0]), thrust::raw_pointer_cast(&d_s1xs2_colinds[0]), thrust::raw_pointer_cast(&d_s1xs2_rowptr[0]), *num_1saddles, 4);

  int sad1xsad2_nnz = 0;
  sad1xsad2_nnz = thrust::copy_if(d_s1xs2_vals.begin(), d_s1xs2_vals.end(), d_s1xs2_vals.begin(), is_not_zero_or_negative()) - d_s1xs2_vals.begin();
  d_s1xs2_vals.resize(sad1xsad2_nnz);

  thrust::inclusive_scan(d_s1xs2_rowptr.begin(), d_s1xs2_rowptr.end(), d_s1xs2_rowptr.begin());

  thrust::copy_if(d_s1xs2_colinds.begin(), d_s1xs2_colinds.end(), d_s1xs2_colinds.begin(), is_not_negative());
  d_s1xs2_colinds.resize(sad1xsad2_nnz);

  //printf("sad1xsad2 nnz: %d\n", sad1xsad2_nnz);
  
  d_sad1xsad2.clear();
  d_sad1xsad2.shrink_to_fit();
  //1sadx2sad CSR ends

  free_ms_arrays();

  cusparse_matrix_mult(d_sad1xj_vals, d_sad1xj_rowptr, d_sad1xj_colinds, sad1xj_nnz, d_jxj_vals, d_jxj_rowptr, d_jxj_colinds, jxj_nnz, d_jxsad2_vals, d_jxsad2_rowptr, d_jxsad2_colinds, jxsad2_nnz, d_s1xs2_vals, d_s1xs2_rowptr, d_s1xs2_colinds, sad1xsad2_nnz, *num_1saddles, h_jn_pts, h_jn_pts, *num_2saddles);

  //If not mentioned, assume CSR in the naming convention
  //CSR data structures

  //printf("Final sad1xsad2 nnz: %d\n", sad1xsad2_nnz);

  thrust::device_vector<short> d_s2_csr(3*sad1xsad2_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_s2_csr[0]), thrust::raw_pointer_cast(&d_2sad_coords[0]), 
				thrust::raw_pointer_cast(&d_s1xs2_colinds[0]), sad1xsad2_nnz);

  //Copying CSR to host
  h_s2_csr.resize(3*sad1xsad2_nnz);
  h_s_conns_csr.resize(sad1xsad2_nnz);
  h_s1xs2_rowptr.resize((*num_1saddles)+1);

  //h_1sad_coords = d_1sad_coords;
  cutilSafeCall(cudaMemcpy(h_1sad_coords, d_1sad_coords, *num_1saddles*sizeof(int), cudaMemcpyDeviceToHost));
  h_s2_csr = d_s2_csr;
  h_s_conns_csr = d_s1xs2_vals;
  h_s1xs2_rowptr = d_s1xs2_rowptr;

  d_s2_csr.clear();
  d_s2_csr.shrink_to_fit();


  //Comment out from here to exclude saddle population runtimes
  //CSC data structures
  thrust::device_vector<float> d_s1xs2_csc_vals(sad1xsad2_nnz, 0);
  thrust::device_vector<int> d_s1xs2_csc_rowinds(sad1xsad2_nnz, 0);
  thrust::device_vector<int> d_s1xs2_colptr(*num_2saddles+1, 0);

  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseCreate(&handle);

  status = cusparseScsr2csc(handle, *num_1saddles, *num_2saddles, sad1xsad2_nnz, 
			thrust::raw_pointer_cast(&d_s1xs2_vals[0]), thrust::raw_pointer_cast(&d_s1xs2_rowptr[0]), 
			thrust::raw_pointer_cast(&d_s1xs2_colinds[0]), thrust::raw_pointer_cast(&d_s1xs2_csc_vals[0]), 
			thrust::raw_pointer_cast(&d_s1xs2_csc_rowinds[0]), thrust::raw_pointer_cast(&d_s1xs2_colptr[0]), 
			CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO);
  CuSparse_Check(status);
 
  //Clearing CSR
  d_s1xs2_vals.clear();
  d_s1xs2_vals.shrink_to_fit();
  d_s1xs2_rowptr.clear();
  d_s1xs2_rowptr.shrink_to_fit();
  d_s1xs2_colinds.clear();
  d_s1xs2_colinds.shrink_to_fit();
  
  thrust::device_vector<short> d_s1_csc(3*sad1xsad2_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_s1_csc[0]), thrust::raw_pointer_cast(&d_1sad_coords[0]),
				 thrust::raw_pointer_cast(&d_s1xs2_csc_rowinds[0]), sad1xsad2_nnz);

  h_s1_csc.resize(3*sad1xsad2_nnz);
  h_s_conns_csc.resize(sad1xsad2_nnz);
  h_s1xs2_colptr.resize((*num_2saddles)+1);

  cutilSafeCall(cudaMemcpy(h_2sad_coords, d_2sad_coords, *num_2saddles*sizeof(int), cudaMemcpyDeviceToHost));
  h_s1_csc = d_s1_csc;
  h_s_conns_csc = d_s1xs2_csc_vals;
  h_s1xs2_colptr = d_s1xs2_colptr;
  
  //Comment out till here to exclude saddle population runtimes

}

extern "C" void traverse_paths(rect_t ext_rect, cudaSurfaceObject_t flag_surface, int* srcs_coords, int* srcs_scan, int* j_scan, int* sad2_scan, int* junction_pts, int* visited, int* visited_scan, int* pxj, int* px2sad, int p_srcs, int j_dests, int sad2_dests)
{
  dim3 block(512,1);
  dim3 grid(64,1);

  thrust::device_vector<int> d_dest(4*p_srcs, -1);
  thrust::device_vector<int> d_valid_set(4*p_srcs, -1);
  size_t valid_size;

  get_12_sources<<<grid,block>>>(ext_rect, flag_surface, srcs_coords, srcs_scan, j_scan, sad2_scan, junction_pts, visited, visited_scan, thrust::raw_pointer_cast(&d_dest[0]), pxj, px2sad, p_srcs, j_dests, sad2_dests); //1-2 paths for junction points or 1-saddles

  cudaDeviceSynchronize();

  while(true)
  {
   get_12_destination<<<grid,block>>>(ext_rect, flag_surface, 4*p_srcs, srcs_coords, srcs_scan, j_scan, sad2_scan, junction_pts, visited, visited_scan, thrust::raw_pointer_cast(&d_dest[0]), pxj, px2sad, j_dests, sad2_dests);

   cudaDeviceSynchronize();

   d_valid_set.clear();
   d_valid_set.resize(4*p_srcs, -1); //Check if these two ops can be replaced to plain reinitialization
 
   valid_size = thrust::copy_if(d_dest.begin(), d_dest.end(), d_valid_set.begin(), is_not_negative()) - d_valid_set.begin();
  
   //printf("Temp size: %d\n", valid_size);
   
   if(valid_size == 0) break;
   
  }

}


extern "C" void free_ms_arrays()
{
   cudaFree(d_visited);
   cudaFree(d_visited_scan);

   cudaFree(d_1saddles_scan);
   //cudaFree(d_1sad_coords);

   cudaFree(d_2saddles_scan);
   //cudaFree(d_2sad_coords);

   cudaFree(d_junction_pts);
   cudaFree(d_jn_pts_coords);
   cudaFree(d_jn_pts_scan);
}


//------------------------------------------------------------------------//
//Saddle-Extrema connections stored in CSR and CSC formats

template<int sad_dim>
__global__ void conn_sad_ext(const rect_t extrema_rect, const rect_t ext_rect, cudaSurfaceObject_t extr, 
				int* sad_coords, int* ext_coords, int* ext_scan, 
				int* sxe, int num_sads, eDIM dim)
{
   int tid = blockIdx.x*blockDim.x + threadIdx.x;
   int num_thds = gridDim.x*blockDim.x;
   
   const int a = (sad_dim == 2)?(1):(0);
   cell_t c, e1, e2;
   int ei1, ei2;
    
   for(int i=tid; i < num_sads; i += num_thds)
   { 
     c = ext_rect.i_to_c(sad_coords[i]);
     ei1 = -1; 
     ei2 = -1;

     e1.x = c.x + ((c.x+a)&1);
     e1.y = c.y + ((c.y+a)&1);
     e1.z = c.z + ((c.z+a)&1);

     e2.x = c.x - ((c.x+a)&1);
     e2.y = c.y - ((c.y+a)&1);
     e2.z = c.z - ((c.z+a)&1);

     if(dim==MAX)
     {
      if(ext_rect.contains(e1))
      	surf3Dread(&ei1, extr, ((e1.x-1)/2)*sizeof(int), ((e1.y-1)/2), ((e1.z-1)/2));

      if(ext_rect.contains(e2))
     	surf3Dread(&ei2, extr, ((e2.x-1)/2)*sizeof(int), ((e2.y-1)/2), ((e2.z-1)/2));
     } 
     
     else
     {
      if(ext_rect.contains(e1)) 
	surf3Dread(&ei1, extr, (e1.x/2)*sizeof(int), (e1.y/2), (e1.z/2)); 

      if(ext_rect.contains(e2))
      	surf3Dread(&ei2, extr, (e2.x/2)*sizeof(int), (e2.y/2), (e2.z/2)); 
     }    

     if(ei1 != -1)
     {
	ei1 = ext_rect.c_to_i(extrema_rect.i_to_c2(ei1));     
	ei1 = ext_scan[ei1]-1;
     	sxe[2*i] = ei1;
     }

     if(ei2 != -1)
     {
     	ei2 = ext_rect.c_to_i(extrema_rect.i_to_c2(ei2));     
     	ei2 = ext_scan[ei2]-1;
     	sxe[2*i + 1] = ei2;
     }
     
   }

}

extern "C" void get_sad_min(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &d_ext, thrust::device_vector<int>& s1xmin, 
				const int& num_min, int* d_min_coords, int* d_min_scan, const eDIM& dim, 
				thrust::host_vector<short> &h_min_csr, thrust::host_vector<float> &h_conns_csr, 
				thrust::host_vector<int> &h_rowptr, thrust::host_vector<short> &h_s1_csc, 
				thrust::host_vector<float> &h_conns_csc, thrust::host_vector<int> &h_colptr)
{
   dim3 block(512,1);
   dim3 grid(64,1);

   conn_sad_ext<1><<<grid,block>>>(extrema_rect, ext_rect, d_ext, 
				d_1sad_coords, d_min_coords, d_min_scan, 
				thrust::raw_pointer_cast(&s1xmin[0]), *num_1saddles, dim);

   sort_array<<<grid,block>>>(thrust::raw_pointer_cast(&s1xmin[0]), *num_1saddles, 2);
   
   thrust::device_vector<float> d_s1xmin_csr_vals(2*(*num_1saddles), -1); 
   thrust::device_vector<int> d_s1xmin_rowptr((*num_1saddles)+1, 0); //size = num_rows+1 
   thrust::device_vector<int> d_s1xmin_csr_colinds(2*(*num_1saddles), -1);

   get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&s1xmin[0]), thrust::raw_pointer_cast(&d_s1xmin_csr_vals[0]), thrust::raw_pointer_cast(&d_s1xmin_csr_colinds[0]), thrust::raw_pointer_cast(&d_s1xmin_rowptr[0]), *num_1saddles, 2);  

   int s1xmin_nnz = 0;
   s1xmin_nnz = thrust::copy_if(d_s1xmin_csr_vals.begin(), d_s1xmin_csr_vals.end(), d_s1xmin_csr_vals.begin(), is_not_zero_or_negative()) - d_s1xmin_csr_vals.begin();
  d_s1xmin_csr_vals.resize(s1xmin_nnz);

  thrust::inclusive_scan(d_s1xmin_rowptr.begin(), d_s1xmin_rowptr.end(), d_s1xmin_rowptr.begin());

  thrust::copy_if(d_s1xmin_csr_colinds.begin(), d_s1xmin_csr_colinds.end(), d_s1xmin_csr_colinds.begin(), is_not_negative());
  d_s1xmin_csr_colinds.resize(s1xmin_nnz);

  //printf("s1xmin vals: %d\n", s1xmin_nnz);

  //Copying CSR to host
  thrust::device_vector<short> d_min_csr(3*s1xmin_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_min_csr[0]), thrust::raw_pointer_cast(&d_min_coords[0]), 
				thrust::raw_pointer_cast(&d_s1xmin_csr_colinds[0]), s1xmin_nnz);

  h_min_csr.resize(3*s1xmin_nnz);
  h_conns_csr.resize(s1xmin_nnz);
  h_rowptr.resize((*num_1saddles)+1);

  h_min_csr = d_min_csr;
  h_conns_csr = d_s1xmin_csr_vals;
  h_rowptr = d_s1xmin_rowptr;

  d_min_csr.clear();
  d_min_csr.shrink_to_fit();

  //CSC data structures
  thrust::device_vector<float> d_s1xmin_csc_vals(s1xmin_nnz, 0);
  thrust::device_vector<int> d_s1xmin_csc_rowinds(s1xmin_nnz, 0);
  thrust::device_vector<int> d_s1xmin_colptr(num_min+1, 0);

  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseCreate(&handle);

  status = cusparseScsr2csc(handle, *num_1saddles, num_min, s1xmin_nnz, 
			thrust::raw_pointer_cast(&d_s1xmin_csr_vals[0]), thrust::raw_pointer_cast(&d_s1xmin_rowptr[0]), 
			thrust::raw_pointer_cast(&d_s1xmin_csr_colinds[0]), thrust::raw_pointer_cast(&d_s1xmin_csc_vals[0]), 
			thrust::raw_pointer_cast(&d_s1xmin_csc_rowinds[0]), thrust::raw_pointer_cast(&d_s1xmin_colptr[0]), 
			CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO);
  CuSparse_Check(status);

  //Clearing CSR
  d_s1xmin_csr_vals.clear();
  d_s1xmin_csr_vals.shrink_to_fit();
  d_s1xmin_rowptr.clear();
  d_s1xmin_rowptr.shrink_to_fit();
  d_s1xmin_csr_colinds.clear();
  d_s1xmin_csr_colinds.shrink_to_fit();
  

  //Copying CSC to host
  thrust::device_vector<short> d_s1_csc(3*s1xmin_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_s1_csc[0]), thrust::raw_pointer_cast(&d_1sad_coords[0]),
				 thrust::raw_pointer_cast(&d_s1xmin_csc_rowinds[0]), s1xmin_nnz);

  h_s1_csc.resize(3*s1xmin_nnz);
  h_conns_csc.resize(s1xmin_nnz);
  h_colptr.resize(num_min+1);

  h_s1_csc = d_s1_csc;
  h_conns_csc = d_s1xmin_csc_vals;
  h_colptr = d_s1xmin_colptr;


}


extern "C" void get_sad_max(const rect_t extrema_rect, const rect_t ext_rect, 
				cudaSurfaceObject_t &d_ext, thrust::device_vector<int>& s2xmax, 
				const int& num_max, int* d_max_coords, int* d_max_scan, const eDIM& dim,
				thrust::host_vector<short> &h_max_csr, thrust::host_vector<float> &h_conns_csr, 
				thrust::host_vector<int> &h_rowptr, thrust::host_vector<short> &h_s2_csc, 
				thrust::host_vector<float> &h_conns_csc, thrust::host_vector<int> &h_colptr)
{
   dim3 block(512,1);
   dim3 grid(64,1);

   conn_sad_ext<2><<<grid,block>>>(extrema_rect, ext_rect, d_ext, 
				d_2sad_coords, d_max_coords, d_max_scan,
				thrust::raw_pointer_cast(&s2xmax[0]), *num_2saddles, dim);
	
   sort_array<<<grid,block>>>(thrust::raw_pointer_cast(&s2xmax[0]), *num_2saddles, 2);

   thrust::device_vector<float> d_s2xmax_csr_vals(2*(*num_2saddles), -1); 
   thrust::device_vector<int> d_s2xmax_rowptr((*num_2saddles)+1, 0); //size = num_rows+1 
   thrust::device_vector<int> d_s2xmax_csr_colinds(2*(*num_2saddles), -1);

   get_csr<<<grid,block>>>(thrust::raw_pointer_cast(&s2xmax[0]), thrust::raw_pointer_cast(&d_s2xmax_csr_vals[0]), thrust::raw_pointer_cast(&d_s2xmax_csr_colinds[0]), thrust::raw_pointer_cast(&d_s2xmax_rowptr[0]), *num_2saddles, 2);  

   int s2xmax_nnz = 0;
   s2xmax_nnz = thrust::copy_if(d_s2xmax_csr_vals.begin(), d_s2xmax_csr_vals.end(), d_s2xmax_csr_vals.begin(), is_not_zero_or_negative()) - d_s2xmax_csr_vals.begin();
  d_s2xmax_csr_vals.resize(s2xmax_nnz);

  thrust::inclusive_scan(d_s2xmax_rowptr.begin(), d_s2xmax_rowptr.end(), d_s2xmax_rowptr.begin());

  thrust::copy_if(d_s2xmax_csr_colinds.begin(), d_s2xmax_csr_colinds.end(), d_s2xmax_csr_colinds.begin(), is_not_negative());
  d_s2xmax_csr_colinds.resize(s2xmax_nnz);

  //printf("s2xmax vals: %d\n", s2xmax_nnz);

  //Copying CSR to host
  thrust::device_vector<short> d_max_csr(3*s2xmax_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_max_csr[0]), thrust::raw_pointer_cast(&d_max_coords[0]), 
				thrust::raw_pointer_cast(&d_s2xmax_csr_colinds[0]), s2xmax_nnz);

  h_max_csr.resize(3*s2xmax_nnz);
  h_conns_csr.resize(s2xmax_nnz);
  h_rowptr.resize((*num_2saddles)+1);

  h_max_csr = d_max_csr;
  h_conns_csr = d_s2xmax_csr_vals;
  h_rowptr = d_s2xmax_rowptr;

  d_max_csr.clear();
  d_max_csr.shrink_to_fit();


  //CSC data structures
  thrust::device_vector<float> d_s2xmax_csc_vals(s2xmax_nnz, 0);
  thrust::device_vector<int> d_s2xmax_csc_rowinds(s2xmax_nnz, 0);
  thrust::device_vector<int> d_s2xmax_colptr(num_max+1, 0);

  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseCreate(&handle);

  status = cusparseScsr2csc(handle, *num_2saddles, num_max, s2xmax_nnz, 
			thrust::raw_pointer_cast(&d_s2xmax_csr_vals[0]), thrust::raw_pointer_cast(&d_s2xmax_rowptr[0]), 
			thrust::raw_pointer_cast(&d_s2xmax_csr_colinds[0]), thrust::raw_pointer_cast(&d_s2xmax_csc_vals[0]), 
			thrust::raw_pointer_cast(&d_s2xmax_csc_rowinds[0]), thrust::raw_pointer_cast(&d_s2xmax_colptr[0]), 
			CUSPARSE_ACTION_NUMERIC, CUSPARSE_INDEX_BASE_ZERO);
  CuSparse_Check(status);

  //Clearing CSR
  d_s2xmax_csr_vals.clear();
  d_s2xmax_csr_vals.shrink_to_fit();
  d_s2xmax_rowptr.clear();
  d_s2xmax_rowptr.shrink_to_fit();
  d_s2xmax_csr_colinds.clear();
  d_s2xmax_csr_colinds.shrink_to_fit();
  

  //Copying CSC to host
  thrust::device_vector<short> d_s2_csc(3*s2xmax_nnz);

  copy_coords<<<grid,block>>>(ext_rect, thrust::raw_pointer_cast(&d_s2_csc[0]), thrust::raw_pointer_cast(&d_2sad_coords[0]),
				 thrust::raw_pointer_cast(&d_s2xmax_csc_rowinds[0]), s2xmax_nnz);

  h_s2_csc.resize(3*s2xmax_nnz);
  h_conns_csc.resize(s2xmax_nnz);
  h_colptr.resize(num_max+1);

  h_s2_csc = d_s2_csc;
  h_conns_csc = d_s2xmax_csc_vals;
  h_colptr = d_s2xmax_colptr;

}



