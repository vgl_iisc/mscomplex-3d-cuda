#include "cutil.h"

typedef short3 cell_t;
typedef uchar flag_t;


inline __device__ int row_order(int xd, int ys, int width)
{
  int id;
  id = (width*ys) + xd; //Takes zero indexing into account 
  return id;
}

inline __device__ int col_order(int xd, int ys, int height)
{
  int id;
  id = (height*xd) + ys; //Takes zero indexing into account
  return id;
}

inline __device__ int return_ind_jn(int pt, int* jn_scan, int* visited_scan)
{
  int pt_id = visited_scan[pt]-1;
  int coord = jn_scan[pt_id]-1;
  return coord;
}

inline __device__ int return_ind_sad(int pt, int* sads_scan)
{
  int coord = sads_scan[pt]-1;
  return coord;
}

inline __device__ int return_src(int dest_id, int* src_coords)
{
  int src = dest_id/4; //Returns original source of path tracing
  return src_coords[src];
}
