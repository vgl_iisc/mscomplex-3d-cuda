#include "cutil.h"
#include <matrix_mult_util.h>

typedef short3 cell_t;
typedef uchar flag_t;

template<int pos>
inline __device__ void get_path_source_dns(int i, cell_t c, flag_t fg, const rect_t ext_rect,
				       cell_t cfct, cudaSurfaceObject_t flag_surface,
				       int* srcs_scan, int* j_scan, int* sad2_scan,
				       int* visited, int* visited_scan, int* junction_pts,
				       int* dest, float* pxj, float* px2sad,
				       int p_srcs, int j_dests, int sad2_dests)
{ 
 if(ext_rect.contains(cfct))
 {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddles
     {
       int x_dest = return_ind_sad(ext_rect.c_to_i(cfct), sad2_scan);
       int y_src;
       if(is_critical(fg) && cell_dim(c)==1) //Source is a 1-saddle
	   y_src = return_ind_sad(ext_rect.c_to_i(c), srcs_scan);
       else  
	   y_src = return_ind_jn(ext_rect.c_to_i(c), srcs_scan, visited_scan); //Source is a junction

       //Populate jx2sad or 1sadx2sad matrix
       px2sad[col_order(x_dest, y_src, p_srcs)] = 1;
       //px2sad[row_order(x_dest, y_src, sad2_dests)] = 1;
              
       dest[(4*i)+pos] = -1;
       
       return;
     }
   
   else if(!is_critical(fg_cfct)) //In pxj, p=rows j=columns, (j,p) = (x,y)
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair
     
     if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
     {
        if(junction_pts[visited_scan[p]-1] == 1) //visited_scan[p]-1 is p's index in junction_pts
	 {
	   int x_dest = return_ind_jn(p, j_scan, visited_scan);
	   int y_src;
	   if(is_critical(fg) && cell_dim(c)==1) //Source is a 1-saddle
	     y_src = return_ind_sad(ext_rect.c_to_i(c), srcs_scan);
	   else
	     y_src = return_ind_jn(ext_rect.c_to_i(c), srcs_scan, visited_scan); //Source is a junction

	    //Populate jxj or 1sadxj matrix 
	    pxj[col_order(x_dest, y_src, p_srcs)] = 1;
	    //pxj[row_order(x_dest, y_src, j_dests)] = 1;
	    	     
	   dest[(4*i)+pos] = -1;
	 }
       
        else
	 {
	 dest[(4*i)+pos] = p;
	 }

	return;
     }
     
   }

 }
 dest[(4*i)+pos] = -1;
}


inline __device__ bool trace_path_dns(int i, cell_t c, flag_t fg, const rect_t ext_rect,
				  cell_t cfct, cudaSurfaceObject_t flag_surface,
				  int* srcs_coords, int* srcs_scan, int* j_scan,
				  int* sad2_scan, int p_srcs, int* visited,
				  int* visited_scan, int* junction_pts, int* dest,
				  float* pxj, float* px2sad, int j_dests, int sad2_dests)
{
 if(ext_rect.contains(cfct))
 {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);
   
   if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddles
     {
       //Populate jx2sad or 1sadx2sad matrix

       int x_dest = return_ind_sad(ext_rect.c_to_i(cfct), sad2_scan);
       int src_id = return_src(i, srcs_coords);
       flag_t fg_src;
       cell_t src = ext_rect.i_to_c(src_id);
       surf3Dread (&fg_src, flag_surface, src.x*sizeof(flag_t), src.y, src.z);

       int y_src;
       if(is_critical(fg_src) && cell_dim(src)==1)
       y_src = return_ind_sad(src_id, srcs_scan); //Source is a 1-saddle      
       else
       y_src = return_ind_jn(src_id, srcs_scan, visited_scan);  //Source is a junction
       
       px2sad[col_order(x_dest, y_src, p_srcs)] += 1;
       //px2sad[row_order(x_dest, y_src, sad2_dests)] += 1;
      	 
       dest[i] = -1;
       
       return true;
     }
   
   else if(!is_critical(fg_cfct))
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair

     if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
     {
       
        if(junction_pts[visited_scan[p]-1] == 1)
	 {
	   //Populate jxj or 1sadxj matrix
	   
	   int src_id = return_src(i, srcs_coords);
	   int x_dest = return_ind_jn(p, j_scan, visited_scan);
	   flag_t fg_src;
	   cell_t src = ext_rect.i_to_c(src_id);
	   surf3Dread (&fg_src, flag_surface, src.x*sizeof(flag_t), src.y, src.z);

	   int y_src;
	   if(is_critical(fg_src) && cell_dim(src)==1)
	   y_src = return_ind_sad(src_id, srcs_scan); //Source is a 1-saddle      
	   else
	   y_src = return_ind_jn(src_id, srcs_scan, visited_scan);  //Source is a junction

	   pxj[col_order(x_dest, y_src, p_srcs)] += 1;
	   //pxj[row_order(x_dest, y_src, j_dests)] += 1;
	   	   
	   dest[i] = -1;
	 }
       
        else
	 {
	   dest[i] = p;
	 }
	
	return true;
     }

   }
   
 }

 dest[i] = -1; //Includes dead end paths which do not reach 2-saddles from 1-saddles 
 return false;
}
