#ifndef conn_h
#define conn_h

#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>

#include "cutil.h"

typedef unsigned char flag_t;
typedef short3 cell_t;

template<int dim> 
__global__ void get_critical_pts(const rect_t ext_rect, cudaSurfaceObject_t flag_surface,
				 int* crit_pts_scan)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells();

  for(int i=tid; i < num_cells; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(i);
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
   
    if((is_critical(fg)) && (cell_dim(c)==dim))
      crit_pts_scan[i] = 1;

    else 
      crit_pts_scan[i] = 0;
   } 

}

 __global__ void stream_compact(cudaSurfaceObject_t flag_surface, int* scanned,
				int* compacted, int scan_size);

#endif
