#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include<cutil.h>
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<cuda_runtime.h>
#include<fstream>
#include<bfs_util.h>

#include <cusparse.h>

#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

typedef unsigned char flag_t;
typedef short3 cell_t;

//Matrix multiplication for A x B = C
//A(m,k) * B(k,n) = C(m,n)
//m = rowsA, n = colsB, k = colsA = rowsB
//A = sad1xj, B = jxj, m = sad1, n = j, k = j

#define CuSparse_Check(x) {cusparseStatus_t _c=x; if (_c != CUSPARSE_STATUS_SUCCESS) {printf("CuSparse Failed: %d, line: %d\n", (int)_c, __LINE__); exit(-1);}}

extern "C" void cusparse_matrix_mult(thrust::device_vector<float> &sad1xj_vals, thrust::device_vector<int> &sad1xj_rowptr, thrust::device_vector<int> &sad1xj_colinds, int nnz_sad1xj, thrust::device_vector<float> &jxj_vals, thrust::device_vector<int> &jxj_rowptr, thrust::device_vector<int> &jxj_colinds, int nnz_jxj, thrust::device_vector<float> &jxsad2_vals, thrust::device_vector<int> &jxsad2_rowptr, thrust::device_vector<int> &jxsad2_colinds, int nnz_jxsad2, thrust::device_vector<float> &s1xs2_vals, thrust::device_vector<int> &s1xs2_rowptr, thrust::device_vector<int> &s1xs2_colinds, int &nnz_s1xs2, int rowsA, int colsA, int colsB, int colsB2)
{
  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseCreate(&handle);
  
  cusparseMatDescr_t descr_jxj, descr_sad1xj, descr_C, descr_B2, descr_C2, descr_store, descr_temp, descr_s1xs2;

  cusparseCreateMatDescr(&descr_sad1xj); //A
  cusparseCreateMatDescr(&descr_jxj); //B
  cusparseCreateMatDescr(&descr_C); //C
  cusparseCreateMatDescr(&descr_B2); //B2
  cusparseCreateMatDescr(&descr_C2); //C2
  cusparseCreateMatDescr(&descr_store); //storage
  cusparseCreateMatDescr(&descr_temp);
  cusparseCreateMatDescr(&descr_s1xs2);

  cusparseSetMatType(descr_sad1xj, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_jxj, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_C, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_B2, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_C2, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_store, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_temp, CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatType(descr_s1xs2, CUSPARSE_MATRIX_TYPE_GENERAL);

  cusparseSetMatIndexBase(descr_sad1xj, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_jxj, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_C, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_B2, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_C2, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_store, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_temp, CUSPARSE_INDEX_BASE_ZERO);
  cusparseSetMatIndexBase(descr_s1xs2, CUSPARSE_INDEX_BASE_ZERO);

  cusparseOperation_t trans_type = CUSPARSE_OPERATION_NON_TRANSPOSE;
  cusparseOperation_t trans_sad1xj = CUSPARSE_OPERATION_NON_TRANSPOSE;
  cusparseOperation_t trans_jxj = CUSPARSE_OPERATION_NON_TRANSPOSE;

  //Initializing C
  thrust::device_vector<int> C_rowptr(rowsA+1, 0); //C
  thrust::device_vector<float> C_vals;
  thrust::device_vector<int> C_colinds;
  //int colsC = colsB;

  //Initializing storage matrix (CSR) to A
  thrust::device_vector<float> store_vals = sad1xj_vals;
  thrust::device_vector<int> store_rowptr = sad1xj_rowptr;
  thrust::device_vector<int> store_colinds = sad1xj_colinds;
  int nnz_store = nnz_sad1xj;

  int base_temp, nnz_temp; //For all matrix addition operations
  int* nnz_temp_hostptr = &nnz_temp;
  cusparseSetPointerMode(handle, CUSPARSE_POINTER_MODE_HOST);

  thrust::device_vector<float> temp_vals; //Temporarily stores the addition between storage matrix and A
  thrust::device_vector<int> temp_rowptr(rowsA+1);
  thrust::device_vector<int> temp_colinds;
  
  int iter = 1;
  const float alpha = 1.0;
  const float beta = 1.0;

  int base_C, nnz_C;
  int* nnzTotalDevHostPtr = &nnz_C;
  cusparseSetPointerMode(handle, CUSPARSE_POINTER_MODE_HOST);

  while(true)
 {
   //Gives you nnz in C, have to compute the nnz pattern in the output matrix first
   status = cusparseXcsrgemmNnz(handle, trans_sad1xj, trans_jxj, rowsA, colsB, colsA, descr_sad1xj, nnz_sad1xj, thrust::raw_pointer_cast(&sad1xj_rowptr[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]), descr_jxj, nnz_jxj, thrust::raw_pointer_cast(&jxj_rowptr[0]), thrust::raw_pointer_cast(&jxj_colinds[0]), descr_C, thrust::raw_pointer_cast(&C_rowptr[0]), nnzTotalDevHostPtr);
	  
   CuSparse_Check(status);

  if(NULL != nnzTotalDevHostPtr) //nnz_C was successfully updated
   {
   nnz_C = *nnzTotalDevHostPtr;
   }
	   
   else
   {
   cutilSafeCall(cudaMemcpy(&nnz_C, thrust::raw_pointer_cast(&C_rowptr[rowsA]), sizeof(int), cudaMemcpyDeviceToHost));
   cutilSafeCall(cudaMemcpy(&base_C, thrust::raw_pointer_cast(&C_rowptr[0]), sizeof(int), cudaMemcpyDeviceToHost));
   nnz_C -= base_C;
   }

  //printf("Size of C: %d\n", nnz_C); 

  C_vals.resize(nnz_C, 0);
  C_colinds.resize(nnz_C, 0);

  //Multiplication
  status = cusparseScsrgemm(handle, trans_sad1xj, trans_jxj, rowsA, colsB, colsA, 
			descr_sad1xj, nnz_sad1xj, 
			thrust::raw_pointer_cast(&sad1xj_vals[0]), thrust::raw_pointer_cast(&sad1xj_rowptr[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]),
			descr_jxj, nnz_jxj, 
			thrust::raw_pointer_cast(&jxj_vals[0]), thrust::raw_pointer_cast(&jxj_rowptr[0]), thrust::raw_pointer_cast(&jxj_colinds[0]), 
			descr_C, 
			thrust::raw_pointer_cast(&C_vals[0]), thrust::raw_pointer_cast(&C_rowptr[0]), thrust::raw_pointer_cast(&C_colinds[0]));

   CuSparse_Check(status);

   //Adding A to storage matrix in CSR format, mxn where m=1sad and n=j
   if (iter != 1) //First iter, storage matrix already init to A, hence no addition needed
   {
     status = cusparseXcsrgeamNnz(handle, rowsA, colsA, descr_sad1xj, nnz_sad1xj, 
			thrust::raw_pointer_cast(&sad1xj_rowptr[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]),
   	 		descr_store, nnz_store, thrust::raw_pointer_cast(&store_rowptr[0]), thrust::raw_pointer_cast(&store_colinds[0]),
     	   		descr_temp, thrust::raw_pointer_cast(&temp_rowptr[0]), nnz_temp_hostptr);
     CuSparse_Check(status); 

     if(NULL != nnz_temp_hostptr)
      {
    	nnz_temp = *nnz_temp_hostptr;
      }
   
    else
      {
    	cudaMemcpy(&nnz_temp, thrust::raw_pointer_cast(&temp_rowptr[rowsA]), sizeof(int), cudaMemcpyDeviceToHost);
   	cudaMemcpy(&base_temp, thrust::raw_pointer_cast(&temp_rowptr[0]), sizeof(int), cudaMemcpyDeviceToHost);
  	nnz_temp -= base_temp;
      }
  
   //printf("nnz_temp = %d\n", nnz_temp);

   temp_vals.resize(nnz_temp, 0);
   temp_colinds.resize(nnz_temp, 0);

   status = cusparseScsrgeam(handle, rowsA, colsA, &alpha, descr_sad1xj, nnz_sad1xj, 
       thrust::raw_pointer_cast(&sad1xj_vals[0]), thrust::raw_pointer_cast(&sad1xj_rowptr[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]), 
       &beta, descr_store, nnz_store, 
       thrust::raw_pointer_cast(&store_vals[0]), thrust::raw_pointer_cast(&store_rowptr[0]), thrust::raw_pointer_cast(&store_colinds[0]),
       descr_temp, thrust::raw_pointer_cast(&temp_vals[0]), thrust::raw_pointer_cast(&temp_rowptr[0]), thrust::raw_pointer_cast(&temp_colinds[0]));

   CuSparse_Check(status);

   //Copy temp to storage matrix and init nnz_store
   nnz_store = nnz_temp;
   store_vals.clear();
   store_rowptr.clear();
   store_colinds.clear();
   
   //Copy temp to storage matrix
   store_vals = temp_vals;
   store_rowptr = temp_rowptr;
   store_colinds = temp_colinds;

   //Clear temp and resize for next iter
   temp_vals.clear();
   temp_rowptr.clear();
   temp_colinds.clear();

   temp_rowptr.resize(rowsA+1, 0);

   }
 
   if(C_vals.size() == 0)
	break;

   //Clear A
   sad1xj_vals.clear();
   sad1xj_rowptr.clear();
   sad1xj_colinds.clear();  
   //sad1xj_rowinds.clear(); //Row index init

   nnz_sad1xj = nnz_C;
   sad1xj_vals.resize(nnz_sad1xj, 0);
   sad1xj_rowptr.resize(rowsA+1, 0);
   sad1xj_colinds.resize(nnz_sad1xj, 0);  
   //sad1xj_rowinds.resize(nnz_sad1xj, 0);

   //Copy C to A
   sad1xj_vals = C_vals;
   sad1xj_rowptr = C_rowptr;
   sad1xj_colinds = C_colinds;

   //Clear C
   C_vals.clear();
   C_rowptr.clear();
   C_colinds.clear();

   //C_vals.resize(nnz_sad1xj, 0);
   C_rowptr.resize(rowsA+1, 0);
   //C_colinds.resize(nnz_sad1xj, 0);

   iter++;

  }

  //End of iterative sxj X jxj multiplication
  //printf("Iterations = %d\n", iter);

  thrust::device_vector<int> C2_rowptr(rowsA+1, 0); //C2 = 1sx2s obtained from (1sadxj) x (jx2sad)
  thrust::device_vector<float> C2_vals;
  thrust::device_vector<int> C2_colinds;

  int base_C2, nnz_C2;
  nnzTotalDevHostPtr = &nnz_C2;

  status = cusparseXcsrgemmNnz(handle, trans_type, trans_type, rowsA, colsB2, colsA, 
			descr_store, nnz_store, thrust::raw_pointer_cast(&store_rowptr[0]), thrust::raw_pointer_cast(&store_colinds[0]), 
			descr_B2, nnz_jxsad2, thrust::raw_pointer_cast(&jxsad2_rowptr[0]), thrust::raw_pointer_cast(&jxsad2_colinds[0]), 
			descr_C2, thrust::raw_pointer_cast(&C2_rowptr[0]), nnzTotalDevHostPtr); 
	  
   CuSparse_Check(status);

  if(NULL != nnzTotalDevHostPtr)
   {
   nnz_C2 = *nnzTotalDevHostPtr;
   }
	   
   else
   {
   cutilSafeCall(cudaMemcpy(&nnz_C2, thrust::raw_pointer_cast(&C2_rowptr[rowsA]), sizeof(int), cudaMemcpyDeviceToHost));
   cutilSafeCall(cudaMemcpy(&base_C2, thrust::raw_pointer_cast(&C2_rowptr[0]), sizeof(int), cudaMemcpyDeviceToHost));
   nnz_C2 -= base_C2;
   }

  //printf("Size of C2: %d\n", nnz_C2);

  C2_vals.resize(nnz_C2, 0);
  C2_colinds.resize(nnz_C2, 0);

  status = cusparseScsrgemm(handle, trans_type, trans_type, rowsA, colsB2, colsA, 
			descr_store, nnz_store, 
			thrust::raw_pointer_cast(&store_vals[0]), thrust::raw_pointer_cast(&store_rowptr[0]), thrust::raw_pointer_cast(&store_colinds[0]),
			descr_B2, nnz_jxsad2, 
			thrust::raw_pointer_cast(&jxsad2_vals[0]), thrust::raw_pointer_cast(&jxsad2_rowptr[0]), thrust::raw_pointer_cast(&jxsad2_colinds[0]), 
			descr_C2, 
			thrust::raw_pointer_cast(&C2_vals[0]), thrust::raw_pointer_cast(&C2_rowptr[0]), thrust::raw_pointer_cast(&C2_colinds[0]));

   CuSparse_Check(status);

  //Adding the two 1sx2s matrices
  base_temp = 0;
  nnz_temp = 0;
  nnz_temp_hostptr = &nnz_temp;

  status = cusparseXcsrgeamNnz(handle, rowsA, colsB2, descr_C2, nnz_C2, 
			thrust::raw_pointer_cast(&C2_rowptr[0]), thrust::raw_pointer_cast(&C2_colinds[0]),
   	 		descr_s1xs2, nnz_s1xs2, thrust::raw_pointer_cast(&s1xs2_rowptr[0]), thrust::raw_pointer_cast(&s1xs2_colinds[0]),
     	   		descr_temp, thrust::raw_pointer_cast(&temp_rowptr[0]), nnz_temp_hostptr);
     CuSparse_Check(status); 

     if(NULL != nnz_temp_hostptr)
      {
    	nnz_temp = *nnz_temp_hostptr;
      }
   
    else
      {
    	cudaMemcpy(&nnz_temp, thrust::raw_pointer_cast(&temp_rowptr[rowsA]), sizeof(int), cudaMemcpyDeviceToHost);
   	cudaMemcpy(&base_temp, thrust::raw_pointer_cast(&temp_rowptr[0]), sizeof(int), cudaMemcpyDeviceToHost);
  	nnz_temp -= base_temp;
      }
  
   //printf("nnz_temp_2 = %d\n", nnz_temp);

   temp_vals.resize(nnz_temp, 0);
   temp_colinds.resize(nnz_temp, 0);

   status = cusparseScsrgeam(handle, rowsA, colsB2, &alpha, descr_C2, nnz_C2, 
       thrust::raw_pointer_cast(&C2_vals[0]), thrust::raw_pointer_cast(&C2_rowptr[0]), thrust::raw_pointer_cast(&C2_colinds[0]), 
       &beta, descr_s1xs2, nnz_s1xs2, 
       thrust::raw_pointer_cast(&s1xs2_vals[0]), thrust::raw_pointer_cast(&s1xs2_rowptr[0]), thrust::raw_pointer_cast(&s1xs2_colinds[0]),
       descr_temp, thrust::raw_pointer_cast(&temp_vals[0]), thrust::raw_pointer_cast(&temp_rowptr[0]), thrust::raw_pointer_cast(&temp_colinds[0]));

   CuSparse_Check(status);
  
   s1xs2_vals.clear();
   s1xs2_rowptr.clear();
   s1xs2_colinds.clear();
   
   nnz_s1xs2 = nnz_temp;
   s1xs2_vals = temp_vals;
   s1xs2_rowptr = temp_rowptr;
   s1xs2_colinds = temp_colinds;
 
}


//Additional Code

  //Row index vector of A to dump into dense storage matrix
  //thrust::device_vector<int> sad1xj_rowinds(nnz_sad1xj, 0);

  //sad1xj_rowinds.clear();
   //sad1xj_rowinds.shrink_to_fit();

   /*sad1xj_vals.clear();
   sad1xj_vals.resize(rowsA*colsA, 0);  

   sad1xj_rowptr.clear();
   sad1xj_colinds.clear();*/
   
   /*
   //Convert dense matrix to CSR
   nnz_sad1xj = 0;
   thrust::device_vector<int> d_1sxj_nnz_row(rowsA, 0);
   
   status = cusparseSnnz(handle, CUSPARSE_DIRECTION_ROW, rowsA, colsA, descr_sp_sad1xj, thrust::raw_pointer_cast(&sp_sad1xj[0]), rowsA, thrust::raw_pointer_cast(&d_1sxj_nnz_row[0]), &nnz_sad1xj); 
  CuSparse_Check(status);

   //printf("1sadxj dense matrix vals: %d\n", nnz_sad1xj);  

   sad1xj_vals.resize(nnz_sad1xj);
   sad1xj_rowptr.resize(rowsA+1, 0);
   sad1xj_colinds.resize(nnz_sad1xj, 0); 

   status = cusparseSdense2csr(handle, rowsA, colsA, descr_sp_sad1xj, thrust::raw_pointer_cast(&sp_sad1xj[0]), rowsA, thrust::raw_pointer_cast(&d_1sxj_nnz_row[0]), thrust::raw_pointer_cast(&sad1xj_vals[0]), thrust::raw_pointer_cast(&sad1xj_rowptr[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]));
  CuSparse_Check(status);

   //You now have your final 1sadxj in CSR format
  /**************/


   //Storing dense storage matrix
   //status = cusparseXcsr2coo(handle, thrust::raw_pointer_cast(&sad1xj_rowptr[0]), nnz_sad1xj, rowsA, thrust::raw_pointer_cast(&sad1xj_rowinds[0]), CUSPARSE_INDEX_BASE_ZERO);
   //CuSparse_Check(status);
   //store_dense_matrix<<<grid,block>>>(thrust::raw_pointer_cast(&sad1xj_vals[0]), thrust::raw_pointer_cast(&sad1xj_rowinds[0]), thrust::raw_pointer_cast(&sad1xj_colinds[0]), thrust::raw_pointer_cast(&dense_sad1xj[0]), rowsA, colsA, nnz_sad1xj);

//Convert C2 back into dense
  //thrust::device_vector<int> C2_rowinds(nnz_C2, 0);
  //status = cusparseXcsr2coo(handle, thrust::raw_pointer_cast(&C2_rowptr[0]), nnz_C2, rowsA, thrust::raw_pointer_cast(&C2_rowinds[0]), CUSPARSE_INDEX_BASE_ZERO);

   //CuSparse_Check(status);

  //Fill dense matrix sad1xsad2_out using col-order
  //store_dense_matrix<<<grid,block>>>(thrust::raw_pointer_cast(&C2_vals[0]), thrust::raw_pointer_cast(&C2_rowinds[0]), thrust::raw_pointer_cast(&C2_colinds[0]), thrust::raw_pointer_cast(&sad1xsad2_out[0]), rowsA, colsB2, nnz_C2);






