#include "cutil.h"
#include<iostream>
#include<stdio.h>
#include<cuda_runtime.h>
#include<fstream>

//#define NUM_THREADS_PER_BLOCK 256
//#define NUM_BLOCKS 32
#define NUM_THREADS_PER_BLOCK 512
#define NUM_BLOCKS 64

cudaArray *d_flag = 0;
cudaArray *d_func = 0;

cudaTextureObject_t func_tex = 0;
cudaSurfaceObject_t flag_surface = 0;

//surface<void, 3> flag_surface;
//surface<void, 1> cp_surface;

typedef float func_t;
typedef unsigned char flag_t;

__device__ bool compare_verts(cell_t v1, cell_t v2, cudaTextureObject_t func_tex)
{
  func_t f1 = tex3D<func_t>(func_tex, v1.x/2, v1.y/2, v1.z/2);
  func_t f2 = tex3D<func_t>(func_tex, v2.x/2, v2.y/2, v2.z/2);

  if( f1 != f2)
    return f1 < f2;
  
  return v1 < v2;
}

__device__ bool compare_edges(const rect_t ext_rect, cell_t e1, cell_t e2, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
  flag_t f1, f2;
  surf3Dread(&f1, flag_surface, e1.x*sizeof(flag_t), e1.y, e1.z);
  surf3Dread(&f2, flag_surface, e2.x*sizeof(flag_t), e2.y, e2.z);

  cell_t v1 = flag_to_mxfct(e1,f1);
  cell_t v2 = flag_to_mxfct(e2,f2);

  if( v1 == v2)
  {
    v1 = second_max_facet(e1,v1); 
    v2 = second_max_facet(e2,v2);

    int boundary_ct1 = ext_rect.boundaryCount(v1); 
    int boundary_ct2 = ext_rect.boundaryCount(v2); 

    if(boundary_ct1 != boundary_ct2)
      return (boundary_ct2 < boundary_ct1);
  }

  return compare_verts(v1,v2,func_tex);
}

__device__ bool compare_faces(const rect_t ext_rect, cell_t f1, cell_t f2, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
  flag_t fg1, fg2;
  surf3Dread(&fg1, flag_surface, f1.x*sizeof(flag_t), f1.y, f1.z);
  surf3Dread(&fg2, flag_surface, f2.x*sizeof(flag_t), f2.y, f2.z);  

  cell_t e1 = flag_to_mxfct(f1,fg1);
  cell_t e2 = flag_to_mxfct(f2,fg2);

  if(e1 == e2)
  {
    e1 = second_max_facet(f1,e1);
    e2 = second_max_facet(f2,e2);

    int boundary_ct1 = ext_rect.boundaryCount(e1); 
    int boundary_ct2 = ext_rect.boundaryCount(e2); 

    if(boundary_ct1 != boundary_ct2)
      return (boundary_ct2 < boundary_ct1);
  }

  return compare_edges(ext_rect,e1,e2,func_tex,flag_surface);
}

__device__ bool compare_cubes(const rect_t ext_rect, cell_t c1, cell_t c2, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{

  flag_t f1, f2;
  surf3Dread(&f1, flag_surface, c1.x*sizeof(flag_t), c1.y, c1.z);
  surf3Dread(&f2, flag_surface, c2.x*sizeof(flag_t), c2.y, c2.z); 

  cell_t fc1 = flag_to_mxfct(c1,f1);
  cell_t fc2 = flag_to_mxfct(c2,f2);

  if(fc1 == fc2)
  {
    fc1 = second_max_facet(c1,fc1);
    fc2 = second_max_facet(c2,fc2);

    int boundary_ct1 = ext_rect.boundaryCount(fc1);
    int boundary_ct2 = ext_rect.boundaryCount(fc2); 

    if(boundary_ct1 != boundary_ct2)
      return (boundary_ct2 < boundary_ct1);
  }

  return compare_faces(ext_rect,fc1,fc2,func_tex,flag_surface);
}

__global__ void assign_maxfacet_vert (const rect_t ext_rect, cudaSurfaceObject_t flag_surface)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  rect_t cell_rect(ext_rect.lc, ext_rect.uc);
  int num_cells = cell_rect.num_cells2();

  for(int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t c = cell_rect.i_to_c2(i);
    flag_t val = 0;
    surf3Dwrite(val, flag_surface, c.x*sizeof(flag_t), c.y, c.z); //Directly filling in 0 gives an address misalignment
  }
}

__global__ void assign_maxfacet_edge (const rect_t ext_rect, const cell_t edir, 
					cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;
  
  //You give it the 1st and last edge/face, it will iterate through all edges/faces respectively
  rect_t cell_rect(ext_rect.lc+edir, ext_rect.uc-edir); //Double grid without border vertices

  int num_cells = cell_rect.num_cells2();
  for( int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t e = cell_rect.i_to_c2(i); //All edges in the double grid    
    //if(!ext_rect.contains(e))
    //return;

    cell_t v1 = e - edir;
    cell_t v2 = e + edir;

    cell_t v = v1;

    if(compare_verts(v,v2,func_tex))
      v = v2;

    //cell_flags[ext_rect.c_to_i(e)] = mxfct_to_flag(e,v);
    flag_t flagval = mxfct_to_flag(e,v);

    surf3Dwrite(flagval, flag_surface, e.x, e.y, e.z);
  }
}

__global__ void assign_maxfacet_face (const rect_t ext_rect, const cell_t fdir1, const cell_t fdir2, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)

{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  cell_t fdir = fdir1+fdir2;
  rect_t cell_rect(ext_rect.lc+fdir, ext_rect.uc-fdir);
 
  int num_cells = cell_rect.num_cells2();
  for( int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t f = cell_rect.i_to_c2(i); //Faces in the double grid
    
    cell_t e1 = f-fdir1;
    cell_t e2 = f+fdir1;
    cell_t e3 = f-fdir2;
    cell_t e4 = f+fdir2;

    cell_t e = e1;

    if(compare_edges(ext_rect,e,e2,func_tex,flag_surface))
      e = e2;

    if(compare_edges(ext_rect,e,e3,func_tex,flag_surface))
      e = e3;

    if(compare_edges(ext_rect,e,e4,func_tex,flag_surface))
      e = e4;

    //cell_flags[ext_rect.c_to_i(f)] = mxfct_to_flag(f,e);
    flag_t flagval = mxfct_to_flag(f,e);
    surf3Dwrite(flagval, flag_surface, f.x, f.y, f.z);
   
  }
}

//Recursively points to the maximal facet
__global__  void assign_maxfacet_cube (const rect_t ext_rect, cudaTextureObject_t func_tex, 
					cudaSurfaceObject_t flag_surface)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  rect_t cell_rect(ext_rect.lc+1, ext_rect.uc-1);
  int num_cells = cell_rect.num_cells2();
  
  for( int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t c = cell_rect.i_to_c2(i);
    //if(!ext_rect.contains(c))
    //return;

    cell_t f1 = c-mk_cell(1,0,0);
    cell_t f2 = c+mk_cell(1,0,0);
    cell_t f3 = c-mk_cell(0,1,0);
    cell_t f4 = c+mk_cell(0,1,0);
    cell_t f5 = c-mk_cell(0,0,1);
    cell_t f6 = c+mk_cell(0,0,1);

    cell_t f = f1;

    if(compare_faces(ext_rect,f,f2,func_tex,flag_surface))
      f = f2;

    if(compare_faces(ext_rect,f,f3,func_tex,flag_surface))
      f = f3;

    if(compare_faces(ext_rect,f,f4,func_tex,flag_surface))
      f = f4;

    if(compare_faces(ext_rect,f,f5,func_tex,flag_surface))
      f = f5;

    if(compare_faces(ext_rect,f,f6,func_tex,flag_surface))
      f = f6;

    //cell_flags[ext_rect.c_to_i(c)] = mxfct_to_flag(c,f);
    flag_t flagval = mxfct_to_flag(c,f);
    surf3Dwrite(flagval, flag_surface, c.x, c.y, c.z);
  }
}

__device__ bool is_pairable (const rect_t ext_rect, cell_t p, cell_t q, 
				cudaSurfaceObject_t flag_surface)
{
   
    if(!ext_rect.contains(q))
    return false;

    if(ext_rect.is_boundary(p) != ext_rect.is_boundary(q)) 
    return false;

    flag_t fg;
    surf3Dread(&fg, flag_surface, q.x*sizeof(flag_t), q.y, q.z);
   
    return (p == flag_to_mxfct(q,fg));
}

__device__ void mark_pair (cell_t p, cell_t q, cudaSurfaceObject_t flag_surface)
{
    flag_t pfg, qfg;
    surf3Dread(&pfg, flag_surface, p.x*sizeof(flag_t), p.y, p.z);
    surf3Dread(&qfg, flag_surface, q.x*sizeof(flag_t), q.y, q.z); 

    pfg |= pair_to_flag(p,q);
    qfg |= pair_to_flag(q,p);

    surf3Dwrite(pfg, flag_surface, p.x, p.y, p.z);
    surf3Dwrite(qfg, flag_surface, q.x, q.y, q.z);
}

__global__ void assign_pair_vert(const rect_t ext_rect, cudaTextureObject_t func_tex, 
				cudaSurfaceObject_t flag_surface)
{
   int tid = blockIdx.x*blockDim.x + threadIdx.x;
   int num_thds= gridDim.x*blockDim.x;
   rect_t cell_rect(ext_rect.lc, ext_rect.uc); 
   int num_cells = cell_rect.num_cells2();

   for(int i = tid ; i < num_cells; i += num_thds)
  {
   cell_t v = cell_rect.i_to_c2(i);

   if(!ext_rect.contains(v))
   continue;

   cell_t e0 = v - mk_cell(1,0,0);
   cell_t e1 = v + mk_cell(1,0,0);
   cell_t e2 = v - mk_cell(0,1,0);
   cell_t e3 = v + mk_cell(0,1,0);
   cell_t e4 = v - mk_cell(0,0,1);
   cell_t e5 = v + mk_cell(0,0,1);

   cell_t einvalid = mk_cell(-1,-1,-1);
   cell_t e = einvalid;

    if(is_pairable(ext_rect, v, e0, flag_surface))
     e = e0;

    if((is_pairable(ext_rect, v, e1, flag_surface)) && ((e == einvalid) || (compare_edges(ext_rect,e1,e,func_tex,flag_surface))))
     e = e1;

    if((is_pairable(ext_rect, v, e2, flag_surface)) && ((e == einvalid) || (compare_edges(ext_rect,e2,e,func_tex,flag_surface))))
    e = e2;

    if((is_pairable(ext_rect, v, e3, flag_surface)) && ((e == einvalid) || (compare_edges(ext_rect,e3,e,func_tex,flag_surface))))
    e = e3;

    if((is_pairable(ext_rect, v, e4, flag_surface)) && ((e == einvalid) || (compare_edges(ext_rect,e4,e,func_tex,flag_surface))))
    e = e4;

    if((is_pairable(ext_rect, v, e5, flag_surface)) && ((e == einvalid) || (compare_edges(ext_rect,e5,e,func_tex,flag_surface))))
    e = e5;

   if(!(e == einvalid))
   mark_pair(v, e, flag_surface);
  }
}

__global__ void assign_pair_edge (const rect_t ext_rect, cell_t edir, cell_t d0, cell_t d1, 
			cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
   int tid = blockIdx.x*blockDim.x + threadIdx.x;
   int num_thds= gridDim.x*blockDim.x;

   rect_t cell_rect(ext_rect.lc+edir,ext_rect.uc-edir);
   int num_cells = cell_rect.num_cells2();     

   for( int i = tid; i < num_cells; i += num_thds)
  {
    cell_t e = cell_rect.i_to_c2(i);
    if(!ext_rect.contains(e))
    continue;
    
    cell_t f0 = e - d0;
    cell_t f1 = e + d0;
    cell_t f2 = e - d1;
    cell_t f3 = e + d1;

    cell_t finvalid = mk_cell(-1, -1, -1);
    cell_t f = finvalid;

    if(is_pairable(ext_rect, e, f0, flag_surface))
    f = f0;

    if((is_pairable(ext_rect, e, f1, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f1, f, func_tex, flag_surface))))
    f = f1;

    if((is_pairable(ext_rect, e, f2, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f2, f, func_tex, flag_surface))))
    f = f2;

    if((is_pairable(ext_rect, e, f3, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f3, f, func_tex, flag_surface))))
    f = f3;

    if(!(f == finvalid))
    mark_pair(e, f, flag_surface);
  }
}

__global__ void assign_pair_face (const rect_t ext_rect, cell_t fdir, cell_t d, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
   int tid = blockIdx.x*blockDim.x + threadIdx.x;
   int num_thds= gridDim.x*blockDim.x;

   rect_t cell_rect(ext_rect.lc+fdir,ext_rect.uc-fdir);
   int num_cells = cell_rect.num_cells2();     

   for( int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t f = cell_rect.i_to_c2(i);    
    if(!ext_rect.contains(f))
    continue;  

    cell_t c0 = f - d;
    cell_t c1 = f + d;
    
    cell_t cinvalid = mk_cell(-1,-1,-1);
    cell_t c = cinvalid;

    if(is_pairable(ext_rect, f, c0, flag_surface))
     c = c0;
 
    if((is_pairable(ext_rect, f, c1, flag_surface)) && ((c == cinvalid) || (compare_cubes(ext_rect, c1, c, func_tex, flag_surface))))
    c = c1;

    if(!(c == cinvalid))
    mark_pair(f, c, flag_surface);
  }
}

__device__ bool is_pairable2 (const rect_t ext_rect, cell_t p, cell_t p_mf, cell_t q, 
				cudaSurfaceObject_t flag_surface)
{
    if(!ext_rect.contains(q))
    return false;

    if(ext_rect.is_boundary(p) != ext_rect.is_boundary(q))
    return false;  

    flag_t q_fg, q_mf_fg;
     
    surf3Dread(&q_fg, flag_surface, q.x*sizeof(flag_t), q.y, q.z);
    cell_t q_mf = flag_to_mxfct(q, q_fg);
    
    surf3Dread(&q_mf_fg, flag_surface, q_mf.x*sizeof(flag_t), q_mf.y, q_mf.z);    
    cell_t q_mf_mf = flag_to_mxfct(q_mf, q_mf_fg);

    return((!(q_mf==p)) && (q_mf_mf == p_mf));
}

__global__ void assign_pair_edge2 (const rect_t ext_rect, cell_t edir, cell_t d0, cell_t d1, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    int num_thds= gridDim.x*blockDim.x;

    rect_t cell_rect(ext_rect.lc+edir,ext_rect.uc-edir); 
    int num_cells = cell_rect.num_cells2();     

    for( int i = tid ; i < num_cells; i += num_thds)
    {
    cell_t e = cell_rect.i_to_c2(i);
    if(!ext_rect.contains(e))
    continue;

    flag_t e_fg;
    surf3Dread(&e_fg, flag_surface, e.x*sizeof(flag_t), e.y, e.z);

    if(is_paired(e_fg))
    continue;

    cell_t e_mf = flag_to_mxfct(e, e_fg);

    cell_t f0 = e - d0;
    cell_t f1 = e + d0;
    cell_t f2 = e - d1;
    cell_t f3 = e + d1;

    cell_t finvalid = mk_cell(-1,-1,-1);
    cell_t f = finvalid;
    
    if(is_pairable2(ext_rect, e, e_mf, f0, flag_surface))
    f = f0;

    if((is_pairable2(ext_rect, e, e_mf, f1, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f1, f, func_tex, flag_surface))))
    f = f1;

    if((is_pairable2(ext_rect, e, e_mf, f2, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f2, f, func_tex, flag_surface))))
    f = f2;

    if((is_pairable2(ext_rect, e, e_mf, f3, flag_surface)) && ((f == finvalid) || (compare_faces(ext_rect, f3, f, func_tex, flag_surface))))
    f = f3;

    if(!(f == finvalid))
    {
    flag_t f_fg;
    surf3Dread(&f_fg, flag_surface, f.x*sizeof(flag_t), f.y, f.z);

    if(!is_paired(f_fg))
      mark_pair(e, f, flag_surface);
    }
  }
}

__global__ void assign_pair_face2 (const rect_t ext_rect, cell_t fdir, cell_t d, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)

{
    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    int num_thds= gridDim.x*blockDim.x;

    rect_t cell_rect(ext_rect.lc+fdir, ext_rect.uc-fdir);
    int num_cells = cell_rect.num_cells2();     

  for( int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t f = cell_rect.i_to_c2(i);
    if(!ext_rect.contains(f))
    continue;
    
    flag_t f_fg;
    surf3Dread(&f_fg, flag_surface, f.x*sizeof(flag_t), f.y, f.z);
   
    if(is_paired(f_fg))
    continue;
 
    cell_t f_mf = flag_to_mxfct(f, f_fg);

    cell_t c0 = f - d;
    cell_t c1 = f + d;
    cell_t cinvalid = mk_cell(-1,-1,-1);
    cell_t c = cinvalid;

    if(is_pairable2(ext_rect, f, f_mf, c0, flag_surface))
    c = c0;

    if((is_pairable2(ext_rect, f, f_mf, c1, flag_surface)) && ((c == cinvalid) || (compare_cubes(ext_rect, c1, c, func_tex, flag_surface))))
    c = c1;

    if(!(c == cinvalid))
    {
    flag_t c_fg;
    surf3Dread(&c_fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(!is_paired(c_fg))
      mark_pair(f, c, flag_surface);
    }
  }
} 

__device__ bool is_pairable3 (cell_t p, cell_t p_mf, cell_t p_mf_mf, cell_t q, 
				cudaSurfaceObject_t flag_surface)
{
   flag_t q_fg;
   surf3Dread(&q_fg, flag_surface, q.x*sizeof(flag_t), q.y, q.z);
   cell_t q_mf = flag_to_mxfct(q, q_fg);

   flag_t q_mf_fg;
   surf3Dread(&q_mf_fg, flag_surface, q_mf.x*sizeof(flag_t), q_mf.y, q_mf.z);
   cell_t q_mf_mf = flag_to_mxfct(q_mf, q_mf_fg);

   flag_t q_mf_mf_fg;
   surf3Dread(&q_mf_mf_fg, flag_surface, q_mf_mf.x*sizeof(flag_t), q_mf_mf.y, q_mf_mf.z);
   cell_t q_mf_mf_mf = flag_to_mxfct(q_mf_mf, q_mf_mf_fg);

   return ((!(q_mf == p)) && (!(q_mf_mf == p_mf)) && (q_mf_mf_mf == p_mf_mf));
}

__global__ void assign_pair_face3 (const rect_t ext_rect, cell_t fdir, cell_t d, 
				cudaTextureObject_t func_tex, cudaSurfaceObject_t flag_surface)
{
    int tid = blockIdx.x*blockDim.x + threadIdx.x;
    int num_thds= gridDim.x*blockDim.x;

    rect_t cell_rect(ext_rect.lc+fdir, ext_rect.uc-fdir);
    int num_cells = cell_rect.num_cells2();     

  for(int i = tid ; i < num_cells; i += num_thds)
  {
    cell_t f = cell_rect.i_to_c2(i);
    if(!ext_rect.contains(f))
    continue;

    flag_t f_fg;
    surf3Dread(&f_fg, flag_surface, f.x*sizeof(flag_t), f.y, f.z);
    cell_t f_mf =  flag_to_mxfct(f,f_fg);

    if(is_paired(f_fg))
    continue;

    flag_t f_mf_fg;
    surf3Dread(&f_mf_fg, flag_surface, f_mf.x*sizeof(flag_t), f_mf.y, f_mf.z);
    cell_t f_mf_mf = flag_to_mxfct(f_mf,f_mf_fg);

    cell_t c0 = f - d;
    cell_t c1 = f + d;
    cell_t cinvalid  = mk_cell(-1,-1,-1);
    cell_t c = cinvalid;

   if(ext_rect.contains(c0) && (ext_rect.is_boundary(f) == ext_rect.is_boundary(c0)))
   {
    if (is_pairable3(f, f_mf, f_mf_mf, c0, flag_surface))
    c = c0;
   }

   if(ext_rect.contains(c1) && (ext_rect.is_boundary(f) == ext_rect.is_boundary(c1)))
   {
    if ((is_pairable3(f, f_mf, f_mf_mf, c1, flag_surface)) && ((c == cinvalid) || compare_cubes(ext_rect,c1,c,func_tex,flag_surface)))
    c = c1;
   }

    if(!(c == cinvalid))
    {
    flag_t c_fg;
    surf3Dread(&c_fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(!is_paired(c_fg))
      mark_pair(f, c, flag_surface);
    }
  }
}

__global__ void mark_cps(const rect_t ext_rect, cudaSurfaceObject_t flag_surface)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;

  int num_cells = ext_rect.num_cells(); 

  for(int i = tid; i < num_cells; i += num_thds)
  {
    cell_t c = ext_rect.i_to_c(i);
    flag_t fg;
    surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);

    if(is_paired(fg))
      continue;
   
    fg |= CELLFLAG_CRITICAL;
    surf3Dwrite(fg, flag_surface, c.x, c.y, c.z);
  }
}

__global__ void mark_boundary_cps(const rect_t ext_rect, rect_t bnd_rect, cell_t bnd_dir, cudaSurfaceObject_t flag_surface)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds= gridDim.x*blockDim.x;
 
  int num_cells = bnd_rect.num_cells(); 
  for(int i = tid; i < num_cells; i += num_thds)
  {
  cell_t c = bnd_rect.i_to_c(i);
  flag_t fg;
  surf3Dread(&fg, flag_surface, c.x*sizeof(flag_t), c.y, c.z);
  
  if(!is_paired(fg))
   continue;

  cell_t p = flag_to_pair(c,fg);
  if(!(p+bnd_dir==c) && !(c+bnd_dir==p))
   continue;

  fg |= CELLFLAG_CRITICAL;
  surf3Dwrite(fg, flag_surface, c.x, c.y, c.z);

  flag_t pfg = pair_to_flag(p,c)|mxfct_to_flag(p,c)|CELLFLAG_CRITICAL;
  surf3Dwrite(pfg, flag_surface, p.x, p.y, p.z);
 
  if(!ext_rect.contains(p))
   continue;
  }  
}

extern "C" cudaTextureObject_t assign_pairs_cuda(short* erptr, func_t* func, flag_t* flag, cudaSurfaceObject_t* flag_surf)
{
  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  
  cell_t func_span = (ext_rect.uc - ext_rect.lc)/2 + 1;
  const cudaExtent func_vol_extent = make_cudaExtent(func_span.x,func_span.y,func_span.z);

  //Create 3D array
  cudaChannelFormatDesc func_channel_desc = cudaCreateChannelDesc<func_t>();
  cutilSafeCall(cudaMalloc3DArray(&d_func, &func_channel_desc, func_vol_extent));
  //channelDesc describes the format of the value that is returned when fetching the texture

  //Pitched pointer
  cudaPitchedPtr func_pitched_ptr;
  func_pitched_ptr.ptr      = func;
  func_pitched_ptr.pitch    = func_vol_extent.width*sizeof(func_t);
  func_pitched_ptr.xsize    = func_vol_extent.width;
  func_pitched_ptr.ysize    = func_vol_extent.height;

  //Copy data to 3D array
  cudaMemcpy3DParms func_copy_params = {0};
  func_copy_params.srcPtr   = func_pitched_ptr;
  func_copy_params.dstArray = d_func;
  func_copy_params.extent   = func_vol_extent;
  func_copy_params.kind     = cudaMemcpyHostToDevice;
  cutilSafeCall(cudaMemcpy3D(&func_copy_params));

  cudaResourceDesc func_texResDes;
  memset(&func_texResDes,0,sizeof(cudaResourceDesc));
  
  func_texResDes.resType            = cudaResourceTypeArray;
  func_texResDes.res.array.array    = d_func;

  cudaTextureDesc func_texDescr;
  memset(&func_texDescr,0,sizeof(cudaTextureDesc));

  func_texDescr.normalizedCoords = false; 
  func_texDescr.filterMode = cudaFilterModePoint;
  func_texDescr.addressMode[0] = cudaAddressModeClamp;
  func_texDescr.addressMode[1] = cudaAddressModeClamp;
  func_texDescr.addressMode[2] = cudaAddressModeClamp;
  func_texDescr.readMode = cudaReadModeElementType;

  cutilSafeCall(cudaCreateTextureObject(&func_tex, &func_texResDes, &func_texDescr, NULL));

  int d_flag_size = ext_rect.num_cells()*sizeof(flag_t);
  
  //Create 3D array
  cudaChannelFormatDesc flag_channel_desc = cudaCreateChannelDesc<flag_t>();

  cell_t flag_span = (ext_rect.uc - ext_rect.lc) + 1; //Double grid capacity for flags
  cudaExtent flag_vol_extent =  make_cudaExtent(flag_span.x, flag_span.y, flag_span.z);

  cutilSafeCall(cudaMalloc3DArray(&d_flag, &flag_channel_desc, flag_vol_extent, cudaArraySurfaceLoadStore));

  cudaResourceDesc flag_surfResDes;
  memset(&flag_surfResDes,0,sizeof(cudaResourceDesc));
  
  flag_surfResDes.resType            = cudaResourceTypeArray;
  flag_surfResDes.res.array.array    = d_flag;

  cutilSafeCall(cudaCreateSurfaceObject(&flag_surface, &flag_surfResDes));
  
  *flag_surf = flag_surface;

  dim3 block(512,1);
  dim3 grid(64,1);

  assign_maxfacet_vert<<<grid,block>>>(ext_rect, flag_surface);
  
  //Parallelism for the next 3 kernels
  assign_maxfacet_edge<<<grid,block>>>(ext_rect, mk_cell(1,0,0), func_tex, flag_surface);
  assign_maxfacet_edge<<<grid,block>>>(ext_rect, mk_cell(0,1,0), func_tex, flag_surface);
  assign_maxfacet_edge<<<grid,block>>>(ext_rect, mk_cell(0,0,1), func_tex, flag_surface);

  assign_maxfacet_face<<<grid,block>>>(ext_rect, mk_cell(1,0,0), mk_cell(0,1,0), func_tex, flag_surface);
  assign_maxfacet_face<<<grid,block>>>(ext_rect, mk_cell(0,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_maxfacet_face<<<grid,block>>>(ext_rect, mk_cell(0,0,1), mk_cell(1,0,0), func_tex, flag_surface);

  assign_maxfacet_cube<<<grid,block>>>(ext_rect, func_tex, flag_surface);

  assign_pair_vert<<<grid,block>>>(ext_rect, func_tex, flag_surface);

  assign_pair_edge<<<grid,block>>>(ext_rect, mk_cell(1,0,0), mk_cell(0,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_pair_edge<<<grid,block>>>(ext_rect, mk_cell(0,1,0), mk_cell(0,0,1), mk_cell(1,0,0), func_tex, flag_surface);
  assign_pair_edge<<<grid,block>>>(ext_rect, mk_cell(0,0,1), mk_cell(1,0,0), mk_cell(0,1,0), func_tex, flag_surface);

  assign_pair_face<<<grid,block>>>(ext_rect, mk_cell(1,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_pair_face<<<grid,block>>>(ext_rect, mk_cell(0,1,1), mk_cell(1,0,0), func_tex, flag_surface);
  assign_pair_face<<<grid,block>>>(ext_rect, mk_cell(1,0,1), mk_cell(0,1,0), func_tex, flag_surface);

  //Second pass of assigning pairs, check other option of calling all under 1 kernel
  assign_pair_edge2<<<grid,block>>>(ext_rect, mk_cell(1,0,0), mk_cell(0,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_pair_edge2<<<grid,block>>>(ext_rect, mk_cell(0,1,0), mk_cell(0,0,1), mk_cell(1,0,0), func_tex, flag_surface);
  assign_pair_edge2<<<grid,block>>>(ext_rect, mk_cell(0,0,1), mk_cell(1,0,0), mk_cell(0,1,0), func_tex, flag_surface);

  assign_pair_face2<<<grid,block>>>(ext_rect, mk_cell(1,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_pair_face2<<<grid,block>>>(ext_rect, mk_cell(0,1,1), mk_cell(1,0,0), func_tex, flag_surface);
  assign_pair_face2<<<grid,block>>>(ext_rect, mk_cell(1,0,1), mk_cell(0,1,0), func_tex, flag_surface);

  /*assign_pair_face3<<<grid,block>>>(ext_rect, mk_cell(1,1,0), mk_cell(0,0,1), func_tex, flag_surface);
  assign_pair_face3<<<grid,block>>>(ext_rect, mk_cell(0,1,1), mk_cell(1,0,0), func_tex, flag_surface);
  assign_pair_face3<<<grid,block>>>(ext_rect, mk_cell(1,0,1), mk_cell(0,1,0), func_tex, flag_surface);*/

  mark_cps<<<grid,block>>>(ext_rect, flag_surface);

  cudaDeviceSynchronize();

  //Pitched pointer
  cudaPitchedPtr flag_pitched_ptr;
  flag_pitched_ptr.ptr      = flag;
  flag_pitched_ptr.pitch    = flag_vol_extent.width*sizeof(flag_t);
  flag_pitched_ptr.xsize    = flag_vol_extent.width;
  flag_pitched_ptr.ysize    = flag_vol_extent.height;
  
  //Copy data to 3D array
  cudaMemcpy3DParms flag_copy_params = {0};
  flag_copy_params.srcArray     = d_flag;
  flag_copy_params.dstPtr       = flag_pitched_ptr;
  flag_copy_params.extent       = flag_vol_extent;
  flag_copy_params.kind         = cudaMemcpyDeviceToHost;
  cutilSafeCall(cudaMemcpy3D(&flag_copy_params));


 cudaError_t error = cudaGetLastError();
if (error != cudaSuccess) 
{
  fprintf(stderr, "ERROR: %s \n", cudaGetErrorString(error));
}

  return func_tex;  
}

extern "C" void mark_boundarycps_cuda(short* erptr, short* bnd, short* bnd_dir)
{
  //dim3 block(256,1);
  //dim3 grid(32,1);
  dim3 block(512,1);
  dim3 grid(64,1);
  
  rect_t ext_rect(erptr[0],erptr[1],erptr[2],erptr[3],erptr[4],erptr[5]);
  rect_t bnd_rect(bnd[0],bnd[1],bnd[2],bnd[3],bnd[4],bnd[5]);
  cell_t bnddir = {bnd_dir[0], bnd_dir[1], bnd_dir[2]};
  mark_boundary_cps<<<grid,block>>>(ext_rect, bnd_rect, bnddir, flag_surface);
}

extern "C" void destroy_texture_obj()
{
 cudaDestroyTextureObject(func_tex);
 cudaFreeArray(d_func);
}

extern "C" void destroy_surface_obj()
{
 cudaDestroySurfaceObject(flag_surface);
 cudaFreeArray(d_flag);
}

#define MAX(a,b) ((a > b) ? a : b)

#include <cufft.h>
#include <cuda_runtime.h>


// Beginning of GPU Architecture definitions
inline int _ConvertSMVer2Cores(int major, int minor)
{
	// Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
	typedef struct {
		int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
		int Cores;
	} sSMtoCores;

	sSMtoCores nGpuArchCoresPerSM[] =
	{ { 0x10,  8 },
	  { 0x11,  8 },
	  { 0x12,  8 },
	  { 0x13,  8 },
	  { 0x20, 32 },
	  { 0x21, 48 },
	  {   -1, -1 }
	};

	int index = 0;
	while (nGpuArchCoresPerSM[index].SM != -1) {
		if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor) ) {
			return nGpuArchCoresPerSM[index].Cores;
		}
		index++;
	}
	printf("MapSMtoCores undefined SMversion %d.%d!\n", major, minor);
	return -1;
}
// end of GPU Architecture definitions


// This function returns the best GPU (with maximum GFLOPS)
inline int cutGetMaxGflopsDeviceId()
{
	int current_device   = 0, sm_per_multiproc = 0;
	int max_compute_perf = 0, max_perf_device  = 0;
	int device_count     = 0, best_SM_arch     = 0;
	cudaDeviceProp deviceProp;

	cudaGetDeviceCount( &device_count );
	// Find the best major SM Architecture GPU device
	while ( current_device < device_count ) {
		cudaGetDeviceProperties( &deviceProp, current_device );
		if (deviceProp.major > 0 && deviceProp.major < 9999) {
			best_SM_arch = MAX(best_SM_arch, deviceProp.major);
		}
		current_device++;
	}

    // Find the best CUDA capable GPU device
	current_device = 0;
	while( current_device < device_count ) {
		cudaGetDeviceProperties( &deviceProp, current_device );
		if (deviceProp.major == 9999 && deviceProp.minor == 9999) {
		    sm_per_multiproc = 1;
		} else {
			sm_per_multiproc = _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor);
		}

		int compute_perf  = deviceProp.multiProcessorCount * sm_per_multiproc * deviceProp.clockRate;
		if( compute_perf  > max_compute_perf ) {
            // If we find GPU with SM major > 2, search only these
			if ( best_SM_arch > 2 ) {
				// If our device==dest_SM_arch, choose this, or else pass
				if (deviceProp.major == best_SM_arch) {
					max_compute_perf  = compute_perf;
					max_perf_device   = current_device;
				}
			} else {
				max_compute_perf  = compute_perf;
				max_perf_device   = current_device;
			}
		}
		++current_device;
	}
	return max_perf_device;
}

extern "C"
void init_cuda()
{
  cudaSetDevice( cutGetMaxGflopsDeviceId() );
}


