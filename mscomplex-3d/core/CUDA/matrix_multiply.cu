#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/device_allocator.h>
#include<thrust/device_malloc.h>
#include<thrust/copy.h>
#include<thrust/scan.h>

#include "cutil.h"
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<cuda_runtime.h>
#include<fstream>
#include "bfs_util.h"

#include <cublas_v2.h>
#include <cusparse.h>

#define NUM_THREADS_PER_BLOCK 256
#define NUM_BLOCKS 32

typedef unsigned char flag_t;
typedef short3 cell_t;

//Matrix multiplication for A x B = C
//A(m,k) * B(k,n) = C(m,n)
//m = rowsA, n = colsB, k = colsA = rowsB

//const char* cublasGetErrorString(cublasStatus_t status);

/*__global__ void int_to_char(unsigned char* A, unsigned int* C, int size)
{
  int tid = blockIdx.x*blockDim.x + threadIdx.x;
  int num_thds = gridDim.x*blockDim.x;

  for(int i=tid; i < size; i += num_thds)
  {
   A[i] = C[i] & 0xFF;
  }
}


extern "C" void matrix_multiply(thrust::device_vector<flag_t> &A, int rowsA, int colsA, thrust::device_vector<flag_t> &B, int colsB, thrust::device_vector<unsigned int> &C, thrust::device_vector<flag_t> &jxsad2, int cols_sad2, thrust::device_vector<unsigned int> &sad1xsad2)
{
  printf("Inside CUBLAS Matrix Multiplication!\n");
  
  dim3 block(512,1); //These dimensions aren't the usual
  dim3 grid(64,1);

  //Create a handle for CUBLAS
  cublasHandle_t handle;
  cublasCreate(&handle);

  cublasStatus_t status = cublasCreate(&handle);

  int rowsB = colsA;
  int rowsC = rowsA;

  const flag_t alpha = 1;
  const unsigned int beta = 0;

  int lda = rowsA, ldb = rowsB, ldc = rowsC;

  size_t nzero_elems;

  while(true)
  {

  status = cublasGemmEx(handle, CUBLAS_OP_N, CUBLAS_OP_N, rowsA, colsB, colsA, &alpha, thrust::raw_pointer_cast(&A[0]), CUDA_R_8I, lda, thrust::raw_pointer_cast(&B[0]), CUDA_R_8I, ldb, &beta, thrust::raw_pointer_cast(&C[0]), CUDA_R_32I, ldc, CUDA_R_32I, CUBLAS_GEMM_ALGO0); 

  printf("After Matrix Multiplication!\n");

  A.clear();
  nzero_elems = thrust::copy_if(C.begin(), C.end(), A.begin(), is_not_zero()) - A.begin();

  printf("After checking zero matrix %d\n", nzero_elems);

  if(nzero_elems == 0) break;

  A.clear();
  int_to_char<<<grid,block>>>(thrust::raw_pointer_cast(&A[0]), thrust::raw_pointer_cast(&C[0]), rowsA*colsA);
  cudaDeviceSynchronize();

  C.clear();
  printf("After copy!\n");
  }
  
  int_to_char<<<grid,block>>>(thrust::raw_pointer_cast(&A[0]), thrust::raw_pointer_cast(&C[0]), rowsA*colsA);
  //ldb and ldc do not change

  status = cublasGemmEx(handle, CUBLAS_OP_N, CUBLAS_OP_N, rowsA, cols_sad2, colsA, &alpha, thrust::raw_pointer_cast(&A[0]), CUDA_R_8I, lda, thrust::raw_pointer_cast(&jxsad2[0]), CUDA_R_8I, ldb, &beta, thrust::raw_pointer_cast(&sad1xsad2[0]), CUDA_R_32I, ldc, CUDA_R_32I, CUBLAS_GEMM_ALGO1);

  if(status!=CUBLAS_STATUS_SUCCESS)
  {
  const char* error = cublasGetErrorString(status);
  printf("%s\n", error); 
  }

  cublasDestroy(handle);

}*/

//m = rowsA, k = colsA, n = colsB
/*extern "C" void cusparse_matrix_mult(, int rowsA, int colsA, int nnzA, , int colsB, int nnzB, )
{
  printf("Inside CuSparse Matrix Multiplication!\n");

  cusparseStatus_t status;
  cusparseHandle_t handle = 0;
  cusparseMatDescr_t descrA, descrB, descrC;

  int *csrRowPtrA, *csrRowPtrB, *csrRowPtrC, *csrColIndA, *csrColIndB, *csrColIndC;
 
  const float alpha = 1.0f;
  const float beta = 0.0f;

  //status = cusparseScsrgemm2(handle, rowsA, colsB, colsA, &alpha, descrA, nnzA, , , , descrB, nnzB, , , , &beta, );
  

}*/


/*const char* cublasGetErrorString(cublasStatus_t status)
{
    switch(status)
    {
        case CUBLAS_STATUS_SUCCESS: return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED: return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED: return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE: return "CUBLAS_STATUS_INVALID_VALUE"; 
        case CUBLAS_STATUS_ARCH_MISMATCH: return "CUBLAS_STATUS_ARCH_MISMATCH"; 
        case CUBLAS_STATUS_MAPPING_ERROR: return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED: return "CUBLAS_STATUS_EXECUTION_FAILED"; 
        case CUBLAS_STATUS_INTERNAL_ERROR: return "CUBLAS_STATUS_INTERNAL_ERROR"; 
    }
    return "Unknown error";
}*/

