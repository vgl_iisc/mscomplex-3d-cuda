#include "cutil.h"
#include <matrix_mult_util.h>

typedef short3 cell_t;
typedef uchar flag_t;


inline __device__ bool check_cofacet(cell_t cfct, flag_t fg_cfct, cell_t c, flag_t fg)
{
  if (!is_critical(fg_cfct) && cell_dim(flag_to_pair(cfct, fg_cfct))==1
      && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c)) //No backtracking
       return true;
  
  else 
       return false;
}

template<int pos>
inline __device__ void get_cofacet_pair(int i, cell_t c, flag_t fg,
					const rect_t ext_rect, cell_t cfct,
					int* outfrontier, cudaSurfaceObject_t flag_surface,
					int* visited)
{
  if(ext_rect.contains(cfct))
  {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);
   
   if(check_cofacet(cfct, fg_cfct, c, fg))
    {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's pair converted to i

     if(visited[p] == 0)
     {
      visited[p] = 1; 
      outfrontier[(4*i)+pos] = p;
     }
     
    }
  }

}

inline __device__ bool is_outd_cofacet(const rect_t ext_rect, cell_t cfct,
				      cell_t c, cudaSurfaceObject_t flag_surface,
				      int* visited)
{
  if(ext_rect.contains(cfct))
  {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(!is_critical(fg_cfct))
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair

     if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
       return true;
   }

   else if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddle
      return true;
   
  }
  
  return false;
}


inline __device__ bool is_2saddle(int i, cell_t cfct, const rect_t ext_rect,
				  cudaSurfaceObject_t flag_surface)
{
  if(ext_rect.contains(cfct))
 {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddle
     {
       return true;
     }
 }
  return false;
}


template<int pos>
inline __device__ void get_path_source(int i, cell_t c, flag_t fg, const rect_t ext_rect,
				       cell_t cfct, cudaSurfaceObject_t flag_surface,
				       int* srcs_scan, int* j_scan, int* sad2_scan,
				       int* visited, int* visited_scan, int* junction_pts,
				       int* dest, int* pxj, int* px2sad,
				       int p_srcs, int j_dests, int sad2_dests)
{
 
 if(ext_rect.contains(cfct))
 {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddles
     { 
       px2sad[(4*i)+pos] = return_ind_sad(ext_rect.c_to_i(cfct), sad2_scan);
       
       dest[(4*i)+pos] = -1;
       
       return;
     }
   
   else if(!is_critical(fg_cfct)) //In pxj, p=rows j=columns, (j,p) = (x,y)
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair
     
     if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
     {
        if(junction_pts[visited_scan[p]-1] == 1) //visited_scan[p]-1 is p's index in junction_pts
	 {
	    pxj[(4*i)+pos] = return_ind_jn(p, j_scan, visited_scan);
	     
	    dest[(4*i)+pos] = -1;
	 }
       
        else
	 {
	 dest[(4*i)+pos] = p;
	 }

	return;
     }
     
   }

 }
 dest[(4*i)+pos] = -1;
}


inline __device__ bool trace_path(int i, cell_t c, flag_t fg, const rect_t ext_rect,
				  cell_t cfct, cudaSurfaceObject_t flag_surface,
				  int* srcs_coords, int* srcs_scan, int* j_scan,
				  int* sad2_scan, int p_srcs, int* visited,
				  int* visited_scan, int* junction_pts, int* dest,
				  int* pxj, int* px2sad, int j_dests, int sad2_dests)
{
 if(ext_rect.contains(cfct))
 {
   flag_t fg_cfct;
   surf3Dread (&fg_cfct, flag_surface, cfct.x*sizeof(flag_t), cfct.y, cfct.z);

   if(is_critical(fg_cfct) && cell_dim(cfct)==2) //2-saddle discovered
     {
       //Populate jx2sad or 1sadx2sad matrix
       px2sad[i] = return_ind_sad(ext_rect.c_to_i(cfct), sad2_scan);

       dest[i] = -1;

       return true;
     }
   
   else if(!is_critical(fg_cfct))
   {
     int p = ext_rect.c_to_i(flag_to_pair(cfct, fg_cfct)); //Cofacet's valid pair
     
     if((visited[p] == 1) && check_unequal_cells(flag_to_pair(cfct,fg_cfct),c))
     {
       
        if(junction_pts[visited_scan[p]-1] == 1)
	 {
	   pxj[i] = return_ind_jn(p, j_scan, visited_scan);
	   
	   dest[i] = -1;
	 }
       
        else
	 {
	   dest[i] = p;
	 }
	
	return true;
     }

   }
   
 }

 dest[i] = -1; //Includes dead end paths which do not reach 2-saddles from 1-saddles 
 return false;
}
