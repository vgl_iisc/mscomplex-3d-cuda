#include <queue>
#include <map>
#include <iterator>

#include<cmath>

#include <boost/foreach.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>

#include <grid_dataset.h>
#include <grid_mscomplex.h>

#include <omp.h>

using namespace std;

namespace br = boost::range;
namespace ba = boost::adaptors;

std::ofstream output_txt, output_csv;

namespace grid
{  
    
/*===========================================================================*/
  
  void mscomplex_t::clear_subgrid_ds()
  {
    
    p_to_i_ms.clear();
    i_to_p_ms.clear();
    
    i_to_rect.clear();
    is_bnd_e.clear();
    one_hop_e.clear();
    is_bnd_vrt.clear();
    
  }
 /*---------------------------------------------------------------------------*/
  
  template <PERS pers>
  void mscomplex_t::part_serial(const double &thresh, const int &edges,
				const bool &canc_all)
  {
    int iter = edges;
    
    auto cmp = bind(&mscomplex_t::persistence_cmp,this,_2,_1);
    priority_queue<int_pair_t,int_pair_list_t,typeof(cmp)> pq(cmp);

    for(int i = 0; i < get_num_critpts(); ++i)
      {
	if(!is_canc_prl(i))
	  {
	    BOOST_FOREACH(int_int_t j, m_des_conn[i])
	      {
		int_pair_t pr(i,j.first);
		
		if(pers==not_eps && is_valid_canc_edge_prl(pr,thresh))
		  pq.push(pr);
		
		else if(pers==eps && is_epsilon_pers_edge(pr))
		  pq.push(pr);
	      }

	    if((canc_all == false) && (pq.size() >= iter))
	      break;
	  }
      }

    iter = edges;
    
     while(pq.size()!=0)
       {
	 int_pair_t pr = pq.top();	 
	 pq.pop();
	 
	 if(pers==not_eps && !is_valid_canc_edge_prl(pr,thresh))
	   continue;
	 
	 else if(pers==eps && !is_epsilon_pers_edge(pr))
	   continue;

	 ASSERT(order_pair<DES>(shared_from_this(),pr) == pr);

	 cancel_pair(pr[0],pr[1]);
	 iter--;

	 if(iter == 0 && canc_all==false)
	   break;

	 BOOST_FOREACH(int_int_t i,m_des_conn[pr[0]])
	   BOOST_FOREACH(int_int_t j,m_asc_conn[pr[1]])
	   {
	     int_pair_t e(j.first,i.first);
	     
	     if(pers==not_eps && is_valid_canc_edge_prl(e,thresh))
	       pq.push(e);
	     
	     else if(pers==eps && is_epsilon_pers_edge(e))
	       pq.push(e);
	   }
       }
     
  }

/*---------------------------------------------------------------------------*/

  void mscomplex_t::get_str(int &start, int &stride, const int &dir,
			    const int &off, const int &block_id)
  {
    int fixed_bnd = block_id*block_size[dir];
      
    //first block
    if(block_id == 0)
      {
	start = 0;
	stride = block_size[dir] + off;
	return;
      }

    //last block
    else if(block_id == num_blocks-1)
      {
	//width > 0 for a valid block
	int width = m_ext_rect.span()[dir] - fixed_bnd;

	//invalid block
	if(width == 0)
	  {
	    start = fixed_bnd;
	    stride = 0;
	    return;
	  }
	  
	//+ve offset
	if(off > 0)
	  {
	    float fctr = (float)off/block_size[dir];
	    start = fixed_bnd + std::round(width*fctr);
	  }

	//zero or -ve offset
	else
	  start = fixed_bnd + off;

	stride = m_ext_rect.span()[dir]-start;
	
	return;
      }
    
    //regular block
    start = fixed_bnd + off;

    if(start + block_size[dir] <= m_ext_rect.span()[dir])
      stride = block_size[dir];

    else
      stride = m_ext_rect.span()[dir] - start;
    
  }

/*---------------------------------------------------------------------------*/
  
  //dir = (x,y,z) = (0,1,2)

  void mscomplex_t::create_blocks(const int &dir, const int &off)
  {
    ENSURE(dir == 0 || dir == 1 || dir == 2, "Incorrect direction");
    ENSURE(off > -block_size[dir] && off < block_size[dir],
	   "Choose offset between (-block_size, block_size)");

    i_to_rect.resize(num_blocks);

    omp_set_num_threads(num_blocks);
    
    #pragma omp parallel
    {
      #pragma omp for
      for(int block_id = 0; block_id < num_blocks; block_id++)
	{
	  int stride, start;
	  cellid_t lc, uc;

	  //get start and stride for each block
	  get_str(start, stride, dir, off, block_id);

	   ENSURE(start+stride <= m_ext_rect.span()[dir],
		  "Block out of bounds");
	   
	  switch(dir)
	    {
	    case 0:
	      lc = cellid_t(start,0,0);
	      uc = lc + cellid_t(stride,block_size[1],block_size[2]);
	      break;
	      
	    case 1:
	      lc = cellid_t(0,start,0);
	      uc = lc + cellid_t(block_size[0],stride,block_size[2]);
	      break;
	      
	    case 2:
	      lc = cellid_t(0,0,start);
	      uc = lc + cellid_t(block_size[0],block_size[1],stride);
	      break;	    	      
	    }

	  if(!m_ext_rect.contains(lc) || !m_ext_rect.contains(uc))
	    {
	      std::cout<<block_id<<"\n";
	      std::cout<<lc<<" "<<uc<<"\n";
	    }
 	    
	  ENSURES(m_ext_rect.contains(lc) && m_ext_rect.contains(uc));
	  i_to_rect[block_id] = rect_t(lc,uc);
	  
	}
    }

  }

/*===========================================================================*/
/*- Critical points = i from (0 to get_num_critpts())
  - These ids constitute any int_pair_t p, which forms an edge
  - Obtaining coords from ids can be done using cellid(i) (dual grid) 
  or cellid(i)/2 (single grid)
  - These collection of edges can be either the MS graph (total edges) 
  or the persistent graph (based on threshold)
  - i_to_p_ms and p_to_i_ms give the mappings from edge to index and vice versa
  - i_to_p maps the edge list to the vert list, p also denotes ids and not vert coords
  - Same for pers graph - i_to_p_pers and p_to_i_pers*/

/*===========================================================================*/

  bool mscomplex_t::crosses_bnd(const int &e, const int &dir, const int &off)
  {
    ENSURE(off > -block_size[dir] && off < block_size[dir],
	   "Choose offset between (-block_size, block_size)");
    
    cellid_t c1 = cellid(i_to_p_ms[e].first);
    cellid_t c2 = cellid(i_to_p_ms[e].second);

    int c1_blck = (int)c1[dir]/block_size[dir];
    int c2_blck = (int)c2[dir]/block_size[dir];
    	
    if(off == 0)
      return (c1_blck != c2_blck);

    else if(off > 0)
      {
	//check coord < corrected bnd, first (0th) block unaffected
	
	if((c1_blck != 0) && c1[dir] < (c1_blck*block_size[dir] + off)) 
	  c1_blck -= 1;

	if((c2_blck != 0) && c2[dir] < (c2_blck*block_size[dir] + off)) 
	  c2_blck -= 1;
      }

    else if(off < 0)
      {
	//check coord > corrected bnd, last (n-1th) block unaffected
	
	if((c1_blck != num_blocks-1) && c1[dir] > ((c1_blck+1)*block_size[dir] + off)) 
	  c1_blck += 1;

	if((c2_blck != num_blocks-1) && c2[dir] > ((c2_blck+1)*block_size[dir] + off)) 
	  c2_blck += 1;
      }

    return (c1_blck != c2_blck);
      
  }

/*---------------------------------------------------------------------------*/

  bool mscomplex_t::touches_bnd(const int &e, const int &dir, const int &off)
  {
    ENSURE((off > -block_size[dir]) && (off < block_size[dir]),
	   "Choose offset between (-block_size, block_size)");
    
    cellid_t c1 = cellid(i_to_p_ms[e].first);
    cellid_t c2 = cellid(i_to_p_ms[e].second);

    if(off == 0)
      return (c1[dir]%block_size[dir] == 0 || c2[dir]%block_size[dir] == 0);
    
    else 
      {	
	int c1_blck = (int)c1[dir]/block_size[dir];
	int c2_blck = (int)c2[dir]/block_size[dir];
	bool c1_bnd, c2_bnd;

	if(off > 0)
	  {
	    if(c1_blck != 0)
	      c1_bnd = (c1[dir] == c1_blck*block_size[dir] + off) ? true : false;
	    
	    if(c1_bnd == false && c2_blck != 0)
	      {
		c2_bnd = (c2[dir] == c2_blck*block_size[dir] + off) ? true : false;
		return c2_bnd;
	      }
	    
	    return c1_bnd;
	  }
	
	else if(off < 0)
	  {
	    if(c1_blck != num_blocks-1)
	      c1_bnd = (c1[dir] == (c1_blck+1)*block_size[dir] + off) ? true : false;
	    
	    if(c1_bnd == false && c2_blck != num_blocks-1)
	      {
		c2_bnd = (c2[dir] == (c2_blck+1)*block_size[dir] + off) ? true : false;
		return c2_bnd;
	      }
	    
	    return c1_bnd;
	  }
	
      }

  }

/*---------------------------------------------------------------------------*/

inline void save_run(const int &it, const int &dir, const int &bef_cps, const int &aft_cps,
		       const int &bef_e, const int &aft_e, const float &off)
   { 
    output_csv<<it<<','<<dir<<','<<bef_cps<<','<<aft_cps<<','<<bef_e<<','<<aft_e<<','<<off<<"\n";
  }

  /*---------------------------------------------------------------------------*/
  
  void mscomplex_t::geom_subdiv_pers(const double &th)
{
  int num_procs = 32;
  int verts = get_num_critpts();
  int iters = 6;
  num_blocks = 2;
  int offset;

  prl_m_canc_list_0.resize(iters);
  prl_m_canc_list_1.resize(iters);
  			
  for(int it = 0; it < iters; it++)
    {
      int dir = it%3; //one offset at a time - X Y Z X Y Z
      //int dir = (int)it/2; //one dir at a time - X X Y Y Z Z

      //int dir = ((iters-1)-it)%3; //Z Y X Z Y X
      //int dir = (int)((iters-1)-it)/2; //Z Z Y Y X X
      
      switch(dir)
	{
	case 0:
	  block_size = cellid_t(std::round((double)m_ext_rect.span()[0]/num_blocks),
				m_ext_rect.span()[1], m_ext_rect.span()[2]);
	  break;
	  
	case 1:
	  block_size = cellid_t(m_ext_rect.span()[0],
				std::round((double)m_ext_rect.span()[1]/num_blocks),
				m_ext_rect.span()[2]);
	  break;
	  
	case 2:
	  block_size = cellid_t(m_ext_rect.span()[0], m_ext_rect.span()[1],
				std::round((double)m_ext_rect.span()[2]/num_blocks));
	  break;
	}
      
      //Set offset
      switch((int)it/3) //one offset at a time - X Y Z X Y Z / Z Y X Z Y X
      //switch(it%2) //one dir at a time - X X Y Y Z Z / Z Z Y Y X X
	{
	case 0:
	  offset = 0;
	  break;
	  
	case 1:
	  offset = (int)(block_size[dir]/2);
	  break;

	default:
	  std::cout<<"Iterations exceed offset specifications\n";
	  
	}
      
      //Convert MS edges to indices
      for(int i = 0; i < get_num_critpts(); ++i)
	{
	  if(!is_canc_prl(i))
	    {
	      BOOST_FOREACH(int_int_t j, m_des_conn[i])
		{
		  int_pair_t pr(i,j.first);
		  ENSURES(index(i) == index(j.first)+1);
		  
		  ms_to_id(pr, verts);	  
		}
	      
	    }
	}
     
      //Creates slices and assigns threads to each slice
      create_blocks(dir, offset);

      omp_set_num_threads(num_procs);
      
      mark_bnd_e(dir, offset);
      
      mark_one_hop_e(verts);

      simplify_subgrids<not_eps>(verts, th, it);
      
      clear_subgrid_ds();

    }
  
}

/*---------------------------------------------------------------------------*/

  void mscomplex_t::mark_bnd_e(const int &dir, const int &off)
  {
    is_bnd_e.resize(i_to_p_ms.size(), 0);
    is_bnd_vrt.resize(get_num_critpts(), 0);
   
    #pragma omp parallel for
     for(int e = 0; e < p_to_i_ms.size(); e++)
       { 
 	bool bnd_e;
 
	//exchange orders of crosses and touches_bnd
	
	//edge that touches atleast one boundary
	bnd_e = (touches_bnd(e, dir, off)) ? true : false;
	
	//edge endpoints cross atleast 1 boundary
	if(bnd_e == false)
	  bnd_e = (crosses_bnd(e, dir, off)) ? true : false;
	
	if(bnd_e == true)
	  {
	    is_bnd_vrt[i_to_p_ms[e].first] = 1;
	    is_bnd_vrt[i_to_p_ms[e].second] = 1;
	    is_bnd_e[e] = 1;
	  }
      }
 
  }


/*---------------------------------------------------------------------------*/

  void mscomplex_t::mark_one_hop_e(int &verts)
  {
    one_hop_e.resize(i_to_p_ms.size(),0);
    
    #pragma omp parallel for
    for(int e = 0; e < p_to_i_ms.size(); e++)
      {
	if(is_bnd_e[e] == 1)
	  {
	    long e_key;
	    int v1_i = i_to_p_ms[e].first;
	    
	    BOOST_FOREACH(int_int_t j, m_des_conn[v1_i])
	      {
		e_key = return_long(v1_i, j.first, verts);
		one_hop_e[p_to_i_ms.at(e_key)] = 1;
	      }
	    
	    BOOST_FOREACH(int_int_t j, m_asc_conn[v1_i])
	      {
		e_key = return_long(j.first, v1_i, verts);
		one_hop_e[p_to_i_ms.at(e_key)] = 1;
	      }
	    
	    int v2_i = i_to_p_ms[e].second;
	    
	    BOOST_FOREACH(int_int_t j, m_des_conn[v2_i])
	      {
		e_key = return_long(v2_i, j.first, verts);
		one_hop_e[p_to_i_ms.at(e_key)] = 1;
	      }
	    
	    BOOST_FOREACH(int_int_t j, m_asc_conn[v2_i])
	      {
		e_key = return_long(j.first, v2_i, verts);
		one_hop_e[p_to_i_ms.at(e_key)] = 1;
	      }
	  }
      }
  }

/*---------------------------------------------------------------------------*/
    
  template <PERS pers>
  void mscomplex_t::simplify_subgrids(int &verts, const double &thresh, const int &iter)
  {
    //Ensures that parallel region has threads = num_blocks
    omp_set_num_threads(num_blocks);
    
    #pragma omp parallel
    {
      auto cmp = bind(&mscomplex_t::persistence_cmp,this,_2,_1);
      priority_queue<int_pair_t,int_pair_list_t,typeof(cmp)> pq(cmp);
      
      #pragma omp for
      for(int block_id = 0; block_id < num_blocks; block_id++)
	{
	  rect_t subgrid(i_to_rect.at(block_id));

	  //check for invalid (n-1)th block
	  if(block_id == num_blocks-1)
	    {
	      //invalid subgrid
	      if(subgrid.lc()[0] == subgrid.uc()[0] ||
		 subgrid.lc()[1] == subgrid.uc()[1] ||
		 subgrid.lc()[2] == subgrid.uc()[2])
		continue;
	    }
	  
	  for(int i = 0; i < verts; i++)
	    {
	      cellid_t c1 = cellid(i);

	      //early exit if vertex not contained in subgrid
	      if(!subgrid.contains(c1))
		continue;
		
	      if(!is_canc_prl(i))
		{
		  BOOST_FOREACH(int_int_t j, m_des_conn[i])
		    {
		      cellid_t c2 = cellid(j.first);
		      
		      int_pair_t pr(i,j.first);
		      
		      //check if second vertex is within subgrid
		      if(!subgrid.contains(c2))
			continue;
		      
		      //check if edge is a boundary edge
		      int e_i = p_to_i_ms.at(return_long(i, j.first, verts));
		      
		      if(is_bnd_e.at(e_i) != 1 && one_hop_e.at(e_i) != 1)
			{
			  if(pers==not_eps && is_valid_canc_edge_prl(pr,thresh))
			    pq.push(pr);
			}
		    }
		}
	    }
	}

     while(pq.size() != 0)
       {
	 int_pair_t pr = pq.top();
	 
	 pq.pop();

	 if(!is_valid_canc_edge_prl(pr,thresh))
	   continue;

	 ASSERT(order_pair<DES>(shared_from_this(),pr) == pr);
	 
	 cancel_pair(pr[0],pr[1]);

	 //Add to list of canc edges
	 omp_get_thread_num() == 0 ? prl_m_canc_list_0[iter].push_back(pr) : prl_m_canc_list_1[iter].push_back(pr);
	 
	 //Inclusion of reconnected edges (non-conservative)
	 /*BOOST_FOREACH(int_int_t i,m_des_conn[pr[0]])
	   BOOST_FOREACH(int_int_t j,m_asc_conn[pr[1]])
	   {
	     //Splitting ensures that we don't recheck i
	     if(is_bnd_vrt.at(i.first) == 1)
	       break;
	     
	     if(is_bnd_vrt.at(j.first) == 1)  
	       continue;
	     
	     int_pair_t e(j.first,i.first);
	     order_pr_by_cp_index(*this,e[0],e[1]);
	     
	     if(is_valid_canc_edge_prl(e,thresh))
	       pq.push(e);
	     
	       }*/

	 //Comment out code above for conservative approach
       }
     
     
     
    }
    
  }

  
}
